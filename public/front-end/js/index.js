$(document).ready(function() {
    var owl = $('.owl-banner');
    owl.owlCarousel({
        margin: 0,
        navText: ['', ''],
        nav: true,
        loop: true,
        dots: true,
        animateOut: 'fadeOut',
        autoplay: true,
        autoplaySpeed: 1500,
        autoplayTimeout: 3000,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            1200: {
                items: 1
            }
        }
    });


    new WOW().init();

    $('.owl-customer').owlCarousel({
        loop: true,
        dots: true,
        margin: 20,
        nav: false,
        // autoplay: true,
        // autoplaySpeed: 3000,
        // autoplayTimeout: 3000,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 2
            }
        }
    })
    $('.owl-related').owlCarousel({
        loop: false,
        dots: false,
        margin: 20,
        nav: true,
        autoplay: true,
        autoplaySpeed: 3000,
        autoplayTimeout: 3000,
        navText: ['', ''],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    })
    $('.owl-blog').owlCarousel({
        loop: false,
        dots: false,
        margin: 20,
        nav: true,
        autoplay: true,
        autoplaySpeed: 3000,
        autoplayTimeout: 3000,
        navText: ['', ''],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    })

    var proc = document.getElementsByClassName("proc");
    var a;

    for (a = 0; a < proc.length; a++) {

        proc[a].onclick = function() {
            $(".process-wrap .nav-tabs li").removeClass('active');
            this.classList.toggle("active");
        }
    }

});

//-------JS sidenav---------//
//-- hien nav--///
function openNav() {
    $('#mySidenav').css({ transform: 'translateX(0)' });
    $('#mySidenav').css({ transition: 'all 0.5s' });
    // document.body.style.overflow = "hidden";
    $(document).ready(function() {
        $('#rotation').addClass('rotation');
        $('#rotation').removeClass('rotation_backwards');
    });
}
//--an nav--//
function closeNav() {
    $('#mySidenav').css({ transform: 'translateX(-250px)' });
    $('#mySidenav').css({ transition: 'all 0.5s' });
    document.body.style.overflow = "visible";
    $(document).ready(function() {
        $('#rotation').addClass(' rotation_backwards');
        $('#rotation').removeClass('rotation');
    });
}
//--tat su kien  khi click vao sidenav--// 
$(document).ready(function() {
    $("#hide-sidebar").click(function(event) {
        event.stopPropagation();
        openNav();
        $('#ovelay').addClass('ovelay');
    });
    $("#mySidenav").click(function(event) {
        event.stopPropagation();
    });
    $("#close-btn").click(function(event) {
        closeNav();
        $('#ovelay').removeClass('ovelay');
    });
    $("body").click(function(event) {
        closeNav();
        $('#ovelay').removeClass('ovelay');
    });
});

$(document).ready(function() {
    //--Them nut button--// 
    var x = $(".sidenav ul li").find("ul");
    if (x.length != 0) {
        x.before('<button class="accordion_2"></button>')

    }
    //--accordition--// 
    var acc_2 = document.getElementsByClassName("accordion_2");
    var j;

    for (j = 0; j < acc_2.length; j++) {
        acc_2[j].onclick = function() {
            this.classList.toggle("active");
            var sub_item = this.nextElementSibling;
            if (sub_item.style.maxHeight) {
                sub_item.style.maxHeight = null;
            } else {
                sub_item.style.maxHeight = sub_item.scrollHeight + 500 + "px";
            }
        }
    }

    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight == '0px') {
                panel.style.maxHeight = panel.scrollHeight + "px";
            } else {
                panel.style.maxHeight = 0;
            }
        }
    }

    var acc_2 = document.getElementsByClassName("accordion-1");
    var j;

    for (j = 0; j < acc_2.length; j++) {
        acc_2[j].onclick = function() {
            this.classList.toggle("active");
            var sub_menu = this.nextElementSibling;
            if (sub_menu.style.maxHeight) {
                sub_menu.style.maxHeight = null;
            } else {
                sub_menu.style.maxHeight = sub_menu.scrollHeight + "px";
            }
        }
    }
});

// Scroll top
$(document).ready(function() {

    $(window).scroll(function() {
        if ($(this).scrollTop() > 1000) {
            $('.scrollup').fadeIn();
            $('.scrollup').addClass('move_top');
            //     $(".fixed").animate({
            //     height: 'toggle'
            // });
        } else {
            $('.scrollup').fadeOut();
            $('.scrollup').removeClass('move_top');
            // $(".scrollup").stop();
        }
    });

    $('.scrollup').click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });
    //    $(document).ready(function(){
    //     $("button").click(function(){
    //         $("div").animate({
    //             height: 'toggle'
    //         });
    //     });
    // });



});
// $(document).ready(function() {

//     // Even when the window is resized, run this code.
//     $(window).resize(function() {

//         // Variables
//         var windowHeight = $(window).height();

//         // Find the value of 90% of the viewport height
//         var ninetypercent = .2 * windowHeight;

//         // When the document is scrolled ninety percent, toggle the classes
//         // Does not work in iOS 7 or below
//         // Hasn't been tested in iOS 8
//         $(document).scroll(function() {

//             // Store the document scroll function in a variable
//             var y = $(this).scrollTop();

//             // If the document is scrolled 90%
//             if (y > ninetypercent) {

//                 // Add the "sticky" class
//                 $('.mid-header').addClass('sticky');
//             } else {
//                 // Else remove it.
//                 $('.mid-header').removeClass('sticky');
//             }
//         });

//         // Call it on resize.
//     }).resize();

// }); // jQuery

$(document).ready(function() {

    var sync1 = $("#sync1");
    var sync2 = $("#sync2");
    var slidesPerPage = 6; //globaly define number of elements per page
    var syncedSecondary = true;

    sync1.owlCarousel({
        items: 1,
        slideSpeed: 2000,
        nav: true,
        autoplay: false,
        dots: true,
        loop: true,
        responsiveRefreshRate: 200,
        navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>', '<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 1px;stroke: #000;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
    }).on('changed.owl.carousel', syncPosition);

    sync2
        .on('initialized.owl.carousel', function() {
            sync2.find(".owl-item").eq(0).addClass("current");
        })
        .owlCarousel({
            items: slidesPerPage,
            dots: true,
            nav: true,
            smartSpeed: 2000,
            slideSpeed: 1000,
            slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
            responsiveRefreshRate: 100,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
        }).on('changed.owl.carousel', syncPosition2);

    function syncPosition(el) {
        //if you set loop to false, you have to restore this next line
        //var current = el.item.index;

        //if you disable loop you have to comment this block
        var count = el.item.count - 1;
        var current = Math.round(el.item.index - (el.item.count / 2) - .5);

        if (current < 0) {
            current = count;
        }
        if (current > count)  {
            current = 0;
        }

        //end block

        sync2
            .find(".owl-item")
            .removeClass("current")
            .eq(current)
            .addClass("current");
        var onscreen = sync2.find('.owl-item.active').length - 1;
        var start = sync2.find('.owl-item.active').first().index();
        var end = sync2.find('.owl-item.active').last().index();

        if (current > end) {
            sync2.data('owl.carousel').to(current, 100, true);
        }
        if (current < start) {
            sync2.data('owl.carousel').to(current - onscreen, 100, true);
        }
    }

    function syncPosition2(el) {
        if (syncedSecondary) {
            var number = el.item.index;
            sync1.data('owl.carousel').to(number, 100, true);
        }
    }

    sync2.on("click", ".owl-item", function(e) {
        e.preventDefault();
        var number = $(this).index();
        sync1.data('owl.carousel').to(number, 300, true);
    });
});