$('.owl-carousel-1').owlCarousel({
    loop:true,
    nav:true,
    autoplay: true,
    autoplaySpeed: 1500,
    autoplayTimeout: 3000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});

$('.owl-carousel-detail-1').owlCarousel({
    // loop:true,
    nav:true,
    navText:['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    margin: 10,
    responsive:{
        0:{
            items:3
        },
        768:{
            items:3
        },
        960:{
            items:3
        },

        1000:{
            items:3
        }
    }
})
// js menu //
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var sub_item = this.nextElementSibling;
    if (sub_item.style.maxHeight){
      sub_item.style.maxHeight = null;
    } else {
      sub_item.style.maxHeight = sub_item.scrollHeight + "px";
    } 
  }
}

function searchrs() {
    var keyword = $('input[name=keyword]').val() ? $('input[name=keyword]').val() : $('input[name=keyword1]').val();
    var url = '/tim-kiem-san-pham?keyword=' + keyword;
    window.location.href = url;
}