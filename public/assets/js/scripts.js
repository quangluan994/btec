function ChangeToSlug()
{
    var name, slug,data;

    name = document.getElementById("name").value;

    //doi thanh chu thuong
    slug = name.toLowerCase();

    //ky tu co dau thanh khong dau
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');
    //xoa ky tu dac biet
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
    //doi khoang trang thanh ky tu dau noi
    slug = slug.replace(/ /gi, "-");
    //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');
    //Xóa các ký tự gạch ngang ở đầu và cuối
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');
    // data = {token:_token , slug:slug}
    // var request = $.ajax({
    // 	url:"/checkinmd",
    // 	type:"POST",
    // 	_token: "{{ csrf_token() }}",
    // 	data: data,
    // 	success: function(){
    // 	document.getElementById('slug').value = slg1;
    // 	},
    // 	error: function(xhr){
    // 		console.log(xhr.message);
    // 	}
    // });

    //In slug ra input có id slug
     document.getElementById('slug').value = slug;
}

// function ChangeToSlug()
// {
//     var name, slug;

//     name = document.getElementById("name").value;

//     //doi thanh chu thuong
//     slug = name.toLowerCase();

//     //ky tu co dau thanh khong dau
//     slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
//     slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
//     slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
//     slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
//     slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
//     slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
//     slug = slug.replace(/đ/gi, 'd');
//     //xoa ky tu dac biet
//     slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
//     //doi khoang trang thanh ky tu dau noi
//     slug = slug.replace(/ /gi, "-");
//     //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
//     slug = slug.replace(/\-\-\-\-\-/gi, '-');
//     slug = slug.replace(/\-\-\-\-/gi, '-');
//     slug = slug.replace(/\-\-\-/gi, '-');
//     slug = slug.replace(/\-\-/gi, '-');
//     //Xóa các ký tự gạch ngang ở đầu và cuối
//     slug = '@' + slug + '@';
//     slug = slug.replace(/\@\-|\-\@|\@/gi, '');

//     //In slug ra textbox có id “slug”
//     document.getElementById('slug').value = slug;
// }

// function save(khId) {
//   var token = $('#token').val();
//   $.ajax({
//       type: 'post',
//       url: '/save',
//       data: {
//         khId: khId,
//         _token: token
//       },
//       // beforeSend: function(xhr){
//       //     xhr.setRequestHeader('X-CSRF-Token', token);
//       // },
//       success: function(response) {
//           var res = $.parseJSON(response);
//           if(res.status) {
//             $('#save-icon').html(res.icon);
//           }
//       }
//   });
// }