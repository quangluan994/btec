function action() {
	var type = $(".hasCustomSelect").val();
    var display_type = $(".hasCustomSelect option:selected").text();
    var token = $("input[name='_token']").val();
    if(type == 'Chọn tác vụ' || !type) {
        sweetAlert('Hãy chọn tác vụ');
        return;
    }
    var url = window.location.origin+window.location.pathname + '/' + type;
   
    var checked = new Array();
    $('input:checkbox[name=checkbox_id]:checked').each(function() {
        if(this.checked) {
            checked.push($(this).val());
        }
    });
    if(checked.length == 0) {
        sweetAlert('Chưa chọn bản ghi.');
        return;
    }
    swal({
        title: "Bạn có muốn thực hiện: "+display_type+"?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#ff1a1a",
        confirmButtonText: "Ok",
        closeOnConfirm: true
    },
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: 'post',
                url: url,
                data: {id: checked},
                beforeSend: function(xhr){
                    xhr.setRequestHeader('X-CSRF-Token', token);
                },
                success: function(response) {
                    var res = $.parseJSON(response);
                    console.log(res);
                    if(!res.status) {
                        return;
                    }
                    location.reload(true);
                    swal({
                        title: "Thành công!",
                        timer: 2000,
                        type: "success"
                    });
                }
            });
        }
    });
}
//check all
$('.check-all').click(function() {
    var checkboxes = $(this).closest('.respontables').find(':checkbox');
    if($(this).is(':checked')) {
        checkboxes.prop('checked', true);
    } else {
        checkboxes.prop('checked', false);
    }
});