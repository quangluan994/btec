<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoHoiNgheNghiepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('co_hoi_nghe_nghieps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->integer('program_id')->unsigned()->nullable();
            $table->string('comment_name');
            $table->string('comment_introduce');
            $table->string('comment_avata');
            $table->text('comment_content');
            $table->foreign('program_id')->references('id')->on('chuong_trinh_dao_taos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('co_hoi_nghe_nghieps');
    }
}
