<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(roleSeeder::class);
        $this->call(userSeeder::class);
        $this->call(displaySeeder::class);
    }
}

class roleSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->insert([
                ['display_name'=>'Admin'],
                ['display_name'=>'Manager'],
                ['display_name'=>'Custommer'],

            ]);
    }
}

class userSeeder extends Seeder
{
	public function run()
	{
		DB::table('users')->insert([
				['name'=>'admin','email'=>'admin@admin.com','password'=>bcrypt('admin123'),'avatar'=>'no-img.jpg','role_id'=>'1']
			]);
	}
}

class displaySeeder extends Seeder
{
    public function run()
    {
        DB::table('display_columns')->insert([
                ['name'=>'count_item_product','value'=>'15'],
                ['name'=>'count_item_new','value'=>'15'],
                ['name'=>'skin_color_admin','value'=>'skin-blue']
            ]);
    }
}