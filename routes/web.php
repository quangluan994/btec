<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
	// Route::get('/clear-cache', function() {
	//     $exitCode = Artisan::call('cache:clear');
	//     return '<h1>Cache facade value cleared</h1>';
	// });
	// Route::get('/view-clear', function() {
	//     $exitCode = Artisan::call('view:clear');
	//     return '<h1>View cache cleared</h1>';
	// });
	// Route::get('/config-cache', function() {
	//     $exitCode = Artisan::call('config:clear');
	//     return '<h1>Clear Config cleared</h1>';
	// });
	Route::group(['prefix' => 'admin'], function() {
	Route::group(['middleware'=> 'auth'],function(){
		Route::group(['middleware'=> 'admin'],function(){
		Route::get('dashboard','\App\Http\Controllers\Admin\DashboardController@index');
		Route::get('media','\App\Http\Controllers\Admin\DashboardController@media');
		
		// Route::resource('permission','\App\Http\Controllers\Admin\PermissionController');
		// Route::post('permission/{id}/update','\App\Http\Controllers\Admin\PermissionController@update');
		// Route::get('permission/{id}/delete','\App\Http\Controllers\Admin\PermissionController@destroy');


		// Route::resource('role','\App\Http\Controllers\Admin\RoleController');
		// Route::post('role/{id}/update','\App\Http\Controllers\Admin\RoleController@update');
		// Route::get('role/{id}/delete','\App\Http\Controllers\Admin\RoleController@destroy');

		Route::resource('profile', '\App\Http\Controllers\Admin\ProfileController');
		Route::post('profile/{id}/update','\App\Http\Controllers\Admin\ProfileController@update');
		Route::post('profile/{id}/changPw','\App\Http\Controllers\Admin\ProfileController@changPassword');


		Route::resource('user','\App\Http\Controllers\Admin\UserController');
		Route::post('user/remove','\App\Http\Controllers\Admin\UserController@remove');
		Route::post('user/active','\App\Http\Controllers\Admin\UserController@active');
		Route::post('user/{id}/update','\App\Http\Controllers\Admin\UserController@update');
		Route::get('user/{id}/delete','\App\Http\Controllers\Admin\UserController@destroy');


		Route::resource('lydo','\App\Http\Controllers\Admin\LyDoController');
		Route::post('lydo/remove','\App\Http\Controllers\Admin\LyDoController@remove');
		Route::post('lydo/active','\App\Http\Controllers\Admin\LyDoController@active');
		Route::post('lydo/{id}/update','\App\Http\Controllers\Admin\LyDoController@update');
		Route::get('lydo/{id}/delete','\App\Http\Controllers\Admin\LyDoController@destroy');


		Route::resource('chuong-trinh-giang-day','\App\Http\Controllers\Admin\ChuongTrinhController');
		Route::post('chuong-trinh-giang-day/remove','\App\Http\Controllers\Admin\ChuongTrinhController@remove');
		Route::post('chuong-trinh-giang-day/active','\App\Http\Controllers\Admin\ChuongTrinhController@active');
		Route::post('chuong-trinh-giang-day/{id}/update','\App\Http\Controllers\Admin\ChuongTrinhController@update');
		Route::get('chuong-trinh-giang-day/{id}/delete','\App\Http\Controllers\Admin\ChuongTrinhController@destroy');


		Route::resource('co-hoi-nghe-nghiep','\App\Http\Controllers\Admin\CoHoiNgheNghiepController');
		Route::post('co-hoi-nghe-nghiep/remove','\App\Http\Controllers\Admin\CoHoiNgheNghiepController@remove');
		Route::post('co-hoi-nghe-nghiep/active','\App\Http\Controllers\Admin\CoHoiNgheNghiepController@active');
		Route::post('co-hoi-nghe-nghiep/{id}/update','\App\Http\Controllers\Admin\CoHoiNgheNghiepController@update');
		Route::get('co-hoi-nghe-nghiep/{id}/delete','\App\Http\Controllers\Admin\CoHoiNgheNghiepController@destroy');

		Route::resource('thong-bao-tuyen-sinh','\App\Http\Controllers\Admin\ThongBaoTuyenSinhController');
		Route::post('thong-bao-tuyen-sinh/remove','\App\Http\Controllers\Admin\ThongBaoTuyenSinhController@remove');
		Route::post('thong-bao-tuyen-sinh/active','\App\Http\Controllers\Admin\ThongBaoTuyenSinhController@active');
		Route::post('thong-bao-tuyen-sinh/{id}/update','\App\Http\Controllers\Admin\ThongBaoTuyenSinhController@update');
		Route::get('thong-bao-tuyen-sinh/{id}/delete','\App\Http\Controllers\Admin\ThongBaoTuyenSinhController@destroy');


		Route::resource('gioi-thieu','\App\Http\Controllers\Admin\GioiThieuController');
		Route::post('gioi-thieu/remove','\App\Http\Controllers\Admin\GioiThieuController@remove');
		Route::post('gioi-thieu/active','\App\Http\Controllers\Admin\GioiThieuController@active');
		Route::post('gioi-thieu/{id}/update','\App\Http\Controllers\Admin\GioiThieuController@update');
		Route::get('gioi-thieu/{id}/delete','\App\Http\Controllers\Admin\GioiThieuController@destroy');


		Route::resource('doi-tac','\App\Http\Controllers\Admin\DoiTacController');
		Route::post('doi-tac/remove','\App\Http\Controllers\Admin\DoiTacController@remove');
		Route::post('doi-tac/active','\App\Http\Controllers\Admin\DoiTacController@active');
		Route::post('doi-tac/{id}/update','\App\Http\Controllers\Admin\DoiTacController@update');
		Route::get('doi-tac/{id}/delete','\App\Http\Controllers\Admin\DoiTacController@destroy');


		Route::resource('hinh-anh','\App\Http\Controllers\Admin\HinhAnhController');
		Route::post('hinh-anh/remove','\App\Http\Controllers\Admin\HinhAnhController@remove');
		Route::post('hinh-anh/active','\App\Http\Controllers\Admin\HinhAnhController@active');
		Route::post('hinh-anh/{id}/update','\App\Http\Controllers\Admin\HinhAnhController@update');
		Route::get('hinh-anh/{id}/delete','\App\Http\Controllers\Admin\HinhAnhController@destroy');


		Route::resource('nhan-xet','\App\Http\Controllers\Admin\NhanXetController');
		Route::post('nhan-xet/remove','\App\Http\Controllers\Admin\NhanXetController@remove');
		Route::post('nhan-xet/active','\App\Http\Controllers\Admin\NhanXetController@active');
		Route::post('nhan-xet/{id}/update','\App\Http\Controllers\Admin\NhanXetController@update');
		Route::get('nhan-xet/{id}/delete','\App\Http\Controllers\Admin\NhanXetController@destroy');


		Route::resource('banner','\App\Http\Controllers\Admin\BannerController');
		Route::post('banner/remove','\App\Http\Controllers\Admin\BannerController@remove');
		Route::post('banner/active','\App\Http\Controllers\Admin\BannerController@active');
		Route::post('banner/{id}/update','\App\Http\Controllers\Admin\BannerController@update');
		Route::get('banner/{id}/delete','\App\Http\Controllers\Admin\BannerController@destroy');


		Route::resource('services','\App\Http\Controllers\Admin\ServicesController');
		Route::post('services/remove','\App\Http\Controllers\Admin\ServicesController@remove');
		Route::post('services/active','\App\Http\Controllers\Admin\ServicesController@active');
		Route::post('services/{id}/update','\App\Http\Controllers\Admin\ServicesController@update');
		Route::get('services/{id}/delete','\App\Http\Controllers\Admin\ServicesController@destroy');


		Route::resource('chuyen-muc','\App\Http\Controllers\Admin\CatePostController');
		Route::post('chuyen-muc/remove','\App\Http\Controllers\Admin\CatePostController@remove');
		Route::post('chuyen-muc/active','\App\Http\Controllers\Admin\CatePostController@active');
		Route::post('chuyen-muc/{id}/update','\App\Http\Controllers\Admin\CatePostController@update');
		Route::get('chuyen-muc/{id}/delete','\App\Http\Controllers\Admin\CatePostController@destroy');


		Route::resource('bai-viet','\App\Http\Controllers\Admin\PostController');
		Route::post('bai-viet/remove','\App\Http\Controllers\Admin\PostController@remove');
		Route::post('bai-viet/active','\App\Http\Controllers\Admin\PostController@active');
		Route::post('bai-viet/{id}/update','\App\Http\Controllers\Admin\PostController@update');
		Route::get('bai-viet/{id}/delete','\App\Http\Controllers\Admin\PostController@destroy');


		Route::resource('menu','\App\Http\Controllers\Admin\MenusController');
		Route::post('menu/{id}/update','\App\Http\Controllers\Admin\MenusController@update');
		Route::get('menu/{id}/delete','\App\Http\Controllers\Admin\MenusController@destroy');


		Route::resource('cai-dat','\App\Http\Controllers\Admin\OptionController');
		Route::post('cai-dat/{id}/update','\App\Http\Controllers\Admin\OptionController@update');
		Route::get('cai-dat/{id}/delete','\App\Http\Controllers\Admin\OptionController@destroy');


		Route::resource('feedback','\App\Http\Controllers\Admin\FeedbackController');
		Route::post('feedback/remove','\App\Http\Controllers\Admin\FeedbackController@remove');
		Route::post('feedback/active','\App\Http\Controllers\Admin\FeedbackController@active');
		Route::post('feedback/{id}/update','\App\Http\Controllers\Admin\FeedbackController@update');
		Route::get('feedback/{id}/delete','\App\Http\Controllers\Admin\FeedbackController@destroy');


		Route::resource('lien-he','\App\Http\Controllers\Admin\ContactController');
		Route::post('lien-he/remove','\App\Http\Controllers\Admin\ContactController@remove');
		Route::post('lien-he/{id}/update','\App\Http\Controllers\Admin\ContactController@update');
		Route::get('lien-he/{id}/delete','\App\Http\Controllers\Admin\ContactController@destroy');


		Route::resource('tuy-chon-hien-thi', '\App\Http\Controllers\Admin\DisplayController');
		Route::post('tuy-chon-hien-thi/so-luong-san-pham/update', '\App\Http\Controllers\Admin\DisplayController@updateItemProduct');
		Route::post('tuy-chon-hien-thi/so-luong-tin-tuc/update', '\App\Http\Controllers\Admin\DisplayController@updateItemNew');
		Route::post('tuy-chon-hien-thi/mau-sac-template-admin/update', '\App\Http\Controllers\Admin\DisplayController@updateColorAdmin');


		Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
		});
	});
});


Auth::routes();
Route::group(['middleware'=> 'auth'],function(){
	Route::resource('profile', 'ProfileController');
	Route::post('profile/{id}/update','\App\Http\Controllers\ProfileController@update');
	Route::post('profile/{id}/changPw','\App\Http\Controllers\ProfileController@changPassword');
	Route::get('profile/orderdetail/{id}','\App\Http\Controllers\ProfileController@showOrderdetail');
});
Route::get('/', 'HomeController@index');
Route::get('/san-pham.html','HomeController@tatcadanhmuc');
Route::get('/danh-muc/{slug}.html','HomeController@danhmuc');


Route::get('/danh-muc-san-pham.html','HomeController@tatcadanhmucsanpham');
Route::get('/danh-muc-san-pham/{slug}.html','HomeController@danhmucsanpham');


Route::get('/san-pham/{slug}.html','HomeController@sanpham');
Route::get('/tin-tuc.html','HomeController@tatcatintuc');
Route::get('/chuyen-muc-tin/{slug}.html','HomeController@tintuc');
Route::get('/tin-tuc/{slug}.html','HomeController@chitiettintuc');


Route::get('/banking','HomeController@banking');
Route::post('/billpayment','HomeController@billPayment');


Route::get('/lien-he.html','ContactController@index');
Route::resource('/lien-he','ContactController');


Route::resource('/request-design','RqDsignController');
Route::post('/slugCategory','\App\Http\Controllers\Admin\CategoryController@checkSlugCategory');
Route::post('/slugCatePost','\App\Http\Controllers\Admin\CatePostController@checkSlugCatePost');
Route::post('/slugPost','\App\Http\Controllers\Admin\PostController@checkSlugPost');
Route::post('/slugProduct','\App\Http\Controllers\Admin\ProductController@checkSlugProduct');
Route::post('/checkPrices','\App\Http\Controllers\HomeController@checkPrice');
Route::post('/randomPost','\App\Http\Controllers\Admin\DiscountController@getRamdomcode');
Route::get('/tim-kiem-san-pham','\App\Http\Controllers\HomeController@timkiemSanpham');
Route::post('/kiem-tra-mau-san-pham','\App\Http\Controllers\HomeController@kiemtraMausp');


Route::resource('cart', 'CartController');
Route::post('cart/{id}/update','CartController@update');
Route::get('cart/{id}/delete','CartController@destroy');
Route::get('/check-out','CartController@payMent');

Route::resource('order', 'OrderController');
Route::post('useDiscountCode','OrderController@useDiscount');


Route::get('/callback','OrderController@vtcCallback');
Route::get('/thanh-toan-loi.html','OrderController@payment_error');
Route::get('/thanh-toan-thanh-cong.html','OrderController@payment_success');
Route::get('/giao-dich-that-bai.html','OrderController@pay_error');
Route::get('/sai-thong-tin-otp.html','OrderController@pay_otp');
Route::get('/khach-hang-huy-giao-dich.html','OrderController@pay_cancel');
Route::get('/tai-khoan-bi-tru-tien.html','OrderController@pay_minus');


Route::get('sendmail', ['as' => 'getContacts' , 'uses' =>'OrderController@get_Register']);
Route::post('sendmail', ['as' =>'postContacts','uses' =>'OrderController@post_Register']);