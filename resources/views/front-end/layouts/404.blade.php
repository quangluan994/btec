@extends('front-end.layouts.master')
@section('title')
404
@stop
@section('content')

    <!-- project -->
    
        <main class="error-page">
            <div class="page-404">
                <img src="images/404.png" alt="404">
                <h2>Lỗi!, Nội dung không tìm thấy</h2>
                <a href="#">Trở về trang chủ</a>
            </div>
        </main>
    <!-- /.project -->
@stop
@section('script')
@endsection