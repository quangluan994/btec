@include('front-end.partials.header')

@yield('content')

@include('front-end.partials.footer')
