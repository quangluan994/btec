<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @yield('head')
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('front-end/node_modules/font-awesome/css/font-awesome.min.css')}}">
    <!-- Link css -->
    <link rel="stylesheet" href="{{asset('front-end/node_modules/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/node_modules/owl.carousel/dist/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/node_modules/owl.carousel/dist/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/style.css')}}">
    <link rel='stylesheet' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'>
    <script src='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js'></script>
    {!!$setting['add_code_header']!!}

    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "WebSite",
      "name" : "{{$setting['company_name']}}",
      "url": "{{ url('/')}}",
      "potentialAction": [{
        "@type": "SearchAction",
        "target": "{{url('/tim-kiem-san-pham?keyword={search_term_string}')}}",
        "query-input": "required name=search_term_string"
      }]
    }
    </script>

    <!-- Organization -->
    <script type="application/ld+json">
    { "@context" : "http://schema.org",
      "@type" : "Organization",
      "legalName" : "{{$setting['company_name']}}",
      "url" : "{{ url('/')}}",
      "contactPoint" : [{
        "@type" : "ContactPoint",
        "telephone" : "{{$setting['hot_line']}}",
        "contactType" : "customer service"
      }],
      "logo" : "{{$setting['logo']}}"
    }
    </script>
</head>

<body>
    <div id="main-page">
        <!-- header -->
        <header>
            <!-- header top -->
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6 hidden-xs">
                            <div class="contact-top">
                                <p>{{$setting['header_service']}}</p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-right fix-center">
                            <ul class="user-acction">
                                @if (Auth::guest())
                                    <li><a href="{{ route('register') }}">Đăng ký</a></li>
                                    <li><a href="{{ route('login') }}">Đăng nhập</a></li>
                                @else
                                    <li class="dropdown">
                                        <a href="{{url('profile/'.Auth::user()->id)}}">
                                          <i class="fa fa-user-circle" aria-hidden="true"></i> {{ Auth::user()->name }}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();"> Đăng xuất
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

                                @endif
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <!-- header top end-->
            <!-- header body -->
            <div class="header-body">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-5 col-md-3 col-lg-3">
                            <div class="logo"><a href="/"><img src="{{$setting['logo']}}" alt="Logo"></a></div>
                        </div>
                        <div class="col-sm-7 col-md-5 col-lg-5 hidden-xs">
                            <div class="contact-wrap">
                                <div class="contact-phone pull-left">
                                    <span class="contact-icon">
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                    </span>
                                    <div class="contact-content">
                                        <span class="name">Hotline</span>
                                        <a href="#" class="desc">{{$setting['hot_line']}}</a>
                                    </div>
                                </div>
                                <div class="contact-adress pull-left">
                                    <span class="contact-icon">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    </span>
                                    <div class="contact-content">
                                        <span class="name">{{$setting['company_province']}}</span>
                                        <a href="#" class="desc">{{$setting['company_address']}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="input-group form-header">
                                <input type="text" name="keyword" class="form-control" placeholder="Từ khóa cần tìm" >
                                <span class="input-group-btn">
                                    <button class="btn btn-default" onclick="searchrs()"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- header body end -->
            <!-- nav -->
            <div id="nav" data-spy="affix" data-offset-top="197">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-9 col-sm-9 col-md-11 col-lg-11">
                            <!-- nav desktop -->
                            <nav class="nav-desktop navbar navbar-default hidden-xs" role="navigation">
                                <ul class="nav navbar-nav">
                                    @foreach($top_menu as $item)
                                    <li>
                                        <a href="{{$item->link}}">
                                            {{$item->label}}
                                            @if($item->getsons($item->id)->count()>0)
                                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            @endif
                                        </a>
                                        @if($item->getsons($item->id)->count()>0)
                                        <ul class="sub-menu">
                                            @foreach($item->getsons($item->id) as $sub)
                                                <li><a href="{{$sub->link}}">{{$sub->label}}</a></li>
                                            @endforeach
                                        </ul>
                                        @endif
                                    </li>
                                    @endforeach
                                </ul>
                            </nav>
                            <!-- nav desktop end-->
                            <!-- nav mobile -->
                            <nav class=" nav-mobile navbar navbar-default hidden-sm hidden-md hidden-lg" role="navigation">
                                <div class="navbar-header">
                                    <a href="#" class="navbar-btn pull-left" data-toggle="collapse" data-target=".mobile-collapse">
                                        <i class="fa fa-bars" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <div class="collapse mobile-collapse">
                                    <ul class="nav navbar-nav side-nav">
                                        @foreach($top_menu as $item)
                                        <li>
                                            <a href="{{$item->link}}">
                                                {{$item->label}}
                                            </a>
                                            
                                            @if($item->getsons($item->id)->count()>0)
                                            <button class="accordion"></button>
                                            <ul class="sub-menu">
                                                @foreach($item->getsons($item->id) as $sub)
                                                <li><a href="{{$sub->link}}">{{$sub->label}}</a></li>
                                                @endforeach
                                            </ul>
                                            @endif
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </nav>
                            <!-- nav mobile -->
                        </div>
                        <div class="col-xs-3 col-sm-2 col-md-1 col-lg-1">
                            <div class="cart-wrap">
                                <a class="cart-icon" href="#">
                                    <span class="cart-count">{{Cart::countRows()}}</span>
                                    <img src="{{asset('front-end/images/cart.png')}}" alt="">
                                </a>
                                <div class="" id="cart-box">
                                    <div class="box-head">
                                        @if(Cart::countRows() == 0)
                                            <span class="cart-counter">Giỏ hàng của bạn trống</span>
                                        @else
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        <span class="cart-counter">{{Cart::count()}}</span>
                                        Sản phẩm trong giỏ hàng
                                        @endif
                                    </div>
                                    @if(Cart::countRows() > 0)
                                    <div class="box-body">
                                        <ul class="list-cart">
                                            @foreach(Cart::content() as $item)
                                            <li class="item-cart clearfix">
                                                <div class="product-thumb">
                                                    <img src="{{$item->options->img}}" alt="{{$item->name}}">
                                                </div>
                                                <div class="product-desc">
                                                    <a href="#" class="product-name">{{$item->name}}</a>
                                                    <a href="{!!url("cart/$item->rowId/delete")!!}" class="btn-delete">
                                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                    </a>
                                                    
                                                </div>
                                                <div class="product-action">
                                                    <div class="product-price">
                                                        <span>{{number_format($item->price)}} đ</span> x <span class="cart-count">{{$item->qty}}</span> = <span>{{number_format($item->subtotal)}} đ</span>
                                                    </div>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="box-footer">
                                        <div class="total-price">
                                            Tổng cộng:
                                            <span> {{Cart::total(0)}} đ</span>
                                        </div>
                                        <div class="total-btn">
                                            <a href="{{url('cart')}}"><button>Tiến hành đặt hàng</button></a>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- nav end -->
            @if(Session::has('success'))
                <div class="callout alert alert-success col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto; background-color:#00b300 ; color:white" role="alert" >
                    <button type="button" class="close">×</button> 
                    {{Session::get('success')}}
                </div>
            @endif

            @if ($errors->any())
            <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto; background-color:red ; color:white" role="alert">
                <button type="button" class="close">×</button>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </header>
        <!-- end header -->