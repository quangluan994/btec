@extends('front-end.layouts.master')

@section('head')

    <meta name="keywords" content="{{$setting['seo_keyword']}}" />

    <meta name="description" content="{{$setting['seo_description']}}" />

    <link rel="SHORTCUT ICON" href="{!!$setting['icon_website']!!}">

    <title>Mỹ phẩm</title>



    <meta property="og:title" content="Mỹ phẩm">

    <meta property="og:description" content="{{$setting['seo_description']}}">

    <meta property="og:type" content="website"/>

    <meta property="og:url" content="@php echo URL::current(); @endphp">

    <meta property="og:site_name" content="{{$setting['company_name']}}">

@stop

@section('content')

    <div class="body-page">

            <h1 class="hidden">{{$setting['seo_title']}}</h1>

            <div class="container">

                <div class="home-item">

                    <div class="row">

                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 padding-none-right">

                            <div class="owl-carousel owl-carousel-1 owl-theme">

                                <div class="item active">
                                    @if($bnfirst)
                                    <a href="{!!$bnfirst->link!!}"><img src="{!!$bnfirst->image!!}" alt="{!!$bnfirst->name!!}"></a>
                                    @endif

                                </div>
                                @if($banners)
                                @foreach($banners as $item)

                                <div class="item">

                                    <a href="{!!$item->link!!}"><img src="{!!$item->image!!}" alt="{!!$item->name!!}"></a>

                                </div>

                                @endforeach
                                @endif

                            </div>

                        </div>

                        <div class="hidden-xs hidden-sm col-md-2 col-lg-2">

                            @foreach($banner_right as $item)

                            <div class="item-row">

                                <a href="{!!$item->link!!}"><img src="{!!$item->image!!}" alt="{!!$item->name!!}"></a>

                            </div>

                            @endforeach

                        </div>

                    </div>

                </div>

                <!-- GROUP 1 -->

                @if($showProducts)

                @foreach($showProducts as $item)

                <div class="home-item">

                    <div class="item-wrap">

                        <div class="category-box">

                            <div class="category-title">

                                <h2><a href="/danh-muc/{{$item['category']->slug}}.html">{{$item['category']->name}}</a></h2>

                                <div class="view-more pull-right">

                                    {{-- <a href="/danh-muc/{{$item['category']->slug}}.html">Xem thêm</a> --}}

                                </div>

                            </div>

                            <div class="category-content">

                                <div class="row">

                                    <div class="hidden-xs hidden-sm col-md-3 col-lg-3 left-content">

                                        <div class="banner-top">

                                            <a href="/danh-muc/{{$item['category']->slug}}.html"><img src="{{$item['category']->image}}" alt="{{$item['category']->name}}"></a>

                                        </div>

                                        {{-- <div class="banner-bot">

                                            <img src="images/banner-logo1-1.png" alt="">

                                            <img src="images/banner-logo1-2.png" alt="">

                                            <img src="images/banner-logo1-3.png" alt="">

                                            <img src="images/banner-logo1-4.png" alt="">

                                            <img src="images/banner-logo1-5.png" alt="">

                                            <img src="images/banner-logo1-6.png" alt="">

                                        </div> --}}

                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 right-content">

                                        <div class="row">

                                            @foreach($item['product'] as $it)

                                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 product-item">

                                                <div class="product-box">

                                                    <div class="product-thumb">

                                                        <a href="/san-pham/{{$it->slug}}.html"><img src="{{$it->thumbnail}}" alt="{{$it->name}}"></a>

                                                        @if($it->price != $it->price_sale)

                                                        <div class="sale">-{{round(((1-(($it->price_sale)/($it->price)))*100),0)}} %</div>

                                                        @endif

                                                    </div>

                                                    <div class="product-name"><a href="/san-pham/{{$it->slug}}.html">

                                                        {{subtext(($it->name),100)}}</a>

                                                    </div>

                                                    <div class="product-price">

                                                        @if($it->qty_in_stock > 0)

                                                            @if($it->price != $it->price_sale)

                                                            <div class="price-old">{{number_format($it->price)}} đ</div>

                                                            @endif

                                                            <div class="price-new">{{number_format($it->price_sale)}} đ</div>

                                                        @else

                                                            <div class="price-new">Hết hàng</div>

                                                        @endif

                                                    </div>

                                                </div>

                                            </div>

                                            @endforeach

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                @endforeach

                @endif

                <!-- GROUP 1 end -->

                

                <!-- new  -->

                <div class="news-wrap">

                    <div class="row">

                        <div class="hidden-xs hidden-sm col-md-7 col-lg-7">

                            <div class="about-wrap">

                                <div class="about-title">

                                    <h1>{{$vecongty->name}}</h1>

                                </div>

                                <div class="content">

                                    <div class="thumb">

                                        <img src="{{$vecongty->image}}" alt="{{$vecongty->name}}">

                                    </div>

                                    <div class="content-text">

                                        {!!$vecongty->content!!}

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 blog-wrap">

                            <div class="blog-title">

                                <h4>TIN TỨC</h4>

                            </div>

                            @foreach($post_new as $item)

                            <div class="blog-item">

                                <div class="row">

                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 blog-thumb">

                                        <a href="/tin-tuc/{{$item->slug}}.html"><img src="{{$item->image}}" alt="{{$item->name}}"></a>

                                    </div>

                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 blog-content">

                                        <h5 class="blog-name">

                                            <a href="/tin-tuc/{{$item->slug}}.html">{{$item->name}}</a>

                                        </h5>

                                        <p class="blog-desc">

                                            {!!subtext(strip_tags($item->description),120)!!}

                                        </p>

                                        <a class="more-view" href="/tin-tuc/{{$item->slug}}.html">Đọc thêm <i class="fa fa-angle-right" aria-hidden="true"></i></a>

                                    </div>

                                </div>

                            </div>

                            @endforeach

                        </div>

                    </div>

                </div>

                <!-- new end -->

            </div>

        </div>

@endsection

