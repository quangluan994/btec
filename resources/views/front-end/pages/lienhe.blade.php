@extends('front-end.layouts.master')
@section('head')
    <meta name="keywords" content="{{$setting['seo_keyword']}}" />
    <meta name="description" content="{{$setting['seo_description']}}" />
    <link rel="SHORTCUT ICON" href="{!!$setting['icon_website']!!}">
    <title>Liên hệ</title>

    <meta property="og:title" content="Liên hệ">
    <meta property="og:description" content="{{$setting['seo_description']}}">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="@php echo URL::current(); @endphp">
    <meta property="og:site_name" content="{{$setting['company_name']}}">
@stop
@section('content')
	<div class="container element-page">
        <main class="lien-he">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="maps">
                        {!!$setting['company_address_map']!!}
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="send-contact">
                        <form action="{{url('/lien-he')}}" method="post">
                            {{csrf_field()}}
                            <h2>Nội dung</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Tên <span>*</span></label>
                                        <input type="text" name="name" id="" class="form-control" placeholder="Họ tên của bạn" aria-describedby="helpId">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Email <span>*</span></label>
                                        <input type="email" name="email" id="" class="form-control" placeholder="Nhập email của bạn" aria-describedby="helpId">
                                    </div>
                                </div>
                                    <div class="form-group" style="display: none">
                                        <input type="text" name="url" id="url" value="{{URL::current()}}">
                                        <input type="text" name="status" id="status" value="chua_xu_ly">
                                    </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Viết bình luận <span>*</span></label>

                                        <textarea class="form-control" name="content" id="" rows="3"></textarea>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-submit">Gửi liên hệ</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="contact">
                        <h2>Chúng tôi ở đây</h2>
                        <p class="head">
                            <strong>{{$setting['company_province']}}</strong>
                            <span>{{$setting['company_address']}}</span>
                        </p>
                        <p>
                            <strong>Số điện thoại:</strong>
                            <a href="#">{{$setting['hot_line']}}</a>
                        </p>
                        <p>
                            <strong>Liên hệ:</strong>
                            <a href="#">{{$setting['company_email']}}</a>
                        </p>
                    </div>
                </div>
            </div>

        </main>
    </div>
@stop
@section('script')
@endsection