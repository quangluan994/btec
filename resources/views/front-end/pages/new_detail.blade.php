@extends('front-end.layouts.master')
@section('head')
    <meta name="keywords" content="{{$setting['seo_keyword']}}" />
    <meta name="description" content="{{$setting['seo_description']}}" />
    <link rel="SHORTCUT ICON" href="{!!$setting['icon_website']!!}">
    <title>{{$setting['seo_title']}}</title>

    <meta property="og:title" content="{{$setting['seo_title']}}">
    <meta property="og:description" content="{{$setting['seo_description']}}">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="@php echo URL::current(); @endphp">
    <meta property="og:site_name" content="{{$setting['company_name']}}">
@stop
@section('content')
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "Product",
    "name": "{{$post->name}}",
    "image" : "{{$post->thumbnail}}",
    "description" : "{{($post->seo_description) ? $post->seo_description : $post->name}}",
    "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": "4.8",
        "reviewCount": "{{$post->id}}"
    },
    "brand": {
        "@type": "Thing",
        "name": "{{$setting['company_name']}}"
    }
}
</script>
<script type="application/ld+json">
{
 "@context": "http://schema.org",
 "@type": "BreadcrumbList",
 "itemListElement":
 [
  {
   "@type": "ListItem",
   "position": 1,
   "item":
   {
    "@id": "{{url('/')}}",
    "name": "Trang chủ"
    }
  },
  {
    "@type": "ListItem",
    "position": 2,
    "item":
     {
       "@id": "{{url('/tin-tuc.html')}}",
       "name": "Tin tức"
     }
  },
  {
    "@type": "ListItem",
    "position": 3,
    "item":
     {
       "@id": "{{url('/')}}",
       "name": "{{$post->name}}"
     }
    }
 ]
}
</script>
    <div class="body-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 right-content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 breadcrumb-wrap">
                            <ul class="breadcrumb">
                                <li><a href="/">Trang chủ</a></li>
                                <li><a href="/chuyen-muc-tin/{{$post->cate->slug}}.html">{{$post->cate->name}}</a></li>
                                <li><strong>{{$post->name}}</strong></li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 news-content">
                            <article>
                                <div class="mega-news">
                                    <h1 class="mega-name">
                                        {{$post->name}}
                                    </h1>
                                    <div class="mega-infor">
                                        <span><i class="fa fa-clock-o" aria-hidden="true"></i> {{$post->created_at->format('d/m/Y')}}</span>
                                    </div>
                                    <div class="content">
                                        {!!$post->content!!}
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 left-content">
                    @if($post_highlight)
                    <div class="feature-blog">
                        <h4 class="title">
                            Tin nổi bật
                        </h4>
                        <div class="content">
                            @foreach($post_highlight as $item)
                            <div class="blog-item">
                                <i class="fa fa-circle icon-blog" aria-hidden="true"></i>
                                <div class="blog-content">
                                    <a href="/tin-tuc/{{$item->slug}}.html">{{$item->name}}</a>
                                    <span>Ngày đăng: {!! $item->created_at->format('d-m-Y') !!}</span>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endif
                    <div class="feature-product">
                        <h4 class="title">
                            Sản phẩm nổi bật
                        </h4>
                        <div class="content">
                            @foreach($productnew as $item)
                            <div class="product-item">
                                <div class="row">
                                    <div class="col-md-5 col-sm-5 product-thumb">
                                        <a href="/san-pham/{{$item->slug}}.html"><img src="{{$item->thumbnail}}" alt="{{$item->name}}"></a>
                                    </div>
                                    <div class="col-md-7 col-sm-7 product-content">
                                        <a class="name" href="/san-pham/{{$item->slug}}.html">{{subtext(($item->name),25)}}</a>
                                        <span class="price">{{number_format($item->price_sale)}} đ</span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('script')
@endsection