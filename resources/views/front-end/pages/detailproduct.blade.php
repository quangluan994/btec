@extends('front-end.layouts.master')
@section('head')
    <meta name="keywords" content="{{$setting['seo_keyword']}}" />
    <meta name="description" content="{{$setting['seo_description']}}" />
    <link rel="SHORTCUT ICON" href="{!!$setting['icon_website']!!}">
    <title>{{$setting['seo_title']}}</title>

    <meta property="og:title" content="{{$setting['seo_title']}}">
    <meta property="og:description" content="{{$setting['seo_description']}}">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="@php echo URL::current(); @endphp">
    <meta property="og:site_name" content="{{$setting['company_name']}}">
@stop
@section('content')
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "Product",
    "name": "{{$product->name}}",
    "image" : "{{$product->thumbnail}}",
    "description" : "{{($product->seo_description) ? $product->seo_description : $product->name}}",
    "aggregateRating": {
        "@type": "AggregateRating",
        "ratingValue": "4.8",
        "reviewCount": "{{$product->id}}"
    },
    "brand": {
        "@type": "Thing",
        "name": "{{$setting['company_name']}}"
    },
    "offers": {
        "@type": "Offer",
        "availability": "http://schema.org/InStock",
        "price": "{{number_format($product->price_sale)}}",
        "priceCurrency": "VND"
    }
}
</script>
<script type="application/ld+json">
{
 "@context": "http://schema.org",
 "@type": "BreadcrumbList",
 "itemListElement":
 [
  {
   "@type": "ListItem",
   "position": 1,
   "item":
   {
    "@id": "{{url('/')}}",
    "name": "Trang chủ"
    }
  },
  {
    "@type": "ListItem",
    "position": 2,
    "item":
     {
       "@id": "{{url('/san-pham.html')}}",
       "name": "Sản phẩm"
     }
  },
  {
    "@type": "ListItem",
    "position": 3,
    "item":
     {
       "@id": "{{url('/')}}",
       "name": "{{$product->name}}"
     }
    }
 ]
}
</script>
	<div class="body-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 right-content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 breadcrumb-wrap">
                            <ul class="breadcrumb">
                                <li><a href="/">Trang chủ</a></li>
                                <li><a href="/danh-muc/{{$product->cate->slug}}.html">{{$product->cate->name}}</a></li>
                                <li><strong>{{subtext(($product->name),140)}}</strong></li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 detail-content">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 carousel-detail">
                                    <div id="carousel-id" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                <img src="{{$product->thumbnail}}" class="" alt="{{$product->thumbnail}}">
                                            </div>
                                            @php 
                                                $a = $product->gallery;
                                                $b = json_decode($a,true);
                                                for($i=0;$i<count($b);$i++)
                                                {
                                                    echo '<div class="item"><img src="'.$b[$i].'" class="" alt=""></div>';
                                                }
                                            @endphp
                                        </div>
                                        <a class="left carousel-control" href="#carousel-id" data-slide="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                                        <a class="right carousel-control" href="#carousel-id" data-slide="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        <div>
                                            <div class="owl-carousel owl-carousel-detail-1 owl-theme">
                                                <div class="item">
                                                    <img data-target="#carousel-id" data-slide-to="0" src="{{$product->thumbnail}}" alt="">
                                                </div>
                                                @php 
                                                    $a = $product->gallery;
                                                    $b = json_decode($a,true);
                                                    for($i=0;$i<count($b);$i++)
                                                    {
                                                        echo '<div class="item"><img data-target="#carousel-id" data-slide-to="'.($i+1).'" src="'.$b[$i].'" alt=""></div>';
                                                    }
                                                @endphp
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 product-detail">
                                    <h1 class="detail-name">{{$product->name}}</h1>
                                    <div class="detail-price">
                                        <span>{{number_format($product->price_sale)}} đ</span>
                                    </div>
                                    <form action="{!!url("cart")!!}" method="post">
                                        {{csrf_field()}}
                                        <input type="hidden" name="product_id" value="{{$product->id}}">
                                        <input type="hidden" name="product_name" value="{{$product->name}}">
                                        <input type="hidden" name="product_price" value="{{$product->price_sale}}">
                                        <input type="hidden" name="url" value="@php 
                                        echo URL::current();
                                        @endphp">
                                        <input type="hidden" name="product_thumbnail" value="{{$product->thumbnail}}">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="detail-quantity">
                                                    <label>
                                                        Số lượng:
                                                    </label>
                                                    <div class="quantity-box">
                                                        <input type="number" class="form-control" name="product_quantity" value="1" id="numb" min="1">
                                                        <div class="group-btn">
                                                            <a href="javascript:void(0)" type="button" class="btn-quantity fa fa-angle-up" onclick="numble_up()"></a>
                                                            <a href="javascript:void(0)" type="button" class="btn-quantity fa fa-angle-down" onclick="numble_down()"></a>
                                                        </div>
                                                    </div>
                                                    <script type="text/javascript">
                                                        function numble_up(){
                                                            var quantity = document.getElementById("numb").value;
                                                            if (!isNaN(quantity)) {
                                                                quantity++;
                                                                document.getElementById("numb").setAttribute("value",quantity);
                                                            }
                                                            
                                                        }
                                                        function numble_down(){
                                                            var quantity = document.getElementById("numb").value;
                                                            if (!isNaN(quantity) && quantity>1) {
                                                                quantity--;
                                                                document.getElementById("numb").setAttribute("value",quantity);
                                                            }
                                                        }
                                                    </script>
                                                </div>
                                            </div>
                                            @if($product->color->count()>0)
                                            <div class="col-md-6">
                                                <ul class="fil_color">
                                                    @php
                                                    $i=1;
                                                    @endphp
                                                    @foreach($product->color as $item)
                                                    <li>
                                                        <input type="radio" id="color-{{$i}}" name="colorcheck" value="{{$item->id}}" class="radioColor" onclick="colorChecked()" @php echo ($i==1)? 'checked' :''; @endphp>
                                                        <label class="button" for="color-{{$i}}" style="background-color:{{$item->color}}">
                                                            <span></span>
                                                        </label>
                                                    </li>
                                                    @php
                                                    $i++;
                                                    @endphp
                                                    @endforeach
                                                </ul>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="detail-group-btn">
                                            <div class="btn-hotline fix-width">
                                                <a href="#">Hỗ trợ: {{$setting['hot_line']}}</a>
                                            </div>
                                            <div id="clickaddCart" class=" fix-width">
                                                @if($product->qty_in_stock > 0)
                                                <div style="color:white">
                                                    <button type="submit" class="btn-buy">
                                                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>Mua hàng
                                                    </button>
                                                </div>
                                                @else
                                                    <div class="btn-hotline btn btn-warning" style="border:none"> Sản phẩm đã hết hàng
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 detail-desc">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#home">Thông tin sản phẩm</a></li>
                                <li class=""><a data-toggle="tab" href="#home1">Bình luận</a></li>
                            </ul>
                            
                            <div class="tab-content">
                                <div id="home" class="tab-pane fade in active">
                                    {!!$product->description!!}
                                </div>
                                <div id="home1" class="fb-comments tab-pane fade in" data-href="@php 
                                    echo URL::current();
                                @endphp" data-numposts="10" data-width="100%"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hidden-xs hidden-sm col-md-3 col-lg-3 left-content">
                    <div class="delivery-wrap">
                        <div class="delivery-title">
                            <h4>CHÍNH SÁCH GIAO HÀNG</h4>
                        </div>
                        <div class="delivery-content">
                            @foreach($delivery_menu as $item)
                            <div class="delivery-item">
                                {!!$item->class!!}
                                <span><a href="{{$item->link}}">{{$item->label}}</a></span>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    
                    <div class="side-category">
                        <div class="side-title">
                            <h4><i class="fa fa-bars" aria-hidden="true"></i>Tất cả sản phẩm</h4>
                        </div>
                        <ul class="side-content">
                            @foreach($cate as $item)
                            <li>
                                <a href="/danh-muc/{{$item->slug}}.html">
                                    {{$item->name}}
                                </a>
                                @if($item->sub_cate->count()>0)
                                <button class="accordion"></button>
                                <ul class="sub-menu">
                                    @foreach($item->sub_cate as $it)
                                    <li><a href="/danh-muc/{{$it->slug}}.html">{{$it->name}}</a></li>
                                    @endforeach
                                </ul>
                                @endif
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    
                </div>
                @if($care->count()>0)
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 more-product">
                    <div class="home-item">
                        <div class="item-wrap">
                            <div class="category-box">
                                <div class="category-title">
                                    <h2>SẢN PHẨM LIÊN QUAN</h2>
                                </div>
                                <div class="category-content">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 right-content">
                                            <div class="row">
                                                @if($care)
                                                @foreach($care as $item)
                                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 product-item">
                                                    <div class="product-box">
                                                        <div class="product-thumb">
                                                            <img src="{{$item->thumbnail}}" alt="{{$item->name}}">
                                                        </div>
                                                        <div class="product-name"><a href="/san-pham/{{$item->slug}}.html">
                                                            {{$item->name}}
                                                        </a></div>
                                                        <div class="product-price">
                                                            <div class="price-new">{{number_format($item->price_sale)}} ₫</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    
@stop
@section('script')
    <script>
    function colorChecked(){
        var product_id = $('input[name=product_id]').val();
        var color_id = $('input[name=colorcheck]:checked').val();
        console.log(color_id);
        var token = $('input[name=_token]').val();
        if(product_id) {
            $.ajax({
                type: 'post',
                url: '/kiem-tra-mau-san-pham',
                headers: { 'X-XSRF-TOKEN' : token },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', token);
                },
                data: {type: 'ajax', _csrfToken : token, product_id:product_id,color_id:color_id},
                success: function(data) {
                    data = $.parseJSON(data);
                    $('#clickaddCart').html(data);
                },
                error: function(error) {
                console.log('error');
                }
            });
        }
    };
</script>
@endsection