@extends('front-end.layouts.master')
@section('head')
    <meta name="keywords" content="{{$setting['seo_keyword']}}" />
    <meta name="description" content="{{$setting['seo_description']}}" />
    <link rel="SHORTCUT ICON" href="{!!$setting['icon_website']!!}">
    <title>{{$setting['seo_title']}}</title>

    <meta property="og:title" content="{{$setting['seo_title']}}">
    <meta property="og:description" content="{{$setting['seo_description']}}">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="@php echo URL::current(); @endphp">
    <meta property="og:site_name" content="{{$setting['company_name']}}">

@stop
@section('content')
        
    <div class="cart-page body-page">
        <div class="container">
            <div class="head">
                <h1>GIỎ HÀNG</h1>
            </div>
            @if(Session::has('danger'))
                <div class="alert alert-danger">{{Session::get('danger')}}
                </div>
            @endif
            <div class="row">
                <div class="col-lg-9">
                    <table class="table table-left">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Sản phẩm</th>
                                <th style="white-space:nowrap;">Ghi chú</th>
                                <th>Giá</th>
                                <th>Số lượng</th>
                                <th>Tổng cộng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(Cart::countRows() > 0)
                            @foreach(Cart::content() as $item)
                            <tr>
                                <td scope="row"><a href="{!!url("cart/$item->rowId/delete")!!}" class="btn-delete"><button><i class="fa fa-times"></i></button></a></td>
                                <td><img src="{{$item->options->img}}" alt="{{$item->name}}"></td>
                                <td><a href="#">{{subtext(($item->name),100)}}</a></td>
                                <td>{{$item->options->color}}</td>
                                <td class="nowrap">{{number_format($item->price)}} đ</td>
                                <td>
                                    <form action="cart/{{$item->rowId}}/update" method="post">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <input type="number" name="qty" id="" value="{{$item->qty}}">
                                            <button type="submit"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                                        </div>
                                    </form>
                                </td>
                                <td class="nowrap">{{number_format($item->subtotal)}} đ</td>
                            </tr>
                            @endforeach
                            @else
                                <tr>
                                    <td colspan="6">Không có sản phẩm nào trong giỏ hàng</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-3">
                    <table class="table table-right">
                        <thead>
                            <tr>
                                <th colspan="2" class="text-center">Tổng số lượng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">Số lượng:</td>
                                <td>{{Cart::count()}}</td>
                            </tr>
                            <tr>
                                <td scope="row">Tổng cộng:</td>
                                <td class="nowrap">{{Cart::total(0)}} đ</td>
                            </tr>
                            @if(Cart::countRows() > 0)
                            <tr>
                                <td scope="row" colspan="2" class="text-center">
                                    <a href="/check-out"><button class="submit">Thanh toán</button></a>
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>

            <div class=" more-product">
                    <div class="home-item">
                        <div class="item-wrap">
                            <div class="category-box">
                                <div class="category-title">
                                    <h2><a href="#">SẢN PHẨM MỚI</a></h2>
                                </div>
                                <div class="category-content">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 right-content">
                                            <div class="row">
                                                @if($product_new)
                                                @foreach($product_new as $item)
                                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 product-item">
                                                    <div class="product-box">
                                                        <div class="product-thumb">
                                                            <a href="/san-pham/{{$item->slug}}.html"><img src="{{$item->thumbnail}}" alt="{{$item->name}}"></a>
                                                        </div>
                                                        <div class="product-name"><a href="/san-pham/{{$item->slug}}.html">
                                                                {{subtext(($item->name),100)}}
                                                            </a></div>
                                                        <div class="product-price">
                                                            @if($item->qty_in_stock > 0)
                                                            @if($item->price != $item->price_sale)
                                                                <div class="price-old">{{number_format($item->price)}} đ</div>
                                                            @endif
                                                                <div class="price-new">{{number_format($item->price_sale)}} đ</div>
                                                            @else
                                                                <div class="price-new">Hết hàng</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>

@stop
@section('script')
@endsection
