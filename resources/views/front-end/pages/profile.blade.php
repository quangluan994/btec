@extends('front-end.layouts.master')
@section('head')
    <meta name="keywords" content="{{$setting['seo_keyword']}}" />
    <meta name="description" content="{{$setting['seo_description']}}" />
    <link rel="SHORTCUT ICON" href="{!!$setting['icon_website']!!}">
    <title>Thông tin cá nhân</title>

    <meta property="og:title" content="Liên hệ">
    <meta property="og:description" content="{{$setting['seo_description']}}">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="@php echo URL::current(); @endphp">
    <meta property="og:site_name" content="{{$setting['company_name']}}">
@stop
<script>
    function editProfile() {
        $('#tbl-edit').show();
        $('#tbl-historyorder').hide();
    }

    function viewHistoryOrder(){
        $('#tbl-info').hide();
        $('#tbl-edit').hide();
        $('#tbl-historyorder').show();
    }
</script>
@section('content')
	<div class="container element-page">
        <div class="row">
            <div class="carousel-heading no-margin" style="text-align: center">
                <h2>Thông tin cá nhân</h2>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 register-account">
                <div class="page-content col-lg-12 col-md-12 col-sm-12" style="text-align: center; margin-bottom: 30px;">
                    <img src="{{($user->avatar) ? $user->avatar : '../uploadfile/user.png'}}" style="border-radius: 50%" width="150" height="150" /><br/>
                    <h3>{{$user->name}}</h3>
                    <h4 style="text-align: center; color: blue"><a href="javascript:;" onclick="editProfile()" >Thay đổi mật khẩu</a></h4>
                    <h4 style="text-align: center; color: blue"><a href="javascript:;" onclick="viewHistoryOrder()">Xem lịch sử mua hàng</a></h4>
                </div>


                <div class="page-content col-lg-12 col-md-12 col-sm-12" id="tbl-edit" style="display: none">
                    <section class="content">
                        {{-- @if ($errors->any())
                        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
                            <button type="button" class="close">×</button>
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif --}}

                        <div class="row">
                            <div class="col-xs-12">
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                   
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-12">
                                    <div id="changPass">
                                        <form class="form-horizontal login_validator" action="{!!url("profile")!!}/{{ $user->id }}/changPw" method="post">
                                            {{csrf_field()}}
                                            <div class="form-group">
                                                 <label>Mật khẩu cũ</label>
                                                 <input type="password" class="form-control passworduser"  name="oldpassword" required>
                                            </div> 
                                            <div class="form-group">
                                                 <label style="margin-bottom: 25px">Mật khẩu mới</label>
                                                 <input type="password" class="form-control passworduser"  name="passwordnew" required>
                                            </div>
                                            <div class="form-group">
                                                 <label style="margin-bottom: 25px">Nhập lại mật khẩu:</label>
                                                 <input type="password" class="form-control passworduser" name="passwordAgain" required>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-3 col-lg-8">
                                                <div class="col-lg-9 push-lg-3">
                                                    <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Cập nhật</button>
                                                    <button class="btn btn-warning" type="reset"><i class="fa fa-refresh"></i> Nhập lại</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="page-content col-lg-12 col-md-12 col-sm-12" id="tbl-historyorder">
                    <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>Mã đơn hàng</th>
                              <th>Giảm giá (%)</th>
                              <th>Tổng tiền </th>
                              <th>Phương thức thanh toán</th>
                              <th>Trạng thái đơn hàng</th>
                              <th>Xem chi tiết</th>
                          </tr>
                      </thead>
                      <tbody>
                        @if($order)
                        @foreach($order as $item)
                            <tr>
                                <td style="padding:10px">PAY.ORDER.{{addchart($item->id)}}</td>
                                <td style="padding:10px">{{$item->discount}}</td>
                                <td style="padding:10px">{{number_format($item->total_amount)}} đ</td>
                                <td style="padding:10px">
                                    @if($item->pay_method == 'store')
                                        Tại cửa hàng
                                    @elseif($item->pay_method == 'cod')
                                        COD
                                    @else
                                        Qua cổng thanh toán
                                    @endif
                                </td>
                                <td style="padding:10px">
                                    @if($item->status == 'dang_cho')
                                        Đang chờ xử lý
                                    @elseif($item->status == 'da_giao_hang')
                                        Đã giao hàng
                                    @elseif($item->status == 'chua_thanh_toan')
                                        Thanh toán thất bại
                                    @else
                                        Thanh toán thành công
                                    @endif
                                </td>
                                <td style="padding:10px">
                                    <a href="{{url('profile/orderdetail/'.$item->id)}}" class="btn btn-success" title=""><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        @else
                        <tr><td colspan="6">Bạn chưa có đơn hàng nào</td></tr>
                        @endif
                      </tbody>
                    </table>

                    <div style="clear:both"></div>
                    <div class="row col-lg-7 col-md-6 col-sm-6" style="float:right">
                        {{$order->links()}}
                    </div>
                </div>
              
            </div>

        </div>
    </div>

    
@endsection