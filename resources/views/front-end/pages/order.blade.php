@extends('front-end.layouts.master')
@section('head')
    <meta name="keywords" content="{{$setting['seo_keyword']}}" />
    <meta name="description" content="{{$setting['seo_description']}}" />
    <link rel="SHORTCUT ICON" href="{!!$setting['icon_website']!!}">
    <title>{{$setting['seo_title']}}</title>

    <meta property="og:title" content="{{$setting['seo_title']}}">
    <meta property="og:description" content="{{$setting['seo_description']}}">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="@php echo URL::current(); @endphp">
    <meta property="og:site_name" content="{{$setting['company_name']}}">
@stop
@section('content')
    @if($order)   
    <div class="orders-page">
        <div class="container">
            <div class="head">
                <h1>ĐƠN HÀNG ĐÃ NHẬN</h1>
                <p>Cảm ơn bạn. Đơn hàng của bạn đã được nhận thành công.</p>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <table class="table table-head">
                        <tbody>
                            <tr>
                                <td scope="row">MÃ ĐƠN HÀNG:</td>
                                <td>NGÀY:</td>
                                @if($order->discount != 0)
                                <td>GIẢM GIÁ(%)</td>
                                @endif
                                <td>TỔNG CỘNG:</td>
                                <td>PHƯƠNG THỨC THANH TOÁN:</td>
                            </tr>
                            <tr>
                                <td scope="row">PAY.ORDER.{{addchart($order->id)}}</td>
                                <td>{{$order->created_at->format('d-m-Y')}}</td>
                                @if($order->discount != 0)
                                <td>{{$order->discount}}%</td>
                                @endif
                                <td>{{number_format($order->total_amount)}} đ</td>
                                <td>
                                    @php
                                    $i = $order->pay_method;
                                    if($i == 'store')
                                        echo 'Thanh toán trực tiếp tại cửa hàng';
                                    elseif($i == 'cod')
                                        echo 'Trả tiền mặt khi giao hàng(COD)';
                                    else
                                        echo 'Thanh toán qua cổng thanh toán vtcPay';
                                    @endphp
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-8">
                    <h2>Chi tiết đơn hàng</h2>
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Sản phẩm</th>
                                <th>Số lượng</th>
                                <th>Tổng cộng</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($order->orderdetail as $item)
                            <tr>
                                <td style="width:150px"><img src="{{$item->product->thumbnail}}" width="120px"></td>
                                <td scope="row">{{$item->product->name}}</td>
                                <td>{{$item->quantity}}</td>
                                <td> {{number_format(($item->price_sale)*($item->quantity))}} đ</td>
                            </tr>
                            @endforeach
                            <tr style="font-weight: bolder; color:red">
                                <td scope="row" colspan="3">Phương thức thanh toán</td>
                                <td>Kiểm tra thanh toán</td>
                            </tr>
                            @if($order->discount != 0)
                            <tr style="font-weight: bolder; color:red">
                                <td scope="row" colspan="3">Giảm giá</td>
                                <td>{{$order->discount}}%</td>
                            </tr>
                            @endif
                            <tr style="font-weight: bolder; color:red">
                                <td scope="row" colspan="3">Tổng</td>
                                <td >{{number_format($order->total_amount)}} đ</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @else

        <main class="error-page">
            <div class="page-404">
                <img src="images/404.png" alt="404">
                <h2>Lỗi!, Nội dung không tìm thấy</h2>
                <a href="#">Trở về trang chủ</a>
            </div>
        </main>
    @endif

@stop
@section('script')
@endsection
