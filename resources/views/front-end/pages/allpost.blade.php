@extends('front-end.layouts.master')
@section('head')
    <meta name="keywords" content="{{$setting['seo_keyword']}}" />
    <meta name="description" content="{{$setting['seo_description']}}" />
    <link rel="SHORTCUT ICON" href="{!!$setting['icon_website']!!}">
    <title>Tin tức làm đẹp</title>

    <meta property="og:title" content="Tin tức làm đẹp">
    <meta property="og:description" content="{{$setting['seo_description']}}">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="@php echo URL::current(); @endphp">
    <meta property="og:site_name" content="{{$setting['company_name']}}">
@stop
@section('content')
	<div class="body-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 right-content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 breadcrumb-wrap">
                            <ul class="breadcrumb">
                                <li><a href="/">Trang chủ</a></li>
                                <li><strong>Tin tức</strong></li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 news-content">
                            @foreach($posts as $item)
                            <div class="news-item">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 news-thumb">
                                        <a href="/tin-tuc/{{$item->slug}}.html"><img src="{{$item->image}}" alt="{{$item->name}}"></a>
                                    </div>
                                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 news-sumary">
                                        <h3 class="name">
                                            <a href="/tin-tuc/{{$item->slug}}.html">{{subtext(($item->name),150)}}</a>
                                        </h3>
                                        <div class="infor">
                                            Ngày đăng: {!! $item->created_at->format('d-m-Y') !!}
                                        </div>
                                        <div class="desc">{!!strip_tags(subtext(($item->description),250))!!}</div>
                                        <a href="/tin-tuc/{{$item->slug}}.html" class="btn-view">
                                            Xem chi tiết<i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="pagination-wrap">
                            {{$posts->links()}}
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 left-content">
                    @if($post_highlight)
                    <div class="feature-blog">
                        <h4 class="title">
                            Tin nổi bật
                        </h4>
                        <div class="content">
                            @foreach($post_highlight as $item)
                            <div class="blog-item">
                                <i class="fa fa-circle icon-blog" aria-hidden="true"></i>
                                <div class="blog-content">
                                    <a href="/tin-tuc/{{$item->slug}}.html">{{$item->name}}</a>
                                    <span>Ngày đăng: {!! $item->created_at->format('d-m-Y') !!}</span>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endif
                    <div class="feature-product">
                        <h4 class="title">
                            Sản phẩm mới
                        </h4>
                        <div class="content">
                            @foreach($productnew as $item)
                            <div class="product-item">
                                <div class="row">
                                    <div class="col-md-5 col-sm-5 product-thumb">
                                        <a href="/san-pham/{{$item->slug}}.html"><img src="{{$item->thumbnail}}" alt="{{$item->name}}"></a>
                                    </div>
                                    <div class="col-md-7 col-sm-7 product-content">
                                        <a class="name" href="/san-pham/{{$item->slug}}.html">{{subtext(($item->name),25)}}</a>
                                        <span class="price">{{number_format($item->price_sale)}} đ</span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
@endsection