@extends('front-end.layouts.master')
@section('head')
    <meta name="keywords" content="{{$setting['seo_keyword']}}" />
    <meta name="description" content="{{$setting['seo_description']}}" />
    <link rel="SHORTCUT ICON" href="{!!$setting['icon_website']!!}">
    <title>{{$setting['seo_title']}}</title>

    <meta property="og:title" content="{{$setting['seo_title']}}">
    <meta property="og:description" content="{{$setting['seo_description']}}">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="@php echo URL::current(); @endphp">
    <meta property="og:site_name" content="{{$setting['company_name']}}">
@stop
@section('content')
       
    <div class="payment-page body-page">
        <div class="container">
            <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="col-12">
                            <h5>Mã đơn hàng: <b>PAY.ORDER.{{addchart($order->id)}}</b></h5>
                            <h5>Họ tên người nhận: <b>{{$order->receiver_name}}</b></h5>
                            <h5>Địa chỉ giao hàng: <b>{{$order->receiver_address}}</b></h5>
                            <h5>Số điện thoại người nhận: <b>{{$order->receiver_phone_number}}</b></h5>
                            <h5>Hình thức thanh toán: <b>
                                @if($order->pay_method == 'store')
                                    Thanh toán trực tiếp tại cửa hàng
                                @elseif($order->pay_method == 'cod')
                                    Trả tiền mặt khi giao hàng
                                @else
                                    Thanh toán qua cổng thanh toán vtcPay
                                @endif
                            </b></h5>
                            <table class="table table-striped" style="margin-top: 50px;">
                                <tr>
                                    <th>STT</th>
                                    <th style="width:180px;">Hình ảnh sản phẩm</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Số lượng</th>
                                    <th>Đơn giá</th>
                                    <th>Ghi chú</th>
                                    <th>Thành tiền</th>
                                </tr>
                                @if($orderdetail)
                                    @php
                                    $stt = 1;
                                    @endphp
                                    @foreach($orderdetail as $item)
                                        <tr>
                                            <td>{{$stt}}</td>
                                            <td><img src="{!! $item->product->thumbnail !!}" alt="{!! $item->product->name !!}" width="150"></td>
                                            <td>{{$item->product->name}}</td>
                                            <td>{{$item->quantity}}</td>
                                            <td>{{number_format($item->price_sale)}} đ</td>
                                            <td>{{$item->color}}</td>
                                            <td>{{number_format(($item->price_sale)*($item->quantity))}} đ</td>
                                        </tr>
                                    @php
                                    $stt++;
                                    @endphp
                                    @endforeach
                                    @if($order->discount !=0)
                                    <tr>
                                        <td colspan="6" class="text-center"><strong>Giảm giá(%):</strong></td>
                                        <td style="color:red"><strong>{{$order->discount}}%</strong></td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td colspan="6" class="text-center"><strong>Tổng cộng:</strong></td>
                                        <td style="color:red"><strong>{{number_format($item->order->total_amount)}} đ</strong></td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>

@stop
@section('script')
@endsection
