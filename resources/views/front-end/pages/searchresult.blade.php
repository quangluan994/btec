@extends('front-end.layouts.master')
@section('head')
    <meta name="keywords" content="{{$setting['seo_keyword']}}" />
    <meta name="description" content="{{$setting['seo_description']}}" />
    <link rel="SHORTCUT ICON" href="{!!$setting['icon_website']!!}">
    <title>Kết quả tìm kiếm</title>

    <meta property="og:title" content="Tìm kiếm sản phẩm làm đẹp">
    <meta property="og:description" content="{{$setting['seo_description']}}">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="@php echo URL::current(); @endphp">
    <meta property="og:site_name" content="{{$setting['company_name']}}">
@stop
@section('content')
    <h1 class="hidden">Kết quả tìm kiếm</h1>
    <div class="body-page">
        <div class="container">
            <div class="row">
                <div class="hidden-xs hidden-sm col-md-3 col-lg-3 left-content">
                    
                    <div class="side-category">
                        <div class="side-title">
                            <h4><i class="fa fa-bars" aria-hidden="true"></i>Tất cả sản phẩm</h4>
                        </div>
                        <ul class="side-content">
                            @foreach($cate as $item)
                            <li>
                                <a href="/danh-muc/{{$item->slug}}.html">
                                    {{$item->name}}
                                </a>
                                @if($item->sub_cate->count()>0)
                                <button class="accordion"></button>
                                <ul class="sub-menu">
                                    @foreach($item->sub_cate as $it)
                                    <li><a href="/danh-muc/{{$it->slug}}.html">{{$it->name}}</a></li>
                                    @endforeach
                                </ul>
                                @endif
                            </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="search-price">
                        <div class="title">
                            <span>Khoảng giá</span>
                        </div>
                        <div class="content">
                            <form action="/tim-kiem-san-pham" method="get">
                                <div class="form-group clearfix">
                                    <select class="form-control" name="filter_price" id="">
                                        <option value="all">--Chọn khoảng giá--</option>
                                        <option value="duoi-500000">Dưới 500.000đ</option>
                                        <option value="tu-500000-den-1000000">500.000đ- 1.000.000đ</option>
                                        <option value="tu-1000000-den-1500000">1.000.000đ- 1.500.000đ</option>
                                        <option value="tu-1500000-den-2000000">1.500.000đ- 2.000.000đ</option>
                                        <option value="tu-2000000-den-2500000">2.000.000đ- 2.500.000đ</option>
                                        <option value="tu-2500000-den-3000000">2.500.000đ- 3.000.000đ</option>
                                        <option value="tren-3000000">Trên 3.000.000đ</option>
                                    </select>
                                    <button type="submit" class="btn btn-submit"><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 right-content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 breadcrumb-wrap">
                            <ul class="breadcrumb">
                                <li><a href="/">Trang chủ</a></li>
                                <li><strong>Kết quả tìm kiếm</strong></li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 list-product">
                            {{-- <h4>Có {{$products->count()}} kết quả tìm kiếm phù hợp với từ khóa : "{{$keyword}}"</h4>
                            <input type="hidden" name="keyword1" value="{{$keyword}}"> --}}
                            <div class="row">
                                @foreach($products as $item)
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 product-item">
                                    <div class="product-box">
                                        <div class="product-thumb">
                                            <img src="{{$item->thumbnail}}" alt="{{$item->name}}">
                                            @if($item->price_sale != $item->price)
                                            <div class="sale">-{{round(((1-(($item->price_sale)/($item->price)))*100),0)}} %</div>
                                            @endif
                                        </div>
                                        <div class="product-name"><a href="/san-pham/{{$item->slug}}.html">
                                           {{subtext(($item->name),100)}}
                                        </a></div>
                                        <div class="product-price">
                                            
                                            @if($item->qty_in_stock > 0)
                                                @if($item->price_sale != $item->price)
                                                <div class="price-old">{{number_format($item->price)}} đ</div>
                                                @endif
                                                <div class="price-new">{{number_format($item->price_sale)}} đ</div>
                                            @else
                                                <div class="price-new">Hết hàng</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="pagination-wrap">
                            {{$products->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
@endsection