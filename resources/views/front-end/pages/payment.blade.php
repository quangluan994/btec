@extends('front-end.layouts.master')
@section('head')
    <meta name="keywords" content="{{$setting['seo_keyword']}}" />
    <meta name="description" content="{{$setting['seo_description']}}" />
    <link rel="SHORTCUT ICON" href="{!!$setting['icon_website']!!}">
    <title>{{$setting['seo_title']}}</title>

    <meta property="og:title" content="{{$setting['seo_title']}}">
    <meta property="og:description" content="{{$setting['seo_description']}}">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="@php echo URL::current(); @endphp">
    <meta property="og:site_name" content="{{$setting['company_name']}}">
@stop
@section('content')
       
    <div class="payment-page body-page">
        <div class="container">
            <div class="head">
                <h1>Thanh Toán</h1>
            </div>
            {{-- @if ($errors->any())
            <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
                <button type="button" class="close">×</button>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif --}}
            @if(Session::has('danger'))
                <div class="alert alert-danger">{{Session::get('danger')}}
                </div>
            @endif
            <div class="row">
                @if(Auth::guest())
                <div class="col-lg-12">
                    <div class="widget-payment">

                        <span><i class="fa fa-user"></i> Bạn đã có tài khoản?</span>
                        <a class="accordion" href="#">Ấn vào đây để đăng nhập</a>
                        <div class="widget-content panel">
                            <div class="wrap-login">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Nếu trước đây bạn đã mua hàng của chúng tôi, vui lòng ghi đầy đủ thông tin trong các hộp dưới đây. Nếu bạn là khách hàng mới, vui lòng chuyển tới phần Đơn hàng & Giao hàng.</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <form action="{{ url('/login') }}" id="login_validator"  class="login_validator" method="post">

                                        {{ csrf_field() }}
                                        <tr>
                                            <td scope="row">Email đăng nhập*</td>
                                            <td><input type="email" id="email" name="email" value="{{ old('email') }}" required autofocus></td>
                                        </tr>
                                            @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        <tr>
                                            <td scope="row">Mật khẩu *</td>
                                            <td><input type="password" id="password" name="password" placeholder="Password" required></td>
                                        </tr>
                                            @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        <tr>
                                            <td scope="row"><input class="submit" type="submit" value="Đăng nhập"></td>
                                            <td><input class="check" type="checkbox"><span>Ghi nhớ mật khẩu</span></td>
                                        </tr>
                                    </form>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                @endif
                <div class="col-lg-12">
                    <div class="widget-payment">

                        <span><i class="fa fa-inbox"></i> Có mã ưu đãi?</span>
                        <a class="accordion" href="#"> Ấn vào đây để nhập mã</a>
                        <div class="widget-content panel">
                            <div class="wrap-login">
                                <table class="table table-coupon">

                                    <tbody>
                                        <form action="useDiscountCode" method="post">
                                            {{csrf_field()}}
                                        <tr>
                                            <td><input type="text" name="discount_code" placeholder="Mã ưu đãi" value=" {{(Session::has('usediscount_code')) ? Session::get('usediscount_code') : '' }} "></td>
                                        </tr>
                                        <tr>
                                            <td scope="row"><input class="submit" type="submit" value="Áp dụng mã ưu đãi"></td>
                                        </tr>
                                        </form>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <form action="{!!url('order')!!}" method="post">
                    {{csrf_field()}}
                    <div class="col-lg-12">
                        <div class="col-lg-7">
                            <table class="table table-left">
                                <thead>
                                    <tr>
                                        <th colspan="2">Thông tin thanh toán</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td scope="row">Tên <span style="color: #da1313">*</span></td>
                                        <td><input type="text" name="receiver_name" value="{{Auth::guest() ? '' : Auth::user()->name}}" required></td>
                                    </tr>
                                    <tr>
                                        <td scope="row">Địa chỉ <span style="color: #da1313">*</span></td>
                                        <td><input type="text" name="receiver_address" placeholder="Số nhà và tên đường" required></td>
                                    </tr>
                                    <tr>
                                        <td scope="row">Số điện thoại <span style="color: #da1313">*</span></td>
                                        <td><input type="text" name="receiver_phone_number" required></td>
                                    </tr>
                                    <tr>
                                        <td scope="row">Địa chỉ email <span style="color: #da1313">*</span></td>
                                        <td><input type="receiver_email" name="receiver_email" value="{{Auth::guest() ? '' : Auth::user()->email}}" required></td>
                                    </tr>
                                    <tr>
                                        <td scope="row">Ghi chú đơn hàng</td>
                                        <td><textarea name="receiver_note" id="" cols="30" rows="10" placeholder="Ghi chú về đơn hàng, ví dụ: thời gian hay địa chỉ giao hàng chi tiết hơn"></textarea></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-lg-5">
                            <table class="table table-right">
                                <thead>
                                    <tr>
                                        <th colspan="5" class="text-center">Đơn hàng của bạn</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(Cart::countRows() > 0)
                                    @foreach(Cart::content() as $item)
                                    <tr>
                                        <td scope="row"><img src="{{$item->options->img}}" alt="{{$item->name}}"></td>
                                        <td scope="row">{{subtext(($item->name),100)}}</td>
                                        <td scope="row" style="white-space: nowrap;">{{$item->options->color}}</td>
                                        <td scope="row" style="text-align: center;">{{$item->qty}}</td>
                                        <td scope="row" style="white-space: nowrap; font-weight: bolder; text-align: right">{{number_format($item->subtotal)}} đ</td>

                                    </tr>
                                    @endforeach
                                    @endif
                                    @if(Session::has('discount_value')) 
                                    <tr>
                                        <td scope="row" colspan="3">Giảm giá:</td>
                                        <td scope="row" colspan="2">{{Session::get('discount_value')}}%</td>
                                        <input type="hidden" name="discount" value="{{Session::get('discount_value')}}" />
                                        <input type="hidden" name="usediscount_code" value="{{Session::get('usediscount_code')}}" />
                                        <input type="hidden" name="usediscount_id" value="{{Session::get('usediscount_id')}}" />
                                    </tr>
                                    <tr>
                                        <td scope="row" colspan="4">Tổng cộng:</td>
                                        <td scope="row" colspan="1" class="nowrap" style="font-size:20px;text-align: right">{{number_format((1-((Session::get('discount_value'))/100))*(floatval(str_replace(",","",Cart::total(0)))))}} đ</td>
                                        <input type="hidden" name="total_amount" value="{{(1-((Session::get('discount_value'))/100))*(floatval(str_replace(",","",Cart::total(0))))}}" />
                                    </tr>
                                    @else
                                    <tr>
                                        <td scope="row" colspan="4">Tổng cộng:</td>
                                        <td scope="row" colspan="1" class="nowrap" style="font-size:20px;text-align: right">{{Cart::total(0)}} đ</td>
                                        <input type="hidden" name="total_amount" value="{{floatval(str_replace(",","",Cart::total(0)))}}" />
                                    </tr>
                                    @endif
                                     
                                    <tr>
                                        <td colspan="5">
                                            <h4 class="title-pay">
                                                Phương thức thanh toán
                                            </h4>
                                            <ul>
                                                <li>
                                                    <div class="form-check form-check-inline">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="radio" name="pay_method" id="" value="cod" checked> <span>Trả tiền mặt khi giao hàng(COD)</span>
                                                        </label>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="form-check form-check-inline">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="radio" name="pay_method" id="" value="store"> <span>Thanh toán trực tiếp tại cửa hàng</span>
                                                        </label>
                                                    </div>
                                                </li>
                                                
                                                <li>
                                                    <div class="form-check form-check-inline">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="radio" name="pay_method" id="" value="banking"> <span>Thanh toán qua cổng thanh toán</span>
                                                        </label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td scope="row" colspan="5" onclick="datHang()" class="text-center"><button class="submit" type="submit">Thanh toán</button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>

                <div class=" more-product">
                    <div class="home-item">
                        <div class="item-wrap">
                            <div class="category-box">
                                <div class="category-title">
                                    <h2><a href="#">SẢN PHẨM MỚI</a></h2>
                                </div>
                                <div class="category-content">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 right-content">
                                            <div class="row">
                                                @if($product_new)
                                                @foreach($product_new as $item)
                                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 product-item">
                                                    <div class="product-box">
                                                        <div class="product-thumb">
                                                            <a href="/san-pham/{{$item->slug}}.html"><img src="{{$item->thumbnail}}" alt="{{$item->name}}"></a>
                                                        </div>
                                                        <div class="product-name"><a href="/san-pham/{{$item->slug}}.html">
                                                                {{subtext(($item->name),100)}}
                                                            </a></div>
                                                        <div class="product-price">
                                                            @if($item->qty_in_stock > 0)
                                                            @if($item->price != $item->price_sale)
                                                                <div class="price-old">{{number_format($item->price)}} đ</div>
                                                            @endif
                                                                <div class="price-new">{{number_format($item->price_sale)}} đ</div>
                                                            @else
                                                                <div class="price-new">Hết hàng</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('script')
@endsection
