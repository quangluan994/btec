@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Chỉnh sửa bài viết
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- plugin styles-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')


<section class="content-header">
        <h1>Chỉnh sửa bài viết</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('admin/bai-viet') }}">Bài viết</a></li>
          <li class="active">Chỉnh sửa</li>
        </ol>
    </section>
    <section class="content">
        @if ($errors->any())
        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
            <button type="button" class="close">×</button>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      
                        <div class="col-xs-6"><h4 style="font-weight: bolder">Chỉnh sửa bài viết:{{$post->name}}</h4></div>
                        <div class="col-xs-6" ><a href="{{url("admin/bai-viet")}}"  class="btn btn-info" title="" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i> Quay lại</a></div>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal login_validator" id="tryitForm" action="{!!url("admin/bai-viet")!!}/{!! $post->id !!}/update" method="post">
                            <div class="col-12">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Tiêu đề *</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="name" id="name" value="{{$post->name}}" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Slug *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="slug" id="slug" value="{{$post->slug}}" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">URL: </label>
                                    <div class="col-xl-7 col-lg-8">
                                        /tin-tuc/<span id="url_slug">{{$post->slug}}</span>.html
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Hình ảnh *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                                <span class="input-group-btn">
                                                <a href="/filemanager/dialog.php?type=0&field_id=thumb_0" class="btn btn-primary red iframe-btn" id="iframe-btn-0"><i class="fa fa-picture-o"></i>Chọn ảnh</a>
                                                </span>
                                                <input id="thumb_0" class="form-control" type="text" value="{{$post->image}}" name="image" required>
                                        </div>
                                        <div id="preview">
                                            <img id="holder" src="{{$post->image}}" style="margin-top:15px;max-height:100px;">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="status" class="col-lg-3 control-label">Tin nổi bật?</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <div class="radio">
                                                <label>
                                                  <input type="radio" name="status" id="optionsRadios1" value="Noi_bat" @php echo ($post->status == 'Noi_bat') ? 'checked' : '' @endphp> Có
                                                </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label>
                                                  <input type="radio" name="status" id="optionsRadios2" value="Khong_noi" @php echo ($post->status == 'Khong_noi') ? 'checked' : '' @endphp > Không
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Mô tả ngắn*</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <textarea name="description" id="editor" class="form-control my-editor" rows="20" required>{{$post->description}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Nội dung*</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <textarea name="content" id="editor1" class="form-control my-editor" rows="20" required>{{$post->content}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Chuyên mục tin *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-plus text-primary" required></i></span>
                                            <select name = 'category_id' class = 'form-control'>
                                                <option value="">--Chọn chuyên mục--</option>
                                                @if($post->category_id !=0 && $post->category_id != NULL)
                                                <option value="{!! $post->cate->id !!}" style="text-align: center" selected >{!! $post->cate->name !!}</option>
                                                <option value="">---Chọn chuyên mục---</option>
                                                <?php showCategoryChecked($cate,$post); ?>
                                                @else
                                                    <option value="">---Chọn chuyên mục---</option>
                                                    <?php showCategories($cate); ?>
                                                @endif

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-xl-7 col-lg-8 add_user_checkbox_error push-lg-3">
                                        <div>
                                            <label class="custom-control custom-checkbox">
                                                <input id="publish" type="checkbox" name="publish" class="custom-control-input" {!! $post->publish ? 'checked' : '' !!}>
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description"> Publish </span>
                                            </label>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                {{-- SEO --}}
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <h3>SEO</h3>
                                        <hr style="margin: 0">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_title" class="col-lg-3 control-label">Seo title (<span id="count_seo_title">{{mb_strlen($post->seo_title, 'UTF-8')}}</span>)</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <input type="text" name="seo_title" id="seo_title" class="form-control" value="{!!$post->seo_title!!}" onKeyDown="countNumseo_title(this.form.seo_title);" onKeyUp="countNumseo_title(this.form.seo_title);">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_keyword" class="col-lg-3 control-label">Seo keyword (<span id="count_seo_keyword">{{mb_strlen($post->seo_keyword, 'UTF-8')}}</span>)</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <input type="text" name="seo_keyword" id="seo_keyword" class="form-control" value="{!!$post->seo_keyword!!}" onKeyDown="countNumseo_keyword(this.form.seo_keyword);" onKeyUp="countNumseo_keyword(this.form.seo_keyword);">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_description" class="col-lg-3 control-label">Seo descripton (<span id="count_seo_description">{{mb_strlen($post->seo_description, 'UTF-8')}}</span>)</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <textarea name="seo_description" class="form-control my-editor" rows="7" id="seo_description" onKeyDown="countNumseo_description(this.form.seo_description);" onKeyUp="countNumseo_description(this.form.seo_description);">{{$post->seo_description}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-lg-9 push-lg-3">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Cập nhật</button>
                                        <button class="btn btn-warning" type="reset" id="clear"><i class="fa fa-refresh"></i> Nhập lại</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script src="{{asset('assets/fancybox/source/jquery.fancybox.js')}}"></script>
<script>
    $('#iframe-btn-0').fancybox({
        'width': 900,
        'height': 900,
        'type': 'iframe',
        'autoScale': false,
        'autoSize': false,
        afterClose: function() {
            var thumb = $('#thumb_0').val();
            var html = '<div class="img_preview"><img src="'+thumb+'"/>';
            html +='<input type="hidden" name="image" value="'+thumb+'" /> </div>';
            $('#preview').html(html);
        }
    });    
</script>
<script>
        $("#slug").keyup(function(){
            var slug = $("#slug").val();
            $('#url_slug').html(slug);
        });
    </script>
@endsection