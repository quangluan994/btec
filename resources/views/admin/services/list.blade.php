@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Danh sách dịch vụ
@parent
@stop
@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tablesid.css')}}"/>

@stop


@section('content')

    <section class="content-header">
        <h1><i class="fa fa-th"></i> Danh sách Dịch vụ</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('admin/services') }}">Dịch vụ</a></li>
          <li class="active">Danh sách</li>
        </ol>
    </section>

    <section class="content">
        @if(Session::has('success'))
            <div class="callout alert alert-success col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto" role="alert" >
                <button type="button" class="close">×</button> 
                {{Session::get('success')}}
            </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border  text-right">
                        <a href="{{url("admin/services/create")}}"  class="btn btn-primary" title=""><i class="fa fa-plus"></i> Thêm</a>

                        <div class=" form-group fix-float">
                            <select class="styled hasCustomSelect form-control" style="width:150px; float:left">
                                <option>Chọn tác vụ</option>
                                <option value="active">Kích hoạt/Vô hiệu hóa</option>
                                <option value="remove">Xóa</option>
                            </select>
                            <a href="javascript:action();" class="btn btn-info">Apply</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="card-block p-t-25">
                            <div class ="row col-lg-12"> 
                                <table class="respontables">
                                    <tr>
                                        <th style="width: 1%"> <input type="checkbox" class="check-all"></th>
                                        <th class="text-center">Tên</th>
                                        <th class="text-center" style="width:180px">Hình ảnh</th>
                                        <th class="text-center">URL</th>
                                        <th class="text-center">Mô tả</th>
                                        <th class="text-center">Trạng thái</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                    @forelse($services as $item)
                                        <tr>
                                            <td><input type="checkbox" class="checkbox" name="checkbox_id" value="{{$item->id}}" id="select-{{$item->id}}"></td>
                                            <td class="text-center">{!!$item->name!!}</td>
                                            <td class="text-center"><img src="{!!$item->image!!}" alt="{!!$item->name!!}" width="150" height="90" /></td>
                                            <td class="text-center">{!!$item->link!!}</td>
                                            <td class="text-center">{!!$item->description!!}</td>
                                            <td class="text-center">
                                                @if($item->publish == 1)
                                                <i class="fa fa-check text-success"></i>
                                                @else
                                                <i class="fa fa-times text-danger"></i>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="/admin/services/{!! $item->id !!}/edit" class="btn btn-primary" title=""><i class="fa fa-edit"></i></a>
                                                <a href="/admin/services/{!!$item->id!!}/delete" class="btn btn-danger" title="" onclick="return confirm('Cảnh báo: Bạn chắc chắn muốn xóa service này?')"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @empty
                                    <tr><td colspan="7"><h4>Không có dữ liệu để hiển thị</h4></td></tr>  
                                    @endforelse     
                                </table>    
                            </div>
                            <div style="clear:both"></div>
                            <div class="row col-lg-4 col-md-6 col-sm-6" style="float:right">
                               {{--  {{$cate->links()}} --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{csrf_field()}}
    </section>
@endsection