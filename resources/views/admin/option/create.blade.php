@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Admin | Option 
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- plugin styles-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Thêm key-value</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('admin/cai-dat') }}">Option</a></li>
          <li class="active">Thêm</li>
        </ol>
    </section>
    <section class="content">
        @if ($errors->any())
        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
            <button type="button" class="close">×</button>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h4>Thêm key-value</h4>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal login_validator" id="tryitForm" action="{!!url("admin/cai-dat")!!}" method="post">
                            <div class="col-12">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="position" class="col-lg-3 control-label">Position*</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <select name = 'position' class = 'form-control'>
                                            <option value="header_top">Header</option>
                                            <option value="footer_website">Footer</option>
                                            <option value="home_page">Trang chủ</option>
                                            <option value="info_company">Thông tin công ty</option>
                                            <option value="seo_website">SEO Website</option>
                                            <option value="add_code">Thêm code</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="key_option" class="col-lg-3 control-label">Key *</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="key_option" id="key_option" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="description" class="col-lg-3 control-label">Description</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <textarea name="description" class="form-control my-editor" rows="4"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="value" class="col-lg-3 control-label">Value *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <textarea name="value" class="form-control my-editor" rows="8"></textarea>
                                        {{-- <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="value" id="value" class="form-control" required>
                                        </div> --}}
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="type" class="col-lg-3 control-label">Type(string, media, textarea) *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <select name = 'type' class = 'form-control'>
                                            <option value="string">String</option>
                                            <option value="media">Media</option>
                                            <option value="textarea">Textarea</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-xl-7 col-lg-8 add_user_checkbox_error push-lg-3">
                                        <div>
                                            <label class="custom-control custom-checkbox">
                                                <input id="publish" type="checkbox" name="publish" class="custom-control-input" checked>
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description"> Publish </span>
                                            </label>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-lg-9 push-lg-3">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-plus"></i>Thêm</button>
                                        <button class="btn btn-warning" type="reset" id="clear"><i class="fa fa-refresh"></i>Nhập lại</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection