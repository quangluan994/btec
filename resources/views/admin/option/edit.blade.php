@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Edit Option
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- plugin styles-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}"/>
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Edit Option</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('admin/cai-dat') }}">Option</a></li>
          <li class="active">Edit option</li>
        </ol>
    </section>
    <section class="content">
        @if ($errors->any())
        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
            <button type="button" class="close">×</button>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      
                        <div class="col-xs-6"><h4>Edit option: {{$option->key_option}}</h4></div>
                        <div class="col-xs-6" ><a href="{{url("admin/cai-dat")}}"  class="btn btn-info" title="" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i> Quay lại</a></div>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal login_validator" id="tryitForm" action="{!!url("admin/cai-dat")!!}/{!! $option->id !!}/update" method="post">
                            <div class="col-12">
                                {{ csrf_field() }}
                                
                                <div class="form-group" style="display: none">
                                    <label for="key_option" class="col-lg-3 control-label">Key *</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" value="{{$option->key_option}}" id="key_option" name="key_option" class="form-control" readonly>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="description" class="col-lg-3 control-label">Mô tả</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <textarea name="description" class="form-control my-editor" rows="4" readonly>{{$option->description}}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="value" class="col-lg-3 control-label">Giá trị *</label>
                                    @if($option->type == 'string')
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" value="{{$option->value}}" id="value" name="value" class="form-control">
                                        </div>
                                    </div>
                                    @elseif($option->type == 'media')
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                                <span class="input-group-btn">
                                                <a href="/filemanager/dialog.php?type=0&field_id=thumb_0" class="btn btn-primary red iframe-btn" id="iframe-btn-0"><i class="fa fa-picture-o"></i>Chọn ảnh</a>
                                                </span>
                                                <input id="thumb_0" class="form-control" type="text" name="value" required value="{{$option->value}}">
                                        </div>
                                        <div id="preview">
                                            <img id="holder" src="{{$option->value}}" style="margin-top:15px;max-height:100px;">
                                        </div>
                                    </div>
                                    @elseif($option->type == 'textarea')
                                    <div class="col-xl-7 col-lg-8">
                                        @if($option->key_option == 'add_code_header' || $option->key_option == 'add_code_footer' || $option->key_option == 'seo_keyword' || $option->key_option == 'seo_description')
                                            <textarea name="value" class="form-control my-editor" rows="10">{{$option->value}}</textarea>
                                        @else
                                        <textarea name="value" id="editor" class="form-control my-editor" rows="4">{{$option->value}}</textarea>
                                        @endif
                                    </div>
                                    @endif
                                </div>
                                
                                <div class="form-group" style="display: none">
                                    <label for="type" class="col-lg-3 control-label">Type(string, media, textarea) *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" value="{{$option->type}}" id="type" name="type" class="form-control" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="display: none">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-xl-7 col-lg-8 add_user_checkbox_error push-lg-3">
                                        <div>
                                            <label class="custom-control custom-checkbox">
                                                <input id="publish" type="checkbox" name="publish" class="custom-control-input" {!! $option->publish ? 'checked' : '' !!}>
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description"> Publish </span>
                                            </label>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-lg-9 push-lg-3">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Cập nhật option</button>
                                        <button class="btn btn-warning" type="reset" id="clear"><i class="fa fa-refresh"></i>Nhập lại</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

<script src="{{asset('assets/fancybox/source/jquery.fancybox.js')}}"></script>
<!-- end of page level js -->
<script>
   
    $('#iframe-btn-0').fancybox({
        'width': 900,
        'height': 900,
        'type': 'iframe',
        'autoScale': false,
        'autoSize': false,
        afterClose: function() {
            var thumb = $('#thumb_0').val();
            if(thumb){
                var html = '<div class="img_preview"><img src="'+thumb+'"/>';
                html +='<input type="hidden" name="value" value="'+thumb+'" /> </div>';
                $('#preview').html(html);
            }
        }
    });

</script>

@endsection