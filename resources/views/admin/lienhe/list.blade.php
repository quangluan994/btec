@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Danh sách liên hệ
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tablesid.css')}}" />
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-th"></i> Danh sách liên hệ</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('admin/lien-he') }}">Liên hệ</a></li>
          <li class="active">Danh sách</li>
        </ol>
    </section>

    <section class="content">
        @if(Session::has('success'))
            <div class="callout alert alert-success col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto" role="alert" >
                <button type="button" class="close">×</button> 
                {{Session::get('success')}}
            </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border text-right">
                        <div class=" form-group fix-float">
                            <select class="styled hasCustomSelect form-control" style="width:150px; float:left">
                                <option>Chọn tác vụ</option>
                                <option value="remove">Xóa</option>
                            </select>
                            <a href="javascript:action();" class="btn btn-info">Apply</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="card-block p-t-25">
                            <div class ="row col-lg-12"> 
                                <table class="respontables">
                                    <tr>
                                        <th style="width: 1%"> <input type="checkbox" class="check-all"></th>
                                        <th class="text-center">Tên người dùng</th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center">Nội dung</th>
                                        <th class="text-center">Thời gian</th>
                                        <th class="text-center">Trạng thái</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                    @if($contacts->count()>0)
                                        @foreach($contacts as $val)
                                        <tr>
                                            <td>
                                            <input type="checkbox" class="checkbox" name="checkbox_id" value="{{$val->id}}" id="select-{{$val->id}}">
                                            </td>
                                            <td class="text-center">{!! $val->name !!}</td>
                                            <td class="text-center">{!! $val->email !!}</td>
                                            <td class="text-center">{!! $val->content !!}</td>
                                            <td class="text-center">{!! $val->created_at->format('d-m-Y') !!}</td>
                                            <td class="text-center">
                                                <form action="{!!url("admin/lien-he")!!}/{!! $val->id !!}/update" method="post">
                                                    {{csrf_field()}}
                                                    <select name="status" class="form-control">
                                                        <option value="chua_xu_ly" {{$val->status == 'chua_xu_ly' ? 'selected' : ''}}>Đang chờ xử lý</option>
                                                        <option value="da_xu_ly" {{$val->status == 'da_xu_ly' ? 'selected' : ''}}>Đã xử lý</option>
                                                    </select><br/>
                                                    <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Cập nhật</button>
                                                </form>
                                            </td>
                                            <td class="text-center">
                                                <a class="btn btn-danger delitem" id="del_{{$val->id}}"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                    <tr><td colspan="7"><h4>Không có dữ liệu để hiển thị</h4></td></tr>  
                                    @endif      
                                </table>    
                            </div>
                            <div style="clear:both"></div>
                            <div class="row col-lg-4 col-md-6 col-sm-6" style="float:right">
                                {{$contacts->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{csrf_field()}}
    </section>
    <script>
        $(".delitem").click(function(){
            var id = $(this).attr("id");
            var splitid = id.split("_");
            var deleteid = splitid[1];
            console.log(deleteid);
            swal({
              title: "Bạn chắc chắn muốn xóa?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Ok! Xóa ngay",
              closeOnConfirm: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    

                    var token = $("input[name='_token']").val();
                    $.ajax({
                        type: 'get',
                        url: '/admin/lien-he/'+deleteid+'/delete',
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token', token);
                        },
                        success: function(response) {
                            var res = $.parseJSON(response);
                            console.log(res);
                            if(!res.status) {
                                return;
                            }
                            location.reload(true);
                            swal({
                                title: "Thành công!",
                                timer: 2000,
                                type: "success"
                            });
                        }
                    });
                }
            });
        });
    </script>
@endsection