@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Chi tiết đơn hàng
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- plugin styles-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tablesid.css')}}"/>
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Chi tiết đơn hàng</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('admin/order') }}">Đơn hàng</a></li>
          <li class="active">Thêm</li>
        </ol>
    </section>
    <section class="content">
        @if ($errors->any())
        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
            <button type="button" class="close">×</button>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <a href="{{url('admin/order')}}" class="btn btn-primary">Quay lại</a><br/>
                    </div>
                    <div class="box-body">
                        <div class="col-12">
                            <h5>Mã đơn hàng: <b>PAY.ORDER.{{addchart($order->id)}}</b></h5>
                            <h5>Họ tên người nhận: <b>{{$order->receiver_name}}</b></h5>
                            <h5>Địa chỉ giao hàng: <b>{{$order->receiver_address}}</b></h5>
                            <h5>Số điện thoại người nhận: <b>{{$order->receiver_phone_number}}</b></h5>
                            <h5>Hình thức thanh toán: <b>
                                @if($order->pay_method == 'store')
                                    Thanh toán trực tiếp tại cửa hàng
                                @elseif($order->pay_method == 'cod')
                                    Trả tiền mặt khi giao hàng
                                @else
                                    Thanh toán qua cổng thanh toán vtcPay
                                @endif
                            </b></h5>
                            <table class="table table-striped" style="margin-top: 50px;">
                                <tr>
                                    <th>STT</th>
                                    <th style="width:180px;">Hình ảnh sản phẩm</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Số lượng</th>
                                    <th>Đơn giá</th>
                                    <th>Ghi chú</th>
                                    <th>Thành tiền</th>
                                </tr>
                                @if($orderdetail)
                                    @php
                                    $stt = 1;
                                    @endphp
                                    @foreach($orderdetail as $item)
                                        <tr>
                                            <td>{{$stt}}</td>
                                            <td><img src="{!! $item->product->thumbnail !!}" alt="{!! $item->product->name !!}" width="150"></td>
                                            <td>{{$item->product->name}}</td>
                                            <td>{{$item->quantity}}</td>
                                            <td>{{number_format($item->price_sale)}} đ</td>
                                            <td>{{$item->color}}</td>
                                            <td>{{number_format(($item->price_sale)*($item->quantity))}} đ</td>
                                        </tr>
                                    @php
                                    $stt++;
                                    @endphp
                                    @endforeach
                                    @if($order->discount !=0)
                                    <tr>
                                        <td colspan="5" class="text-center"><strong>Giảm giá(%):</strong></td>
                                        <td style="color:red"><strong>{{$order->discount}}%</strong></td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td colspan="5" class="text-center"><strong>Tổng cộng:</strong></td>
                                        <td style="color:red"><strong>{{number_format($item->order->total_amount)}} đ</strong></td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop