@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Thêm tag
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- plugin styles-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')

<header class="head">

    <div class="main-bar">
        <div class="row">
            <div class="col-lg-6">
                <h4 class="nav_top_align">
                    <i class="fa fa-plus"></i>
                    Thêm tag
                </h4>
            </div>
            <div class="col-lg-6">
                <div class="float-right">
                    <ol class="breadcrumb nav_breadcrumb_top_align">
                        <li class="breadcrumb-item">
                            <a href="index">
                                <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                Dashboard
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{!!url('admin/tag')!!}">tag</a>
                        </li>
                        <li class="breadcrumb-item active">Thêm tag</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="outer">
    <div class="inner bg-container">
        <div class="card">

            <div class="card-block m-t-35">
                <div>
                    <h4>Thêm mới tag</h4>
                </div>
                <form class="form-horizontal login_validator" id="tryitForm" action="{!!url("admin/tag")!!}" method="post">
                    <div class="row">
                        <div class="col-12">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            {{ csrf_field() }}

                            <div class="form-group row m-t-25">
                                <div class="col-lg-3 text-lg-right">
                                    <label for="u-name" class="col-form-label">
                                        Tên tag *
                                    </label>
                                </div>
                                <div class="col-xl-6 col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i>
                                        </span>
                                        <input type="text" name="name" id="u-name"
                                        class="form-control">
                                    </div>

                                </div>
                            </div>

                            <div class="form-group row m-t-25">
                                <div class="col-lg-3 text-lg-right">
                                    <label for="slug" class="col-form-label">
                                        Slug
                                    </label>
                                </div>
                                <div class="col-xl-6 col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i>
                                        </span>
                                        <input type="text" name="slug" id="slug"
                                        class="form-control">
                                    </div>

                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-3 text-lg-right">
                                    <label for="publish" class="col-form-label">Trạng thái *</label>
                                </div>
                                <div class="col-xl-6 col-lg-8">
                                    <div>
                                       <label class="custom-control custom-radio">
                                        <input type="radio" name="publish" class="custom-control-input" value="1" checked>
                                        <span class="custom-control-indicator custom_checkbox_success"></span>
                                        <span class="custom-control-description text-success">Yes</span>
                                    </label>
                                    <label class="custom-control custom-radio">
                                        <input type="radio" name="publish" class="custom-control-input" value="0">
                                        <span class="custom-control-indicator custom_checkbox_danger"></span>
                                        <span class="custom-control-description text-danger">No</span>
                                    </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-lg-3 text-lg-right">
                                    <label for="email" class="col-form-label">Mô tả
                                    *</label>
                                </div>
                                <div class="col-xl-6 col-lg-8">
                                    <div class="input-group">
                                        <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                                        <textarea name="desc" class="form-control my-editor" rows="12"></textarea>
                                        @include('admin.layouts.editor')
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                            <div class="col-xl-12">
                                <h3>SEO</h3>
                                <hr style="margin: 0">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3 text-lg-right">
                                <label for="seo_title" class="col-form-label">Seo title </label>
                            </div>
                            <div class="col-xl-6 col-lg-8">
                                <div class="input-group">
                                    <input type="text" name="seo_title" id="seo_title" class="form-control" placeholder="">
                                </div>

                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3 text-lg-right">
                                <label for="seo_keyword" class="col-form-label">Seo keyword </label>
                            </div>
                            <div class="col-xl-6 col-lg-8">
                                <div class="input-group">
                                    <input type="text" name="seo_keyword" id="seo_keyword" class="form-control" placeholder="">
                                </div>

                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-lg-3 text-lg-right">
                                <label for="seo_description" class="col-form-label">Seo description</label>
                            </div>
                            <div class="col-xl-6 col-lg-8">
                                <div class="input-group">
                                    <input type="text" name="seo_description" id="seo_description" class="form-control" placeholder="">
                                </div>
                            </div>
                        </div>
                            
                            <div class="form-group row">
                                <div class="col-lg-9 push-lg-3">
                                    <button class="btn btn-primary" type="submit">
                                        <i class="fa fa-plus"></i>
                                        Thêm
                                    </button>
                                    <button class="btn btn-warning" type="reset" id="clear">
                                        <i class="fa fa-refresh"></i>
                                        Nhập lại
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- /.inner -->
</div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- plugin scripts-->
<script type="text/javascript" src="{{asset('assets/js/pluginjs/jasny-bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/holderjs/js/holder.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
<!-- end of plugin scripts-->
<script type="text/javascript" src="{{asset('assets/js/pages/validation.js')}}"></script>
<!-- end of page level js -->
@stop