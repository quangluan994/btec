@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Danh sách tag
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!--plugin styles-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/select2/css/select2.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datatables/css/scroller.bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datatables/css/colReorder.bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/datatables/css/dataTables.bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/responsive.dataTables.css')}}" />
<!-- end of plugin styles -->
<!--Page level styles-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}" />
<!--End of page level styles-->
@stop


{{-- Page content --}}
@section('content')
<header class="head">
    <div class="main-bar">
        <div class="row">
            <div class="col-lg-6 col-md-4 col-sm-4">
                <h4 class="nav_top_align">
                    <i class="fa fa-th"></i>
                    Data Tables City
                </h4>
            </div>
            <div class="col-lg-6 col-md-8 col-sm-8">
                <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                    <li class="breadcrumb-item">
                        <a href="{{ url('admin/dashboard') }}">
                            <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ url('admin/tag') }}">tag</a>
                    </li>
                    <li class="breadcrumb-item active">Danh sách</li>
                </ol>
            </div>
        </div>
    </div>
</header>
<div class="outer">
    <div class="inner bg-light lter bg-container">
        <div class="row">
            <div class="col-12 data_tables">
                <!-- BEGIN EXAMPLE1 TABLE PORTLET-->
                <div class="card">
                    <div class="card-header bg-white">
                        <i class="fa fa-table"></i>
                    </div>
                    <div class="card-header bg-white">
                        <a href="{!!url("admin/tag/create")!!}"  class="btn btn-primary" title=""><i class="fa fa-plus"></i> Thêm</a>
                    </div>
                    <div class="card-block p-t-25">
                        <div class="">
                            <div class="pull-sm-right">
                                <div class="tools pull-sm-right"></div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>

                                    <th>STT</th>
                                    <th>Tên tag</th>
                                    <th>Trạng thái</th>
                                    <th>Mô tả ngắn</th>
                                    <th style="width: 85px">Action</th>
                                </tr>

                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($tags as $cities) 
                                <tr>
                                    <td>{!! $i !!}</td>
                                    <td>{!! $cities->name !!}</td>
                                    <td>
                                        @if($cities->publish == 1)
                                        <i class="fa fa-check text-success"></i>
                                        @else
                                        <i class="fa fa-times text-danger"></i>
                                        @endif
                                    </td>
                                    <td>{!! $cities->desc !!}</td>

                                    <td>
                                        <a href="/admin/tag/{!! $cities->id !!}/edit" class="btn btn-primary" title=""><i class="fa fa-edit"></i></a>
                                        <a href="/admin/tag/{!! $cities->id !!}/delete" class="btn btn-danger" title=""><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                @php
                                    $i++;
                                @endphp
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE1 TABLE PORTLET-->

            </div>
        </div>
    </div>
    <!-- /.inner -->
</div>
<!-- /.outer -->
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<!--plugin scripts-->
<script type="text/javascript" src="{{asset('assets/vendors/select2/js/select2.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/pluginjs/dataTables.tableTools.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.colReorder.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.buttons.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.responsive.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.rowReorder.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.colVis.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.html5.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/buttons.print.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.scroller.min.js')}}"></script>
<!-- end of plugin scripts -->
<!--Page level scripts-->
<script type="text/javascript" src="{{asset('assets/js/pages/datatable.js')}}"></script>

@if (alert()->ready())
    <script>
        swal({
            title: "{!! alert()->message() !!}",
            text: "{!! alert()->option('text') !!}",
            type: "{!! alert()->type() !!}",
            @if(alert()->option('timer'))
                timer: {!! alert()->option('timer') !!},
                showConfirmButton: false,
            @endif
        });
    </script>
@endif
@if (alert()->ready())
    <script>
        swal({
            title: "{!! alert()->message() !!}",
            type: "{!! alert()->type() !!}",
            text: "{!! alert()->option('text') !!}",
            showCancelButton: "{!! alert()->option('showCancelButton') !!}",
            cancelButtonColor: "{!! alert()->option('cancelButtonColor') !!}",
            confirmButtonColor: "{!! alert()->option('confirmButtonColor') !!}",
            confirmButtonText: "{!! alert()->option('confirmButtonText') !!}",
        }).then(function () {
            swal(
                '{!! alert()->option('deleted') !!}',
                '{!! alert()->option('msg') !!}',
                '{!! alert()->option('type') !!}'
            )
        });
    </script>
@endif
<!-- end of global scripts-->
@stop