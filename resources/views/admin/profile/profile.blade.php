@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Chỉnh sửa thông tin cá nhân
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- plugin styles-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}" />
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Chỉnh sửa thông tin cá nhân</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li class="active">Chỉnh sửa thông tin cá nhân</li>
        </ol>
    </section>
    <section class="content">
        @if ($errors->any())
        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
            <button type="button" class="close">×</button>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <h4 style="font-weight: bolder">Chỉnh sửa thông tin cá nhân</h4>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal login_validator" id="tryitForm" action="{!!url("admin/profile")!!}/{{ $user->id }}/update" method="post">
                            <div class="col-12">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Tên người dùng *</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="name" id="name" value="{!! $user->name !!}" class="form-control" required readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-lg-3 control-label">Email *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="email" id="email" value="{{$user->email}}" class="form-control" required readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Avata</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                                <span class="input-group-btn">
                                                <a href="/filemanager/dialog.php?type=0&field_id=thumb_0" class="btn btn-primary red iframe-btn" id="iframe-btn-0"><i class="fa fa-picture-o"></i>Chọn ảnh</a>
                                                </span>
                                                <input id="thumb_0" class="form-control" type="text" value="{{$user->avatar}}" name="avatar" required>
                                        </div>
                                        <div id="preview">
                                            <img id="holder" src="{{$user->avatar}}" style="margin-top:15px;max-height:100px;">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-lg-9 push-lg-3">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Cập nhật</button>
                                        <button class="btn btn-warning" type="reset" id="clear"><i class="fa fa-refresh"></i> Nhập lại</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12">
                            <input type="checkbox" id="changePassword" name="changePassword">Đổi mật khẩu
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-12">
                            <div class="changPass" style="display: none">
                                <form class="form-horizontal login_validator" id="tryitForm" action="{!!url("admin/profile")!!}/{{ $user->id }}/changPw" method="post">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                         <label>Mật khẩu cũ</label>
                                         <input type="password" class="form-control passworduser"  name="oldpassword" disabled="disabled" required>
                                    </div> 
                                    <div class="form-group">
                                         <label style="margin-bottom: 25px">Mật khẩu mới</label>
                                         <input type="password" class="form-control passworduser"  name="passwordnew" disabled="disabled" required>
                                    </div>
                                    <div class="form-group">
                                         <label style="margin-bottom: 25px">Nhập lại mật khẩu:</label>
                                         <input type="password" class="form-control passworduser" name="passwordAgain" disabled="disabled" required>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-offset-3 col-lg-8">
                                        <div class="col-lg-9 push-lg-3">
                                            <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Cập nhật</button>
                                            <button class="btn btn-warning" type="reset" id="clear"><i class="fa fa-refresh"></i> Nhập lại</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </section>

    <script>
        $(document).ready(function(){
            $("#changePassword").change(function(){
                if($(this).is(":checked"))
                {
                    $(".passworduser").removeAttr('disabled');
                    $('.changPass').show();
            
                }
                else
                {
                    $(".passworduser").attr('disabled','');
                    $('.changPass').hide();
                }
            });
        });
    </script>
    <script src="{{asset('assets/fancybox/source/jquery.fancybox.js')}}"></script>
    <script>
        $('#iframe-btn-0').fancybox({
            'width': 900,
            'height': 900,
            'type': 'iframe',
            'autoScale': false,
            'autoSize': false,
            afterClose: function() {
                var thumb = $('#thumb_0').val();
                if(thumb){
                    var html = '<div class="img_preview"><img src="'+thumb+'"/>';
                    html +='<input type="hidden" name="image" value="'+thumb+'" /> </div>';
                    $('#preview').html(html);
                }
            }
        });    
    </script>
@endsection