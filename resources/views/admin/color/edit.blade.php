@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Sửa màu sản phẩm
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- plugin styles-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')


<section class="content-header">
        <h1>Sửa màu</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('admin/mau-sac') }}">Màu sắc</a></li>
          <li class="active">Chỉnh sửa</li>
        </ol>
    </section>
    <section class="content">
        @if ($errors->any())
        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
            <button type="button" class="close">×</button>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if(Session::has('danger'))
            <div class="alert alert-danger">{{Session::get('danger')}}
            </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      
                        <div class="col-xs-6"><h4 style="font-weight: bolder">Sửa màu săc:{{$color->display_name}}</h4></div>
                        <div class="col-xs-6" ><a href="{{url("admin/mau-sac")}}"  class="btn btn-info" title="" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i> Quay lại</a></div>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal login_validator" id="tryitForm" action="{!!url("admin/mau-sac")!!}/{!! $color->id !!}/update" method="post">
                            <div class="col-12">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="display_name" class="col-lg-3 control-label">Tên màu *</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="display_name" id="display_name" class="form-control" value="{!! $color->display_name !!}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="color" class="col-lg-3 control-label">Mã màu*</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                            <div class="input-group-btn" style="width:50px">
                                            <input type="color" name="value_color" id="value_color" class="form-control" value="{!! $color->color !!}" style="padding:0">
                                            </div>
                                            <input type="text" name="color" id="color" class="form-control" value="{!! $color->color !!}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-lg-9 push-lg-3">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Cập nhật</button>
                                        <button class="btn btn-warning" type="reset" id="clear"><i class="fa fa-refresh"></i> Nhập lại</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script src="{{asset('assets/fancybox/source/jquery.fancybox.js')}}"></script>
<script>
    $("#value_color").change(function() {
        var color = $("#value_color").val();
        $("#color").val(color);
    });
</script>
@endsection