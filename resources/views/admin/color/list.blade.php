@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Bảng màu sản phẩm
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tablesid.css')}}"/>
<!--End of page level styles-->
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-th"></i> Bảng màu sản phẩm</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('admin/mau-sac') }}">Màu sắc</a></li>
          <li class="active">Bảng màu</li>
        </ol>
    </section>

    <section class="content">
        @if(Session::has('success'))
            <div class="callout alert alert-success col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto" role="alert" >
                <button type="button" class="close">×</button> 
                {{Session::get('success')}}
            </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border text-right">
                        <div class="col-lg-2" style="text-align: left">
                            <a href="{{url("admin/mau-sac/create")}}"  class="btn btn-primary" title=""><i class="fa fa-plus"></i> Thêm</a>
                        </div>

                        <div class=" form-group fix-float">
                            <select class="styled hasCustomSelect form-control" style="width:150px; float:left">
                                <option>Chọn tác vụ</option>
                                <option value="remove">Xóa</option>
                            </select>
                            <a href="javascript:action();" class="btn btn-info">Apply</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="card-block p-t-25">
                            <div class ="row col-lg-12"> 
                                <table class="respontables">
                                    <tr>
                                        <th style="width: 1%"> <input type="checkbox" class="check-all"></th>
                                        <th class="text-center">Màu sắc</th>
                                        <th class="text-center">Mã màu</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                    @if($colors->count()>0)
                                        @foreach($colors as $val)
                                        <tr>
                                            <td><input type="checkbox" class="checkbox" name="checkbox_id" value="{{$val->id}}" id="select-{{$val->id}}"></td>
                                            <td class="text-center">{!! $val->display_name !!}</td>
                                            <td class="text-center">{!! $val->color !!}</td>
                                            <td class="text-center">
                                                <a href="/admin/mau-sac/{!! $val->id !!}/edit" class="btn btn-primary" title=""><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-danger delitem" id="del_{{$val->id}}"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                    <tr><td colspan="6"><h4>Không có dữ liệu để hiển thị</h4></td></tr>  
                                    @endif      
                                </table>       
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{csrf_field()}}
    </section>
    
    <script>
        $(".delitem").click(function(){
            var id = $(this).attr("id");
            var splitid = id.split("_");
            var deleteid = splitid[1];
            console.log(deleteid);
            swal({
              title: "Bạn chắc chắn muốn xóa?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Ok! Xóa ngay",
              closeOnConfirm: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    

                    var token = $("input[name='_token']").val();
                    $.ajax({
                        type: 'get',
                        url: '/admin/mau-sac/'+deleteid+'/delete',
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token', token);
                        },
                        success: function(response) {
                            var res = $.parseJSON(response);
                            console.log(res);
                            if(!res.status) {
                                return;
                            }
                            location.reload(true);
                            swal({
                                title: "Thành công!",
                                timer: 2000,
                                type: "success"
                            });
                        }
                    });
                }
            });
        });
    </script>
@endsection