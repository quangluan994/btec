
<!DOCTYPE html>
<html>

<head>
    <title>Login | Admin </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    {{-- <link rel="shortcut icon" href="img/logo1.ico" /> --}}
    <!--Global styles -->
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/components.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" />
    <!--End of Global styles -->
    <!--Plugin styles-->
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}"/>
    <!--End of Plugin styles-->
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/pages/login.css') }}" />
</head>

<body>

    <div class="container wow fadeInDown" data-wow-delay="0.5s" data-wow-duration="2s">
        <div class="row">
            <div class="col-lg-8 push-lg-2 col-md-10 push-md-1 col-sm-10 push-sm-1 login_top_bottom">
                <div class="row">
                    <div class="col-lg-8 push-lg-2 col-md-10 push-md-1 col-sm-12">
                        <div class="login_logo login_border_radius1">
                            <h3 class="text-center">
                                {{-- <img src="{{ asset('assets/img/logow.png') }}" alt="josh logo" class="admire_logo"> --}}
                                <span class="text-white"><br/>
                                Admin EduTalk</span>
                            </h3>
                        </div>
                        <div class="bg-white login_content login_border_radius">
                            <form action="{{ route('login') }}" id="login_validator" method="post" class="login_validator">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="email" class="col-form-label"> E-mail</label>
                                    <div class="input-group">
                                        <span class="input-group-addon input_email"><i
                                            class="fa fa-envelope text-primary"></i></span>
                                            <input type="text" class="form-control  form-control-md" id="email" name="email" placeholder="E-mail" value="{{ old('email') }}">
                                            <br>
                                            
                                        </div>
                                        @if ($errors->has('email'))
                                        <span class="help-block text-danger">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <!--</h3>-->
                                    <div class="form-group">
                                        <label for="password" class="col-form-label">Mật khẩu</label>
                                        <div class="input-group">
                                            <span class="input-group-addon addon_password"><i
                                                class="fa fa-lock text-primary"></i></span>
                                                <input type="password" class="form-control form-control-md" id="password" name="password" placeholder="Password">
                                                <br>
                                                
                                            </div>
                                            @if ($errors->has('password'))
                                                <span class="help-block text-danger">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                                @endif
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <input type="submit" value="Đăng nhập" class="btn btn-danger btn-block login_button">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-6">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input form-control" value="{{ old('remember') ? 'checked' : '' }}">
                                                    <span class="custom-control-indicator"></span>
                                                    <a class="custom-control-description">Ghi nhớ</a>
                                                </label>
                                            </div>
                                            <div class="col-6 text-right forgot_pwd">
                                                <a href="{{ route('password.request') }}" class="custom-control-description forgottxt_clr">Quên mật khẩu?</a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- global js -->
            <script type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('assets/js/tether.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
            <!-- end of global js-->
            <!--Plugin js-->
            <script type="text/javascript" src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('assets/vendors/wow/js/wow.min.js') }}"></script>
            <!--End of plugin js-->
            <script type="text/javascript" src="{{ asset('assets/js/pages/login.js') }}"></script>
        </body>

        </html>
