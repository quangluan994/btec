<ul class="sidebar-menu" data-widget="tree">
	<li>
        <a href="{{ url('/') }} " target="blank">
            <i class="fa fa-globe"></i>
            <span class="link-title">&nbsp;Website</span>
        </a>
    </li>
	<li class="{{ Request::is('admin/dashboard') ? 'active' : ''}}">
		<a href="{{ url('admin/dashboard') }}">
			<i class="fa fa-dashboard"></i> <span>Dashboard</span>
		</a>
	</li>
	<li>
		<a href="{!! url('admin/media') !!} ">
            <i class="fa fa-picture-o"></i>
            <span class="link-title">&nbsp;Thư viện ảnh</span>
        </a>
	</li>

	<li class="treeview {{ (Request::is('admin/chuong-trinh-giang-day/create') || Request::is('admin/chuong-trinh-giang-day/edit') || Request::is('admin/chuong-trinh-giang-day') ? 'active' : '') }}">
		<a href="#">
			<i class="fa fa-product-hunt"></i>
			<span>Chương trình giảng dạy</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="{!!url("admin/chuong-trinh-giang-day/create")!!}"><i class="fa fa-plus"></i>Thêm</a></li>
			<li><a href="{!!url("admin/chuong-trinh-giang-day")!!}"><i class="fa fa-list"></i>Danh sách</a></li>
		</ul>
	</li>

	<li class="treeview {{ (Request::is('admin/co-hoi-nghe-nghiep/create') || Request::is('admin/co-hoi-nghe-nghiep/edit') || Request::is('admin/co-hoi-nghe-nghiep') ? 'active' : '') }}">
		<a href="#">
			<i class="fa fa-pencil" aria-hidden="true"></i>
			<span>Cơ hội nghề nghiệp</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="{!!url("admin/co-hoi-nghe-nghiep/create")!!}"><i class="fa fa-plus"></i>Thêm</a></li>
			<li><a href="{!!url("admin/co-hoi-nghe-nghiep")!!}"><i class="fa fa-list"></i>Danh sách</a></li>
		</ul>
	</li>

	<li class="treeview {{ (Request::is('admin/ly-do/create') || Request::is('admin/ly-do/edit') || Request::is('admin/ly-do') ? 'active' : '') }}">
		<a href="#">
			<i class="fa fa-credit-card-alt"></i>
			<span>Lý do</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="{!!url("admin/ly-do/create")!!}"><i class="fa fa-plus"></i>Thêm</a></li>
			<li><a href="{!!url("admin/ly-do")!!}"><i class="fa fa-list"></i>Danh sách</a></li>
		</ul>
	</li>

	<li class="treeview {{ (Request::is('admin/banner/create') || Request::is('admin/banner/edit') || Request::is('admin/banner') ? 'active' : '') }}">
		<a href="#">
			<i class="fa fa-picture-o"></i>
			<span>Banner</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="{!!url("admin/banner/create")!!}"><i class="fa fa-plus"></i>Thêm</a></li>
			<li><a href="{!!url("admin/banner")!!}"><i class="fa fa-list"></i>Danh sách</a></li>
		</ul>
	</li>
	
	<li class="treeview {{ (Request::is('admin/thong-bao-tuyen-sinh/create') || Request::is('admin/thong-bao-tuyen-sinh/edit') || Request::is('admin/thong-bao-tuyen-sinh') ? 'active' : '') }}">
		<a href="#">
			<i class="fa fa-bell" aria-hidden="true"></i>
			<span>Thông báo tuyển sinh</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="{!!url("admin/thong-bao-tuyen-sinh/create")!!}"><i class="fa fa-plus"></i>Thêm</a></li>
			<li><a href="{!!url("admin/thong-bao-tuyen-sinh")!!}"><i class="fa fa-list"></i>Danh sách</a></li>
		</ul>
	</li>
	<li class="treeview {{ (Request::is('admin/gioi-thieu/create') || Request::is('admin/gioi-thieu/edit') || Request::is('admin/gioi-thieu') ? 'active' : '') }}">
		<a href="#">
			<i class="fa fa-info" aria-hidden="true"></i>
			<span>Giới thiệu</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="{!!url("admin/gioi-thieu/create")!!}"><i class="fa fa-plus"></i>Thêm</a></li>
			<li><a href="{!!url("admin/gioi-thieu")!!}"><i class="fa fa-list"></i>Danh sách</a></li>
		</ul>
	</li>

	<li class="treeview {{ (Request::is('admin/doi-tac/create') || Request::is('admin/doi-tac/edit') || Request::is('admin/doi-tac') ? 'active' : '') }}">
		<a href="#">
			<i class="fa fa-users" aria-hidden="true"></i>
			<span>Đối tác</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="{!!url("admin/doi-tac/create")!!}"><i class="fa fa-plus"></i>Thêm</a></li>
			<li><a href="{!!url("admin/doi-tac")!!}"><i class="fa fa-list"></i>Danh sách</a></li>
		</ul>
	</li>

	<li class="treeview {{ (Request::is('admin/hinh-anh/create') || Request::is('admin/hinh-anh/edit') || Request::is('admin/hinh-anh') ? 'active' : '') }}">
		<a href="#">
			<i class="fa fa-picture-o" aria-hidden="true"></i>
			<span>Hình ảnh</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="{!!url("admin/hinh-anh/create")!!}"><i class="fa fa-plus"></i>Thêm</a></li>
			<li><a href="{!!url("admin/hinh-anh")!!}"><i class="fa fa-list"></i>Danh sách</a></li>
		</ul>
	</li>

	<li class="treeview {{ (Request::is('admin/nhan-xet/create') || Request::is('admin/nhan-xet/edit') || Request::is('admin/nhan-xet') ? 'active' : '') }}">
		<a href="#">
			<i class="fa fa-commenting-o" aria-hidden="true"></i>
			<span>Nhận xét</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="{!!url("admin/nhan-xet/create")!!}"><i class="fa fa-plus"></i>Thêm</a></li>
			<li><a href="{!!url("admin/nhan-xet")!!}"><i class="fa fa-list"></i>Danh sách</a></li>
		</ul>
	</li>
	
	<li class="treeview {{ (Request::is('admin/user/create') || Request::is('admin/user/edit') || Request::is('admin/user') ? 'active' : '') }}">
		<a href="#">
			<i class="fa fa-user"></i>
			<span>Người dùng</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="{!!url("admin/user/create")!!}"><i class="fa fa-plus"></i>Thêm</a></li>
			<li><a href="{!!url("admin/user")!!}"><i class="fa fa-list"></i>Danh sách</a></li>
		</ul>
	</li>

	<li class=" {{ (Request::is('admin/lien-he/create') || Request::is('admin/lien-he/edit') || Request::is('admin/lien-he') ? 'active' : '') }}">
		<a href="{!!url("admin/lien-he")!!}">
			<i class="fa fa-comment"></i>
			<span>Liên hệ</span>
		</a>
	</li>
	<li class="treeview {{ (Request::is('admin/menu') || Request::is('admin/cai-dat') ? 'active' : '') }}">
		<a href="#">
			<i class="fa fa-cogs"></i>
			<span>Cài đặt</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="{!!url("admin/menu")!!}"><i class="fa fa-bars"></i>Menu</a></li>
			<li><a href="{!!url("admin/tuy-chon-hien-thi")!!}"><i class="fa fa-television"></i>Hiển thị</a></li>
			<li><a href="{!!url("admin/cai-dat")!!}"><i class="fa fa-angle-right"></i>Tùy chỉnh</a></li>
		</ul>
	</li>

	
</ul>