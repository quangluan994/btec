@extends('admin.layouts.default')
{{-- Page title --}}
@section('title')
Thêm danh mục
@parent
@stop
@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}"/>
@stop
{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Thêm danh mục</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('admin/danh-muc') }}">Danh mục</a></li>
          <li class="active">Thêm</li>
        </ol>
    </section>
    <section class="content">
        @if ($errors->any())
        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
            <button type="button" class="close">×</button>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="col-xs-6"><h4>Thêm danh mục</h4></div>
                      
                        <div class="col-xs-6" ><a href="{{url("admin/danh-muc")}}"  class="btn btn-info" title="" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i> Quay lại</a></div>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal login_validator" id="tryitForm" action="{!!url("admin/danh-muc")!!}" method="post">
                            <div class="col-12">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Tên danh mục *</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="name" id="name" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Slug *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="slug" id="slug" class="form-control" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">URL: </label>
                                    <div class="col-xl-7 col-lg-8">
                                        /danh-muc/<span id="url_slug"></span>.html
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="thumbnail" class="col-lg-3 control-label">Hình ảnh</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <a href="/filemanager/dialog.php?type=0&field_id=thumb_0" class="btn btn-primary red iframe-btn" id="iframe-btn-0"><i class="fa fa-picture-o"></i>Chọn ảnh</a>
                                            </span>
                                            <input id="thumb_0" class="form-control" type="text" name="image">
                                        </div>
                                        <div id="preview">

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Chọn danh mục cha *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-plus text-primary"></i></span>
                                            <select name = 'parent_id' class = 'form-control'>
                                                <option value="0">-- Chọn danh mục cha --</option>
                                                <?php showCategories($cate); ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-xl-7 col-lg-8 add_user_checkbox_error push-lg-3">
                                        <div>
                                            <label class="custom-control custom-checkbox">
                                                <input id="publish" type="checkbox" name="publish" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description"> Publish </span>
                                            </label>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <h3>SEO</h3>
                                        <hr style="margin: 0">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_title" class="col-lg-3 control-label">Seo title(<span id="count_seo_title">0</span>)</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <input type="text" name="seo_title" id="seo_title" class="form-control" onKeyDown="countNumseo_title(this.form.seo_title);" onKeyUp="countNumseo_title(this.form.seo_title);">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_keyword" class="col-lg-3 control-label">Seo keyword(<span id="count_seo_keyword">0</span>) </label>
                                    <div class="col-xl-7 col-lg-8">
                                        <input type="text" name="seo_keyword" id="seo_keyword" class="form-control" onKeyDown="countNumseo_keyword(this.form.seo_keyword);" onKeyUp="countNumseo_keyword(this.form.seo_keyword);">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_description" class="col-lg-3 control-label">Seo descripton(<span id="count_seo_description">0</span>)</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <textarea name="seo_description" class="form-control my-editor" rows="7" id="seo_description" onKeyDown="countNumseo_description(this.form.seo_description);" onKeyUp="countNumseo_description(this.form.seo_description);"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-lg-9 push-lg-3">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-plus"></i>Thêm</button>
                                        <button class="btn btn-warning" type="reset" id="clear"><i class="fa fa-refresh"></i>Nhập lại</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

<script src="{{asset('assets/fancybox/source/jquery.fancybox.js')}}"></script>
<script>
    $("#name").keyup(function(){
        var name = $("#name").val();
        var token = $('input[name=_token]').val();
        if(name) {
            $.ajax({
                type: 'post',
                url: '/slugCategory',
                headers: { 'X-XSRF-TOKEN' : token },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', token);
                },
                data: {type: 'ajax', _csrfToken : token},
                data: {name: name},
                success: function(data) {
                    data = $.parseJSON(data);
                    $('#slug').val(data);
                    $('#url_slug').html(data);
                },
                error: function(error) {
                console.log('error');
                }
            });
        }
    });

    $('#iframe-btn-0').fancybox({
        'width': 900,
        'height': 900,
        'type': 'iframe',
        'autoScale': false,
        'autoSize': false,
        afterClose: function() {
            var thumb = $('#thumb_0').val();
            if(thumb){
                var html = '<div class="img_preview"><img src="'+thumb+'"/>';
                html +='<input type="hidden" name="image" value="'+thumb+'" /> </div>';
                $('#preview').html(html);
            }
        }
    });
</script>
<script>
        $("#slug").keyup(function(){
            var slug = $("#slug").val();
            $('#url_slug').html(slug);
        });
    </script>
@stop