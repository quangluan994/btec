@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Danh sách lý do
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tablesid.css')}}"/>

<!--End of page level styles-->
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-th"></i> Danh sách lý do</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('admin/ly-do') }}">Lý do</a></li>
          <li class="active">Danh sách</li>
        </ol>
    </section>

    <section class="content">
        @if(Session::has('success'))
            <div class="callout alert alert-success col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto" role="alert" >
                <button type="button" class="close">×</button> 
                {{Session::get('success')}}
            </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border text-right">
                        <a href="{{url("admin/ly-do/create")}}"  class="btn btn-primary" title=""><i class="fa fa-plus"></i> Thêm</a>

                        <div class=" form-group fix-float">
                            <select class="styled hasCustomSelect form-control" style="width:150px; float:left">
                                <option>Chọn tác vụ</option>
                                <option value="active">Kích hoạt/Vô hiệu hóa</option>
                                <option value="remove">Xóa</option>
                            </select>
                            <a href="javascript:action();" class="btn btn-info">Apply</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="card-block p-t-25">
                            <div class ="row col-lg-12"> 
                                <table class="respontables">
                                    <tr>
                                        <th style="width: 1%"> <input type="checkbox" class="check-all"></th>
                                        <th class="text-center">Tên</th>
                                        <th class="text-center">Trạng thái</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                    @forelse($lydo as $item)
                                        <tr>
                                            <td><input type="checkbox" class="checkbox" name="checkbox_id" value="{{$item->id}}" id="select-{{$item->id}}"></td>
                                            <td class="text-center">{!!$item->name!!}</td>
                                            <td class="text-center">
                                                @if($item->publish == 1)
                                                <i class="fa fa-check text-success"></i>
                                                @else
                                                <i class="fa fa-times text-danger"></i>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="/admin/ly-do/{!! $item->id !!}/edit" class="btn btn-primary" title=""><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-danger delitem" id="del_{{$item->id}}"><i class="fa fa-trash"></i></a>

                                            </td>
                                        </tr>
                                    @empty
                                    <tr><td colspan="7"><h4>Không có dữ liệu để hiển thị</h4></td></tr>  
                                    @endforelse     
                                </table>    
                            </div>
                            <div style="clear:both"></div>
                            <div class="row col-lg-4 col-md-6 col-sm-6" style="float:right">
                               {{--  {{$cate->links()}} --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{csrf_field()}}
    </section>
    <script>
        $(".delitem").click(function(){
            var id = $(this).attr("id");
            var splitid = id.split("_");
            var deleteid = splitid[1];
            console.log(deleteid);
            swal({
              title: "Bạn chắc chắn muốn xóa?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Ok! Xóa ngay",
              closeOnConfirm: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    

                    var token = $("input[name='_token']").val();
                    $.ajax({
                        type: 'get',
                        url: '/admin/ly-do/'+deleteid+'/delete',
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token', token);
                        },
                        success: function(response) {
                            var res = $.parseJSON(response);
                            console.log(res);
                            if(!res.status) {
                                return;
                            }
                            location.reload(true);
                            swal({
                                title: "Thành công!",
                                timer: 2000,
                                type: "success"
                            });
                        }
                    });
                }
            });
        });
    </script>
@endsection

