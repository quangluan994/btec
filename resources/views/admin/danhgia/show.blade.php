@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Show
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- plugin styles-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')

<header class="head">

    <div class="main-bar">
        <div class="row">
            <div class="col-lg-6">
                <h4 class="nav_top_align">
                    <i class="fa fa-pencil"></i>
                    Show đánh giá
                </h4>
            </div>
            <div class="col-lg-6">
                <div class="float-right">
                    <ol class="breadcrumb nav_breadcrumb_top_align">
                        <li class="breadcrumb-item">
                            <a href="{{ url('admin/dashboard') }}">
                                <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                Dashboard
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{!! url('admin/danh-gia') !!}">Đánh giá</a>
                        </li>
                        <li class="breadcrumb-item active">Show</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="outer">
    <div class="inner bg-container">
        <div class="card">

            <div class="card-block m-t-35">

                <div class="row">
                    <div class="col-12">
                        <div class="form-group row m-t-25">
                            <div class="col-lg-3 text-lg-right">
                                <label for="u-name" class="col-form-label">Name: </label>
                            </div>
                            <div class="col-xl-6 col-lg-8">
                                <div class="input-group">                                    
                                    {!! $danhgias->name !!}
                                </div>

                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 text-lg-right">
                                <label for="email" class="col-form-label">Email: </label>
                            </div>
                            <div class="col-xl-6 col-lg-8">
                                {!! $danhgias->email !!}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3 text-lg-right">
                                <label for="email" class="col-form-label">Content: </label>
                            </div>
                            <div class="col-xl-6 col-lg-8">
                                {!! $danhgias->content !!}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3 text-lg-right">
                                <label for="email" class="col-form-label">Star giáo viên: </label>
                            </div>
                            <div class="col-xl-6 col-lg-8">
                                {!! $danhgias->star_gv !!} <i class="fa fa-star-o text-danger" aria-hidden="true"></i>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3 text-lg-right">
                                <label for="email" class="col-form-label">Star phương pháp dạy: </label>
                            </div>
                            <div class="col-xl-6 col-lg-8">
                                {!! $danhgias->star_gv !!} <i class="fa fa-star-o text-danger" aria-hidden="true"></i>


                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3 text-lg-right">
                                <label for="email" class="col-form-label">Star học phí: </label>
                            </div>
                            <div class="col-xl-6 col-lg-8">
                                {!! $danhgias->star_gv !!} <i class="fa fa-star-o text-danger" aria-hidden="true"></i>


                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3 text-lg-right">
                                <label for="email" class="col-form-label">Star cơ sở vật chất: </label>
                            </div>
                            <div class="col-xl-6 col-lg-8">
                                {!! $danhgias->star_gv !!} <i class="fa fa-star-o text-danger" aria-hidden="true"></i>


                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3 text-lg-right">
                                <label for="email" class="col-form-label">Star môi trường: </label>
                            </div>
                            <div class="col-xl-6 col-lg-8">
                                {!! $danhgias->star_gv !!} <i class="fa fa-star-o text-danger" aria-hidden="true"></i>

                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3 text-lg-right">
                                <label for="email" class="col-form-label">Star Total: </label>
                            </div>
                            <div class="col-xl-6 col-lg-8">
                                {!! $danhgias->star_gv !!} <i class="fa fa-star-o text-danger" aria-hidden="true"></i>


                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3 text-lg-right">
                                <label for="email" class="col-form-label">Khóa học: </label>
                            </div>
                            <div class="col-xl-6 col-lg-8">
                                {!! $danhgias->khoahoc->name !!}

                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-3 text-lg-right">
                                <label for="email" class="col-form-label">Thới gian: </label>
                            </div>
                            <div class="col-xl-6 col-lg-8">
                                {!! $danhgias->created_at->format('d M Y') !!}

                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- /.inner -->
</div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- plugin scripts-->
<script type="text/javascript" src="{{asset('assets/js/pluginjs/jasny-bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/holderjs/js/holder.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}"></script>
<!-- end of plugin scripts-->
<script type="text/javascript" src="{{asset('assets/js/pages/validation.js')}}"></script>
<!-- end of page level js -->
@stop