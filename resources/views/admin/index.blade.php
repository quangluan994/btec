@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Bảng điều khiển Admin
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!--Plugin styles-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/c3/css/c3.min.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/toastr/css/toastr.min.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/switchery/css/switchery.min.css')}}" />
<!--page level styles-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/new_dashboard.css')}}"/>
<!-- end of page level styles -->
@stop


{{-- Page content --}}
@section('content')

    <section class="content-header">
        <h1><i class="fa fa-th"></i> Bảng điều khiển</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        </ol>
    </section>

    <section class="content">
        @if(Session::has('success'))
            <div class="callout alert alert-success col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto" role="alert" >
                <button type="button" class="close">×</button> 
                {{Session::get('success')}}
            </div>
        @endif
        @if(Session::has('danger'))
            <div class="alert alert-danger">{{Session::get('danger')}}
            </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        
                    </div>
                    <div class="box-body">
                        <div class="card-block p-t-25">
                            <div class ="col-lg-12"> 
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box">
                                        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

                                        <div class="info-box-content">
                                          <span class="info-box-text">Option</span>
                                          <span class="info-box-number">{!! count($option) !!}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                      </div>
                                      <!-- /.info-box -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box">
                                        <span class="info-box-icon bg-red"><i class="fa fa-book"></i></span>

                                        <div class="info-box-content">
                                          <span class="info-box-text">Danh mục</span>
                                          <span class="info-box-number">{!! count($cate) !!}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                      </div>
                                      <!-- /.info-box -->
                                    </div>
                                    <!-- /.col -->

                                    <!-- fix for small devices only -->
                                    <div class="clearfix visible-sm-block"></div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box">
                                        <span class="info-box-icon bg-green"><i class="fa fa-product-hunt"></i></span>

                                        <div class="info-box-content">
                                          <span class="info-box-text">Sản phẩm</span>
                                          <span class="info-box-number">{!! count($product) !!}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                      </div>
                                      <!-- /.info-box -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box">
                                        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                                        <div class="info-box-content">
                                          <span class="info-box-text">Người dùng</span>
                                          <span class="info-box-number">{!! count($user) !!}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                      </div>
                                      <!-- /.info-box -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box">
                                        <span class="info-box-icon bg-aqua"><i class="fa fa-bars"></i></span>

                                        <div class="info-box-content">
                                          <span class="info-box-text">Chuyên mục</span>
                                          <span class="info-box-number">{!! count($catepost) !!}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                      </div>
                                      <!-- /.info-box -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box">
                                        <span class="info-box-icon bg-red"><i class="fa fa-newspaper-o" aria-hidden="true"></i></span>

                                        <div class="info-box-content">
                                          <span class="info-box-text">Tin tức</span>
                                          <span class="info-box-number">{!! count($post) !!}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                      </div>
                                      <!-- /.info-box -->
                                    </div>
                                    <!-- /.col -->

                                    <!-- fix for small devices only -->
                                    <div class="clearfix visible-sm-block"></div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box">
                                        <span class="info-box-icon bg-green"><i class="fa fa-truck" aria-hidden="true"></i></span>

                                        <div class="info-box-content">
                                          <span class="info-box-text">Đơn hàng</span>
                                          <span class="info-box-number">{!! count($order) !!}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                      </div>
                                      <!-- /.info-box -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box">
                                        <span class="info-box-icon bg-yellow"><i class="fa fa-comments" aria-hidden="true"></i></span>

                                        <div class="info-box-content">
                                          <span class="info-box-text">Phản hồi</span>
                                          <span class="info-box-number">{!! count($contact) !!}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                      </div>
                                      <!-- /.info-box -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('footer_scripts')
<!--  plugin scripts -->

<!-- end page level scripts -->
@endsection