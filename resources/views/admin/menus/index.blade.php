@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
    Danh sách menu
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
    <!--Plugin styles-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/select2/css/select2.min.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/dataTables.bootstrap.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tables.css')}}"/>
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tablesid.css')}}"/>

    <!-- end of page level styles -->
@stop
{{-- Page content --}}
@section('content')

    


    <section class="content-header">
        <h1><i class="fa fa-th"></i> Menu</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('admin/menu') }}">Menu</a></li>
          <li class="active">Danh sách</li>
        </ol>
    </section>

    <section class="content">
        {!! Menu::render() !!}
    </section>

    
@stop

@section('footer_scripts')
    {!! Menu::scripts() !!}
@endsection

