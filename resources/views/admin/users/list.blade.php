@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Danh sách người dùng
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tablesid.css')}}"/>
<!--End of page level styles-->
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-th"></i> Danh sách người dùng</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('admin/user') }}">Người dùng</a></li>
          <li class="active">Danh sách</li>
        </ol>
    </section>

    <section class="content">
        @if(Session::has('success'))
            <div class="callout alert alert-success col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto" role="alert" >
                <button type="button" class="close">×</button> 
                {{Session::get('success')}}
            </div>
        @endif

        @if(Session::has('danger'))
            <div class="alert alert-danger">{{Session::get('danger')}}
            </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border  text-right">
                        <div class=" form-group fix-float">
                            <select class="styled hasCustomSelect form-control" style="width:150px; float:left">
                                <option>Chọn tác vụ</option>
                                <option value="remove">Xóa</option>
                            </select>
                            <a href="javascript:action();" class="btn btn-info">Apply</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="card-block p-t-25">
                            <div class ="row col-lg-12"> 
                                <table class="respontables">
                                    <tr>
                                        <th style="width: 1%"> <input type="checkbox" class="check-all"></th>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Tên hiển thị </th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center">Ảnh đại diện</th>
                                        <th class="text-center">Quyền người dùng</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                    @if($users->count()>0)
                                        @foreach($users as $val)
                                        <tr>
                                            <td><input type="checkbox" class="checkbox" name="checkbox_id" value="{{$val->id}}" id="select-{{$val->id}}"></td>
                                            <td class="text-center">{!! $val->id !!}</td>
                                            <td class="text-center">{!! $val->name !!}</td>
                                            <td class="text-center">{!! $val->email !!}</td>
                                            <td class="text-center"><img src="{!! $val->avatar !!}" alt="" width="70" height="70"></td>
                                            <td class="text-center">{!! $val->role->display_name !!}</td>
                                            <td class="text-center">
                                                <a href="/admin/user/{!! $val->id !!}/edit" class="btn btn-primary" title=""><i class="fa fa-edit"></i></a>
                                                <a href="/admin/user/{!! $val->id !!}/delete" class="btn btn-danger" ><i class="fa fa-trash"></i></a>
                                                
                                                {{-- <script>
                                                    function salert(){
                                                        swal({
                                                          title: "Bạn chắc chắn muốn xóa?",
                                                          type: "warning",
                                                          showCancelButton: true,
                                                          confirmButtonColor: "#DD6B55",
                                                          confirmButtonText: "Ok! Xóa ngay",
                                                          closeOnConfirm: false
                                                        },
                                                        function (isConfirm) {
                                                            if (isConfirm) {
                                                                var token = $("input[name='_token']").val();
                                                                $.ajax({
                                                                    type: 'get',
                                                                    url:'/admin/user/{!! $val->id !!}/delete',
                                                                    beforeSend: function(xhr){
                                                                        xhr.setRequestHeader('X-CSRF-Token', token);
                                                                    },
                                                                    success: function(response) {
                                                                        var res = $.parseJSON(response);
                                                                        if(!res.status) {
                                                                            swal({
                                                                                title: "Không thể xóa admin!",
                                                                                timer: 2000,
                                                                                type: "error"
                                                                            });
                                                                        }else {
                                                                            swal({
                                                                                title: "Thành công!",
                                                                                timer: 2000,
                                                                                type: "success"
                                                                            });
                                                                        }
                                                                        location.reload(true);
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                </script> --}}
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                    <tr><td colspan="7"><h4>Không có dữ liệu để hiển thị</h4></td></tr>  
                                    @endif      
                                </table>   
                            </div>
                            <div style="clear:both"></div>
                            <div class="row col-lg-4 col-md-6 col-sm-6" style="float:right">
                                {{$users->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{csrf_field()}}
    </section>

@stop
