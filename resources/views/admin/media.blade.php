@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
    Thư viện ảnh
    @parent
@stop
@section('header_styles')

@stop


{{-- Page content --}}
@section('content')

<iframe src="/filemanager/dialog.php?type=0&field_id=thumb_0" style="width: 100%; height: 800px; overflow: hidden; border: none;"></iframe>

@stop

{{-- page level scripts --}}
@section('footer_scripts')

@stop