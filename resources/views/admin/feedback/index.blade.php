@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
    Danh sách phản hồi của khách hàng
    @parent
@stop
{{-- page level styles --}}
@section('header_styles')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tablesid.css')}}"/>
    <!-- end of page level styles -->
@stop


{{-- Page content --}}
@section('content')
    
    <section class="content-header">
        <h1><i class="fa fa-th"></i> Danh sách Feedback</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('admin/feedback') }}">Feedback</a></li>
          <li class="active">Danh sách</li>
        </ol>
    </section>

    <section class="content">
        @if(Session::has('success'))
            <div class="callout alert alert-success col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto" role="alert" >
                <button type="button" class="close">×</button> 
                {{Session::get('success')}}
            </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <a href="{{url("admin/feedback/create")}}"  class="btn btn-primary" title=""><i class="fa fa-plus"></i> Thêm</a>

                        <div class=" col-xs-3 form-group" style="float:right">
                            <select class="styled hasCustomSelect form-control" style="width:150px; float:left">
                                <option>Chọn tác vụ</option>
                                <option value="remove">Xóa</option>
                            </select>
                            <a href="javascript:action();" class="btn btn-info">Apply</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="card-block p-t-25">
                            <div class ="row col-lg-12"> 
                                 <table class="respontables">
                                    <tr>
                                        <th style="width: 1%"> <input type="checkbox" class="check-all"></th>
                                        <th class="text-center">Tên khách hàng</th>
                                        <th class="text-center">Địa chỉ</th>
                                        <th class="text-center">Avata</th>
                                        <th class="text-center">Nội dung</th>
                                        <th class="text-center">Trạng thái</th>
                                        <th class="text-center">Create</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    @if($feedback->count()>0)
                                        @foreach($feedback as $val)
                                        <tr>
                                            <td>
                                            <input type="checkbox" class="checkbox" name="checkbox_id" value="{{$val->id}}" id="select-{{$val->id}}">
                                            </td>
                                            <td class="text-center">{!! $val->name !!}</td>
                                            <td class="text-center">{!! $val->address !!}</td>
                                            <td class="text-center"><img src="{{$val->image}}" alt="" width="60" height="60"></td>
                                            <td class="text-center">{!! $val->content !!}</td>
                                            <td class="text-center">
                                                @if($val->publish == 1)
                                                <i class="fa fa-check text-success"></i>
                                                @else
                                                <i class="fa fa-times text-danger"></i>
                                                @endif
                                            </td>
                                            <td class="text-center">{!! $val->created_at->format('d-m-Y') !!}</td>
                                            <td class="text-center">
                                                <a href="/admin/feedback/{!! $val->id !!}/edit" class="btn btn-primary" title=""><i class="fa fa-edit"></i></a>
                                                <a href="/admin/feedback/{{$val->id}}/delete" class="btn btn-danger" title="" onclick="return confirm('Cảnh báo: Bạn chắc chắn muốn xóa feedback này?')"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                    <tr><td colspan="8"><h4>Không có dữ liệu để hiển thị</h4></td></tr>  
                                    @endif      
                                </table>   
                            </div>
                            <div style="clear:both"></div>
                            <div class="row col-lg-4 col-md-6 col-sm-6" style="float:right">
                                {{$feedback->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{csrf_field()}}
    </section>
    
@endsection