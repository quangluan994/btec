@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Chỉnh sửa chuyên mục
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- plugin styles-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Chỉnh sửa chuyên mục</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('admin/danh-muc') }}">Danh mục</a></li>
          <li class="active">Chỉnh sửa</li>
        </ol>
    </section>
    <section class="content">
        @if ($errors->any())
        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
            <button type="button" class="close">×</button>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      
                        <div class="col-xs-6"><h4 style="font-weight: bolder">Chỉnh sửa chuyên mục: {!!$catepost->name!!}</h4></div>
                      
                        <div class="col-xs-6" ><a href="{{url("admin/chuyen-muc")}}"  class="btn btn-info" title="" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i> Quay lại</a></div>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal login_validator" id="tryitForm" action="{!! url("admin/chuyen-muc")!!}/{!!$catepost->id!!}/update" method="post">
                            <div class="col-12">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Tên chuyên mục *</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="name" id="name" class="form-control" value="{!!$catepost->name!!}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Slug *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="slug" id="slug" class="form-control" value="{!!$catepost->slug!!}" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">URL: </label>
                                    <div class="col-xl-7 col-lg-8">
                                        /chuyen-muc-tin/<span id="url_slug">{!!$catepost->slug!!}</span>.html
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="parent_id" class="col-lg-3 control-label">Chọn chuyên mục cha *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <select required name="parent_id" id="parent_id" class="validate[required] form-control select2">
                                            @if($catepost->parent_id !=0 && $catepost->parent_id != NULL)
                                            <option value="{!! $catepost->parent_cate->id !!}" style="text-align: center" selected >{!! $catepost->parent_cate->name !!}</option>
                                            <option value="0">-- Chọn chuyên mục cha --</option>
                                            <?php showCateselected($cate,$catepost); ?>
                                            @else
                                                <option value="0">-- Chọn chuyên mục cha --</option>
                                                <?php showCategories($cate); ?>
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-xl-7 col-lg-8 add_user_checkbox_error push-lg-3">
                                        <div>
                                            <label class="custom-control custom-checkbox">
                                                <input id="publish" type="checkbox" name="publish"
                                                       class="custom-control-input" {!! $catepost->publish ? 'checked' : '' !!} >
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description"> Publish </span>
                                            </label>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <h3>SEO</h3>
                                        <hr style="margin: 0">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_title" class="col-lg-3 control-label">Seo title (<span id="count_seo_title">{{mb_strlen($catepost->seo_title, 'UTF-8')}}</span>)</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <input type="text" name="seo_title" id="seo_title" class="form-control" value="{!!$catepost->seo_title!!}" onKeyDown="countNumseo_title(this.form.seo_title);" onKeyUp="countNumseo_title(this.form.seo_title);">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_keyword" class="col-lg-3 control-label">Seo keyword (<span id="count_seo_keyword">{{mb_strlen($catepost->seo_keyword, 'UTF-8')}}</span>)</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <input type="text" name="seo_keyword" id="seo_keyword" class="form-control" value="{!!$catepost->seo_keyword!!}" onKeyDown="countNumseo_keyword(this.form.seo_keyword);" onKeyUp="countNumseo_keyword(this.form.seo_keyword);">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_description" class="col-lg-3 control-label">Seo descripton (<span id="count_seo_description">{{mb_strlen($catepost->seo_description, 'UTF-8')}}</span>)</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <textarea name="seo_description" class="form-control my-editor" rows="7" id="seo_description" onKeyDown="countNumseo_description(this.form.seo_description);" onKeyUp="countNumseo_description(this.form.seo_description);">{{$catepost->seo_description}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-lg-9 push-lg-3">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Cập nhật</button>
                                        <button class="btn btn-warning" type="reset" id="clear"><i class="fa fa-refresh"></i>Nhập lại</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $("#slug").keyup(function(){
            var slug = $("#slug").val();
            $('#url_slug').html(slug);
        });
    </script>
@endsection
