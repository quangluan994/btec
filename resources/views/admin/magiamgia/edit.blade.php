@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Chỉnh sửa mã giảm giá
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- plugin styles-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')


<section class="content-header">
        <h1>Chỉnh sửa bài viết</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('admin/ma-giam-gia') }}">Mã giảm giá</a></li>
          <li class="active">Chỉnh sửa</li>
        </ol>
    </section>
    <section class="content">
        @if ($errors->any())
        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
            <button type="button" class="close">×</button>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      
                        <div class="col-xs-6"><h4 style="font-weight: bolder">Chỉnh sửa mã giảm giá</h4></div>
                        <div class="col-xs-6" ><a href="{{url("admin/ma-giam-gia")}}"  class="btn btn-info" title="" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i> Quay lại</a></div>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal login_validator" id="tryitForm" action="{!!url("admin/ma-giam-gia")!!}/{!! $discount_code->id !!}/update" method="post">
                            <div class="col-12">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Tên(mô tả)</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="name" id="name" value="{{$discount_code->name}}" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="code_value" class="col-lg-3 control-label">Mã*</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="btn btn-primary input-group-addon" onclick="getRandomcode()"> <i class="fa fa-random" aria-hidden="true"></i> Random</span>
                                            <input type="text" name="code_value" id="code_value" class="form-control" value="{{$discount_code->code_value}}" readonly>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="type" class="col-lg-3 control-label">Loại mã</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <div class="radio">
                                                <label>
                                                  <input type="radio" name="type" id="optionsRadios1" onclick="changTypeNb()" value="So_luong" @php echo ($discount_code->type == 'So_luong') ? 'checked' : '' @endphp> Giới hạn số lượng
                                                </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label>
                                                  <input type="radio" name="type" id="optionsRadios2" onclick="changTypeDate()" value="Theo_ngay" @php echo ($discount_code->type == 'Theo_ngay') ? 'checked' : '' @endphp> Giảm giá theo ngày
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="discount" class="col-lg-3 control-label">Giá trị(%)</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="number" name="discount" id="discount" class="form-control" min="0" max="100" value="{{$discount_code->discount}}" required>
                                        </div>
                                    </div>
                                </div>
                                
                                @if($discount_code->type == 'So_luong')
                                <div class="form-group" id="number_type">
                                    <label for="number_code" class="col-lg-3 control-label">Số lượng</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="number" name="number_code" id="number_code" class="form-control" min="0" value="{{$discount_code->number_code}}" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="form_date_type" style="display:none">
                                    <label for="from_date" class="col-lg-3 control-label">Từ ngày</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <input type="date" name="from_date" id="from_date" class="form-control" value="{{$discount_code->from_date}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="to_date_type" style="display:none">
                                    <label for="to_date" class="col-lg-3 control-label">Đến ngày</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <input type="date" name="to_date" id="to_date" class="form-control" value="{{$discount_code->to_date}}">
                                        </div>
                                    </div>
                                </div>
                                @endif

                                @if($discount_code->type == 'Theo_ngay')
                                <div class="form-group" id="number_type" style="display:none">
                                    <label for="number_code" class="col-lg-3 control-label">Số lượng</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="number" name="number_code" id="number_code" class="form-control" min="0" value="{{$discount_code->number_code}}" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="form_date_type">
                                    <label for="from_date" class="col-lg-3 control-label">Từ ngày</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <input type="date" name="from_date" id="from_date" class="form-control" value="{{$discount_code->from_date}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="to_date_type">
                                    <label for="to_date" class="col-lg-3 control-label">Đến ngày</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <input type="date" name="to_date" id="to_date" class="form-control" value="{{$discount_code->to_date}}">
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-lg-9 push-lg-3">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Cập nhật</button>
                                        <button class="btn btn-warning" type="reset" id="clear"><i class="fa fa-refresh"></i> Nhập lại</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script src="{{asset('assets/fancybox/source/jquery.fancybox.js')}}"></script>
<script>
    function changTypeNb(){
        $('#number_type').show();
        $('#form_date_type').hide();
        $('#to_date_type').hide();
    }
    function changTypeDate(){
        $('#number_type').hide();
        $('#form_date_type').show();
        $('#to_date_type').show();
    }    
</script>
@endsection