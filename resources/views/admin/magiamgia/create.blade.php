@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Thêm mã
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- plugin styles-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}"/>
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}"/>
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Thêm mã</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('admin/bai-viet') }}">Mã giảm giá</a></li>
          <li class="active">Thêm</li>
        </ol>
    </section>
    <section class="content">
        @if ($errors->any())
        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
            <button type="button" class="close">×</button>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      
                        <div class="col-xs-6"><h4>Thêm mã giảm gái</h4></div>
                        <div class="col-xs-6" ><a href="{{url("admin/ma-giam-gia")}}"  class="btn btn-info" title="" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i> Quay lại</a></div>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal login_validator" id="tryitForm" action="{!!url("admin/ma-giam-gia")!!}" method="post">
                            <div class="col-12">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Tên(mô tả)</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="name" id="name" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="code_value" class="col-lg-3 control-label">Mã*</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="btn btn-primary input-group-addon" onclick="getRandomcode()"> <i class="fa fa-random" aria-hidden="true"></i> Random</span>
                                            <input type="text" name="code_value" id="code_value" class="form-control" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="type" class="col-lg-3 control-label">Loại mã</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <div class="radio">
                                                <label>
                                                  <input type="radio" name="type" id="optionsRadios1" value="So_luong" checked onclick="changTypeNb()"> Giới hạn số lượng
                                                </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label>
                                                  <input type="radio" name="type" id="optionsRadios2" value="Theo_ngay" onclick="changTypeDate()"> Giảm giá theo ngày
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="discount" class="col-lg-3 control-label">Giá trị(%)</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="number" name="discount" id="discount" class="form-control" min="0" max="100" value="0" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="number_type">
                                    <label for="number_code" class="col-lg-3 control-label">Số lượng</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="number" name="number_code" id="number_code" class="form-control" min="0" value="0" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="form_date_type" style="display:none">
                                    <label for="from_date" class="col-lg-3 control-label">Từ ngày</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <input type="date" name="from_date" id="from_date" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="to_date_type" style="display:none">
                                    <label for="to_date" class="col-lg-3 control-label">Đến ngày</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <input type="date" name="to_date" id="to_date" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-lg-9 push-lg-3">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-plus"></i>Thêm</button>
                                        <button class="btn btn-warning" type="reset" id="clear"><i class="fa fa-refresh"></i>Nhập lại</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

<script src="{{asset('assets/fancybox/source/jquery.fancybox.js')}}"></script>
<!-- end of page level js -->
<script>
    // $("#name").keyup(function(){
    //     var name = $("#name").val();
    //     var token = $('input[name=_token]').val();
    //     if(name) {
    //         $.ajax({
    //         type: 'post',
    //         url: '/slugPost',
    //         headers: { 'X-XSRF-TOKEN' : token },
    //             beforeSend: function (xhr) {
    //                 xhr.setRequestHeader('X-CSRF-Token', token);
    //             },
    //             data: {type: 'ajax', _csrfToken : token},
    //         data: {name: name},
    //         success: function(data) {
    //             data = $.parseJSON(data);
    //             $('#slug').val(data);
    //         },
    //         error: function(error) {
    //             console.log('error');
    //         }
    //     });        
    //     }
    // });
    function getRandomcode() {
        var token = $('input[name=_token]').val();
         $.ajax({
            type: 'post',
            url: '/randomPost',
            headers: { 'X-XSRF-TOKEN' : token },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', token);
                },
                data: {type: 'ajax', _csrfToken : token},
            data: {name: name},
            success: function(data) {
                data = $.parseJSON(data);
                $('#code_value').val(data);
            },
            error: function(error) {
                console.log('error');
            }
        });      
    }
    function changTypeNb(){
        $('#number_type').show();
        $('#form_date_type').hide();
        $('#to_date_type').hide();
    }
    function changTypeDate(){
        $('#number_type').hide();
        $('#form_date_type').show();
        $('#to_date_type').show();
    }
</script>
@endsection

