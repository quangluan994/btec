@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Tùy chọn hiển thị
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!-- plugin styles-->
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')}}" />
<link type="text/css" rel="stylesheet" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}" />
<!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Tùy chọn hiển thị</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li class="active">Hiển thị</li>
        </ol>
    </section>
    <section class="content">
        @if(Session::has('success'))
            <div class="callout alert alert-success col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto" role="alert" >
                <button type="button" class="close">×</button> 
                {{Session::get('success')}}
            </div>
        @endif

        @if ($errors->any())
        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
            <button type="button" class="close">×</button>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    {{-- Số lượng sản phẩm trong 1 trang sản phẩm --}}
                    <div class="box-header with-border">
                      <h5 style="font-weight: bolder">Số lượng item trong 1 trang sản phẩm:</h5>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal login_validator" id="tryitForm" action="{!!url("admin/tuy-chon-hien-thi")!!}/so-luong-san-pham/update" method="post">
                            <div class="col-12">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="count_item_product" class="col-lg-3 control-label">Giá trị: </label>
                                    <div class="col-xl-5 col-lg-5 ">
                                        <input type="number" name="count_item_product" id="count_item_product" value="{{$pagi_product}}" class="form-control" min="1" max="50" required>
                                    </div>
                                    <div class="col-xl-4 col-lg-3">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Cập nhật</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <hr>
                    
                    {{-- Số lượng tin tức trong 1 trang tin tức --}}
                    <div class="box-header with-border">
                      <h5 style="font-weight: bolder">Số lượng item trong 1 trang tin tức:</h5>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal login_validator" id="tryitForm" action="{!!url("admin/tuy-chon-hien-thi")!!}/so-luong-tin-tuc/update" method="post">
                            <div class="col-12">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="count_item_new" class="col-lg-3 control-label">Giá trị: </label>
                                    <div class="col-xl-5 col-lg-5 ">
                                        <input type="number" name="count_item_new" id="count_item_new" value="{{$pagi_new}}" class="form-control" min="1" max="50" required>
                                    </div>
                                    <div class="col-xl-4 col-lg-3">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Cập nhật</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <hr>

                    {{-- Color_skin template admin --}}
                    <div class="box-header with-border">
                      <h5 style="font-weight: bolder">Skin admin:</h5>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal login_validator" id="tryitForm" action="{!!url("admin/tuy-chon-hien-thi")!!}/mau-sac-template-admin/update" method="post">
                            <div class="col-12">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="skin_color_admin" class="col-lg-3 control-label">Màu sắc: </label>
                                    <div class="col-xl-5 col-lg-5 ">
                                        <select name = 'skin_color_admin' class = 'form-control' required>
                                            <option value="skin-blue" <?php echo ($skin_color_admin == 'skin-blue') ? 'selected' : '' ?> style="color:blue">Skin-blue</option>
                                            <option value="skin-red" <?php echo ($skin_color_admin == 'skin-red') ? 'selected' : '' ?> style="color:red">Skin-red</option>
                                            <option value="skin-yellow" <?php echo ($skin_color_admin == 'skin-yellow') ? 'selected' : '' ?> style="color:yellow">Skin-yellow</option>
                                            <option value="skin-purple" <?php echo ($skin_color_admin == 'skin-purple') ? 'selected' : '' ?> style="color:purple">Skin-purple </option>
                                            <option value="skin-green " <?php echo ($skin_color_admin == 'skin-green') ? 'selected' : '' ?> style="color:green">Skin-green </option>
                                            <option value="skin-black " <?php echo ($skin_color_admin == 'skin-black') ? 'selected' : '' ?> style="color:black">Skin-black </option>
                                        </select>
                                    </div>
                                    <div class="col-xl-4 col-lg-3">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Cập nhật</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <hr>
                </div>
            </div>
        </div>

        
    </section>


@endsection