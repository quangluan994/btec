@extends('admin.layouts.default')

{{-- Page title --}}
@section('title')
Danh sách sản phẩm
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<link type="text/css" rel="stylesheet" href="{{asset('assets/css/pages/tablesid.css')}}"/>
<!--End of page level styles-->
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1><i class="fa fa-th"></i> Danh sách Sản phẩm</h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('admin/san-pham') }}">Sản phẩm</a></li>
          <li class="active">Danh sách</li>
        </ol>
    </section>

    <section class="content">
        @if(Session::has('success'))
            <div class="callout alert alert-success col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto" role="alert" >
                <button type="button" class="close">×</button> 
                {{Session::get('success')}}
            </div>
        @endif

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border text-right">
                        <div class="col-lg-2">
                            <a href="{{url("admin/san-pham/create")}}"  class="btn btn-primary" title=""><i class="fa fa-plus"></i> Thêm</a>
                        </div>
                        <div class="col-lg-7">
                            <form action="san-pham" method="get" accept-charset="utf-8">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <input type="text" name="name" value="{{ $name }}" placeholder="Sản phẩm..." class="form-group form-control">
                                    </div>&nbsp; &nbsp;                                
                                    <div class="col-lg-3">
                                       <select name="publish" class="form-group form-control col-lg-2">
                                           <option value="all" <?php if($publish == 'all') echo "selected"; ?>>Trạng thái</option>
                                           <option value="1" <?php if($publish == '1') echo "selected"; ?>>Enable</option>
                                           <option value="0" <?php if($publish == '0') echo "selected"; ?>>Disable</option>
                                       </select>
                                    </div>
                                   &nbsp; &nbsp;
                                    <div class="col-lg-3">
                                        <input type="submit" name="" value="Tìm kiếm" class=" form-group form-control btn btn-success">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class=" form-group fix-float">
                            <select class="styled hasCustomSelect form-control" style="width:150px; float:left">
                                <option>Chọn tác vụ</option>
                                <option value="active">Kích hoạt/Vô hiệu hóa</option>
                                <option value="remove">Xóa</option>
                            </select>
                            <a href="javascript:action();" class="btn btn-info">Apply</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="card-block p-t-25">
                            <div class ="row col-lg-12"> 
                                <table class="respontables">
                                    <tr>
                                        <th style="width: 1%"> <input type="checkbox" class="check-all"></th>
                                        <th class="text-center">Sản phẩm</th>
                                        <th class="text-center" style="width:180px;">Hình ảnh</th>
                                        <th data-th="Driver details" class="text-center"><span>Danh mục</span></th>
                                        {{-- <th class="text-center">Mô tả ngắn</th> --}}
                                        <th class="text-center">Trạng thái</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                    @if($products->count()>0)
                                        @foreach($products as $val)
                                        <tr>
                                            <td><input type="checkbox" class="checkbox" name="checkbox_id" value="{{$val->id}}" id="select-{{$val->id}}"></td>
                                            <td class="text-center">{!! $val->name !!}</td>
                                            <td class="text-center" style="width:180px"><img src="{!! $val->thumbnail !!}" alt="" width="150" height="80"></td>
                                            <td class="text-center">
                                                @if(($val->category_id != 0) && ($val->category_id !=NULL))
                                                {!! $val->cate->name !!}
                                                @else
                                                @endif
                                            </td>
                                            {{-- <td class="text-center">{!! $val->info !!}</td> --}}
                                            <td class="text-center">
                                                @if($val->publish == 1)
                                                <i class="fa fa-check text-success"></i>
                                                @else
                                                <i class="fa fa-times text-danger"></i>
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                <a href="/admin/san-pham/{!! $val->id !!}/edit" class="btn btn-primary" title=""><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-danger delitem" id="del_{{$val->id}}"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                    <tr><td colspan="6"><h4>Không có dữ liệu để hiển thị</h4></td></tr>  
                                    @endif      
                                </table>    
                            </div>
                            <div style="clear:both"></div>
                            <div class="row col-lg-4 col-md-6 col-sm-6" style="float:right">
                               {{$products->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{csrf_field()}}
    </section>
    <script>
        $(".delitem").click(function(){
            var id = $(this).attr("id");
            var splitid = id.split("_");
            var deleteid = splitid[1];
            console.log(deleteid);
            swal({
              title: "Bạn chắc chắn muốn xóa?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Ok! Xóa ngay",
              closeOnConfirm: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    

                    var token = $("input[name='_token']").val();
                    $.ajax({
                        type: 'get',
                        url: '/admin/san-pham/'+deleteid+'/delete',
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token', token);
                        },
                        success: function(response) {
                            var res = $.parseJSON(response);
                            console.log(res);
                            if(!res.status) {
                                return;
                            }
                            location.reload(true);
                            swal({
                                title: "Thành công!",
                                timer: 2000,
                                type: "success"
                            });
                        }
                    });
                }
            });
        });
    </script>
@stop
{{-- page level scripts --}}
@section('footer_scripts')

@stop