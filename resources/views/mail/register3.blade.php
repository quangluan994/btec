From: quangluan44949@gmail.com;<br />
To: quangluan4494@gmail.com;<br />
Nội dung: Có 1 đơn hàng mới<br />
<hr/>
<div class="row">
    <div class="col-lg-8">
        <table class="table table-head" border="1" cellspacing="0">
            <tbody>
                <tr>
                    <td scope="row">MÃ ĐƠN HÀNG:</td>
                    <td>NGÀY:</td>
                    <td>GIẢM GIÁ(%)</td>
                    <td>TỔNG CỘNG:</td>
                    <td>PHƯƠNG THỨC THANH TOÁN:</td>
                </tr>
                <tr>
                    <td scope="row">PAY.ORDER.{{addchart(Session::get('orderID'))}}</td>
                    <td>{{Session::get('orderCreatedat')}}</td>
                    <td>{{Session::get('orderDiscount')}}%</td>
                    <td>{{Cart::total(0)}} đ</td>
                    <td>Kiểm tra thanh toán</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-lg-8">
        <h2>Chi tiết đơn hàng</h2>
        <table class="table" border="1" cellspacing="0">
            <thead>
                <tr>
                    <th style="width: 180px"></th>
                    <th>Sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Ghi chú</th>
                    <th>Tổng cộng</th>
                </tr>
            </thead>
            <tbody>
                @if(Cart::countRows() > 0)
                @foreach(Cart::content() as $item)
                <tr>
                    <td><img src="{{$item->options->img}}" width="150px"></td>
                    <td scope="row">{{subtext(($item->name),100)}}</td>
                    <td>{{$item->qty}}</td>
                    <td>{{$item->options->color}}</td>
                    <td> {{number_format($item->subtotal)}} đ</td>
                </tr>
                @endforeach
                @endif
                <tr style="font-weight: bolder; color:red">
                    <td scope="row" colspan="2">Phương thức thanh toán</td>
                    <td scope="row" colspan="2">Kiểm tra thanh toán</td>
                </tr>
                @if($order->discount != 0)
                <tr style="font-weight: bolder; color:red">
                    <td scope="row" colspan="2">Giảm giá</td>
                    <td scope="row" colspan="2">{{$order->discount}}%</td>
                </tr>
                @endif
                <tr style="font-weight: bolder; color:red">
                    <td scope="row" colspan="2">Tổng</td>
                    <td scope="row" colspan="2">{{Cart::total(0)}} đ</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
