<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="{{asset('front-end/node_modules/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="">
    <style>
        body{
            width:80%;
            font-size: 16px;
            margin:5px auto;
        }
        header{
            width: 100%;
            height: 100px;
            background-color: rgb(218, 19, 19);
            opacity: 0.8;
        }
        .send_mail{
            width: 100%;
            margin-top: 10px;
        }
        table{
            width: 80%;
        }
        footer{
            width: 100%;
            background-color: rgb(218, 19, 19);
            margin-top: 10px;
        }

    </style>
</head>
<body>
    
    <header>
        <img src="{{$setting['logo']}}" />
    </header>

    <div class="row send_mail">
        <h3>Bạn đã hoàn thành một đơn hàng:</h3>
        <hr/>
        MÃ ĐƠN HÀNG: <span style="font-weight: bolder">PAY.ORDER.{{addchart($order->id))}}</span><br />
        NGÀY ĐẶT: <span style="font-weight: bolder">{{$order->created_at}}</span><br />
        GIẢM GIÁ: <span style="font-weight: bolder">{{$order->discount}}%</span><br />
        TỔNG CỘNG: <span style="font-weight: bolder">{{number_format($order->total_amount)}} đ</span><br />
        PHƯƠNG THỨC THANH TOÁN: 
        <span style="font-weight: bolder">
            @php
            $i = $order->pay_method;
            if($i == 'store')
                echo 'Thanh toán trực tiếp tại cửa hàng';
            elseif($i == 'cod')
                echo 'Trả tiền mặt khi giao hàng(COD)';
            else
                echo 'Thanh toán qua cổng thanh toán vtcPay';
            @endphp
        </span><br />
        <hr />
        <div class="col-lg-12">
            <h2>Chi tiết đơn hàng</h2>
            <table class="table" border="1" cellspacing="0">
                <thead>
                    <tr>
                        <th style="width: 180px"></th>
                        <th>Sản phẩm</th>
                        <th>Số lượng</th>
                        <th>Ghi chú</th>
                        <th>Tổng cộng</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($order->orderdetail as $item)
                    <tr>
                        <td><img src="{{$item->product->thumbnail}}" width="150px"></td>
                        <td scope="row">{{subtext(($item->product->name),100)}}</td>
                        <td>{{$item->quantity}}</td>
                        <td>{{$item->color}}</td>
                        <td> {{number_format(($item->price_sale)*($item->quantity))}} đ</td>
                    </tr>
                    @endforeach
                    <tr style="font-weight: bolder; color:red">
                        <td scope="row" colspan="2">Phương thức thanh toán</td>
                        <td scope="row" colspan="2">
                            @php
                            $i = $order->pay_method;
                            if($i == 'store')
                                echo 'Thanh toán trực tiếp tại cửa hàng';
                            elseif($i == 'cod')
                                echo 'Trả tiền mặt khi giao hàng(COD)';
                            else
                                echo 'Thanh toán qua cổng thanh toán vtcPay';
                            @endphp
                        </td>
                    </tr>
                    <tr style="font-weight: bolder; color:red">
                        <td scope="row" colspan="2">Giảm giá</td>
                        <td scope="row" colspan="2">{{$order->discount}}%</td>
                    </tr>
                    <tr style="font-weight: bolder; color:red">
                        <td scope="row" colspan="2">Tổng</td>
                        <td scope="row" colspan="2">{{number_format($order->total_amount)}} đ</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <footer>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="logo-bottom">
                    <a href="/"><img src="{{$setting['logo_footer']}}" alt=""></a>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <h4 class="title">{{$setting['company_name']}}</h4>
                <div class="content">
                    <p>
                        <i class="fa fa-map-marker" aria-hidden="true"></i>{{$setting['company_address']}}
                    </p>
                    <p>
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <a href="#">{{$setting['hot_line']}}</a>
                    </p>
                    <p>
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                        <a href="#">{{$setting['company_email']}}</a>
                    </p>
                    <a class="btn-hostline" href="#">Hotline: {{$setting['hot_line']}}</a>
                </div>
            </div>
        </div>
    </footer>
    
</body>
</html>
