From: luanpro9x94@gmail.com;<br />
To: quangluan4494@gmail.com;<br />
Nội dung: Có 1 đơn hàng mới<br />
<hr/>
<div class="row">
    <div class="col-lg-8">
        <table class="table table-head">
            <tbody>
                <tr>
                    <td scope="row">MÃ ĐƠN HÀNG:</td>
                    <td>NGÀY:</td>
                    @if($order->discount != 0)
                    <td>GIẢM GIÁ(%)</td>
                    @endif
                    <td>TỔNG CỘNG:</td>
                    <td>PHƯƠNG THỨC THANH TOÁN:</td>
                </tr>
                <tr>
                    <td scope="row">{{$order->id}}</td>
                    <td>{{$order->created_at}}</td>
                    @if($order->discount != 0)
                    <td>{{$order->discount}}%</td>
                    @endif
                    <td>{{number_format($order->total_amount)}} đ</td>
                    <td>Kiểm tra thanh toán</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-lg-8">
        <h2>Chi tiết đơn hàng</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>Sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Tổng cộng</th>
                </tr>
            </thead>
            <tbody>
                @foreach($order->orderdetail as $item)
                <tr>
                    <td scope="row">{{$item->product->name}}</td>
                    <td>{{$item->quantity}}</td>
                    <td> {{number_format(($item->price_sale)*($item->quantity))}} đ</td>
                </tr>
                @endforeach
                <tr style="font-weight: bolder; color:red">
                    <td scope="row" colspan="2">Phương thức thanh toán</td>
                    <td>Kiểm tra thanh toán</td>
                </tr>
                @if($order->discount != 0)
                <tr style="font-weight: bolder; color:red">
                    <td scope="row" colspan="2">Giảm giá</td>
                    <td>{{$order->discount}}%</td>
                </tr>
                @endif
                <tr style="font-weight: bolder; color:red">
                    <td scope="row" colspan="2">Tổng</td>
                    <td>{{number_format($order->total_amount)}} đ</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
