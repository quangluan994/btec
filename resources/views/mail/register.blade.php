
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
</head>
<body>
    <div class="container ">
        <div class="body-content" style="border: solid 2px #da1313;">
            <header class="clearfix" style="width: 100%;height: 100px;margin-bottom: 30px;background-color: #eee;border-bottom: solid 3px #da1313;border-top: solid 5px #da1313;">
                <div class="thumb" style="float: left;width: 30%;text-align: center;">
                    <img class="img" src="{{$setting['logo']}}" width="240px" />
                </div>
                <div class="content" style="height: 100%; position: relative; overflow: hidden;">
                    <span style="text-align: center;position: absolute;left: 0;top: 50%;
                    -webkit-transform: translateY(-50%);
                       -moz-transform: translateY(-50%);
                        -ms-transform: translateY(-50%);
                         -o-transform: translateY(-50%);
                            transform: translateY(-50%);
                            width: 100%;
                            text-align: center;
                            font-size: 24px;
                            color: #da1313; ">ĐƠN HÀNG MỚI</span>
                </div>
            </header>

            <div class="send_mail" style="width: 90%;
            margin: 0 auto;">
                <h3>Có 1 đơn hàng mới:</h3>
                MÃ ĐƠN HÀNG: <span style="font-weight: bolder">PAY.ORDER.{{addchart(Session::get('orderID'))}}</span><br />
                NGÀY ĐẶT: <span style="font-weight: bolder">{{Session::get('orderCreatedat')}}</span><br />
                GIẢM GIÁ: <span style="font-weight: bolder">{{Session::get('orderDiscount')}}%</span><br />
                TỔNG CỘNG: <span style="font-weight: bolder">{{Cart::total(0)}} đ</span><br />
                PHƯƠNG THỨC THANH TOÁN: 
                <span style="font-weight: bolder">
                    @php
                    $i = Session::get('orderPaymethod');
                    if($i == 'store')
                        echo 'Thanh toán trực tiếp tại cửa hàng';
                    elseif($i == 'cod')
                        echo 'Trả tiền mặt khi giao hàng(COD)';
                    else
                        echo 'Thanh toán qua cổng thanh toán vtcPay';
                    @endphp
                </span><br />
                <hr />
                <div class="">
                    <h2>Chi tiết đơn hàng</h2>
                   
                </div>
            </div>
            <table class="table" border="1" cellspacing="0" style="width: 100%;">
                <thead>
                    <tr>
                        <th style="width: 180px"></th>
                        <th>Sản phẩm</th>
                        <th>Số lượng</th>
                        <th>Ghi chú</th>
                        <th>Tổng cộng</th>
                    </tr>
                </thead>
                <tbody>
                    @if(Cart::countRows() > 0)
                    @foreach(Cart::content() as $item)
                    <tr>
                        <td><img src="{{$item->options->img}}" width="150px"></td>
                        <td>{{subtext(($item->name),100)}}</td>
                        <td>{{$item->qty}}</td>
                        <td>{{$item->options->color}}</td>
                        <td> {{number_format($item->subtotal)}} đ</td>
                    </tr>
                    @endforeach
                    @endif
                    <tr style="font-weight: bolder; color:red">
                        <td colspan="3">Phương thức thanh toán</td>
                        <td colspan="2" style="text-align: right">Kiểm tra thanh toán</td>
                    </tr>
                    @if(Session::has('orderDiscount'))
                    <tr style="font-weight: bolder; color:red">
                        <td colspan="3">Giảm giá</td>
                        <td colspan="2" style="text-align: right">{{Session::get('orderDiscount')}}%</td>
                    </tr>
                    @endif
                    <tr style="font-weight: bolder; color:red">
                        <td colspan="3">Tổng</td>
                        <td colspan="2" style="text-align: right">{{Cart::total(0)}} đ</td>
                    </tr>
                </tbody>
            </table>
            <footer style="border-bottom: solid 5px #da1313;
            border-top: solid 3px #da1313;   
            width: 100%;
            background-color: #eee;
            margin-top: 40px;clear:both; overflow: hidden;">
                
                <div class="row" style="width: 100%">
                    <div class="col-md-4 col-lg-4" style="float: left;width: 30%;text-align: center; margin-right:40px; ">
                        <div class="logo-bottom">
                            <a href="/"><img src="{{$setting['logo']}}" width="240px"></a>
                        </div>
                    </div>
                    <div class="col-md-8 col-lg-8" style="float:left">
                        <h4 class="title">{{$setting['company_name']}}</h4>
                        <div class="content">
                            <p>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>{{$setting['company_address']}}
                            </p>
                            <p>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <a href="#">{{$setting['hot_line']}}</a>
                            </p>
                            <p>
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <a href="#">{{$setting['company_email']}}</a>
                            </p>
                        </div>
                    </div>
                </div>
               
            </footer>
        </div>
    </div>
</body>
</html>

