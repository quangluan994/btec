<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
</head>
<body>
    <div class="container ">
        <div class="body-content">
            <header class="clearfix" style="width: 100%;height: 50px;margin-bottom: 30px;background-color: #eee;border-bottom: solid 3px #da1313">
                <div class="content" style="height: 100%; position: relative; overflow: hidden;">
                    <span style="text-align: center;position: absolute;left: 0;top: 50%;
                    -webkit-transform: translateY(-50%);
                       -moz-transform: translateY(-50%);
                        -ms-transform: translateY(-50%);
                         -o-transform: translateY(-50%);
                            transform: translateY(-50%);
                            width: 100%;
                            text-align: center;
                            font-size: 24px;
                            color: #da1313; ">THÔNG TIN ĐĂNG KÝ THÀNH VIÊN</span>
                </div>
            </header>

            <div class="send_mail" style="width: 90%;
            margin: 0 auto;">
                <h3>Cảm ơn bạn đã đăng ký thành viên tại {{$setting['company_name']}}</h3>
                <div class="">
                    <h3>Thông tin thành viên:</h3>
                    Họ và tên: <b>{{$data['name']}}</b>
                    Email: <b>{{$data['email']}}</b>
                </div>
            </div>
            <footer style="
            border-top: solid 2px #da1313;   
            width: 100%;
            background-color: #eee;
            margin-top: 40px;clear:both; overflow: hidden;">
                
                <div class="row" style="width: 100%">
                    <div class="col-md-4 col-lg-4" style="float: left;width: 30%;text-align: center; margin-right:40px; ">
                        <div class="logo-bottom">
                            <a href="/"><img src="{{$setting['logo']}}"></a>
                        </div>
                    </div>
                    <div class="col-md-8 col-lg-8" style="float:left">
                        <h4 class="title">{{$setting['company_name']}}</h4>
                        <div class="content">
                            <p>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>{{$setting['company_address']}}
                            </p>
                            <p>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <a href="#">{{$setting['hot_line']}}</a>
                            </p>
                            <p>
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <a href="#">{{$setting['company_email']}}</a>
                            </p>
                        </div>
                    </div>
                </div>
               
            </footer>
        </div>
    </div>
</body>
</html>

