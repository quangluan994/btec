<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    protected $table = 'categories';

    use Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function sub_cate(){
    	return $this->hasMany('App\Category','parent_id');
    }

    public function parent_cate(){
    	return $this->belongsTo('App\Category','parent_id','id');
    }
    public function product()
    {
        return $this->hasMany('App\Product','category_id','id');
    }
    public function product1()
    {
        return $this->hasManyThrough('App\Product', 'App\Category','parent_id','category_id','id');
    }
}
