<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $table='colors';
    public function product(){
    	return $this->belongsToMany('App\Product','product_color','color_id','product_id');
    }

    public function colorProduct(){
        return $this->hasMany('App\Product_Color','color_id','id');
    }
}
