<?php
function changTitle($str,$strSymbol='-',$case=MB_CASE_LOWER){
	$str=trim($str);
	if($str=="") return "";
	$str = str_replace('"','',$str);
	$str = str_replace("'",'',$str);
	$str = stripUnicode($str);
	$str = mb_convert_case($str,$case,'utf-8');
	$str = preg_replace('/[\W|_]+/',$strSymbol,$str);
	return $str;
}

function stripUnicode($str){
	if(!$str) return '';
	$unicode = array(
			'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
            'd'=>'đ|Đ',
            'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
            'i'=>'í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị',
            'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
            'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
            'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',

		);
	foreach($unicode as $nonUni=>$uni){
		$arr = explode("|",$uni);
		$str = str_replace($arr,$nonUni,$str);
	}
	return $str;
}
// function subMenu($data,$id){
// 	foreach ($data as $item) {

// 	}
// }

function showCategories($categories, $parent_id = 0, $char = '')
{
    foreach ($categories as $key => $item)
    {
        // Nếu là chuyên mục con thì hiển thị
        if ($item['parent_id'] == $parent_id)
        {
            echo '<option value="'.$item['id'].'">';
                echo $char . $item['name'];
            echo '</option>';
             
            // Xóa chuyên mục đã lặp
            unset($categories[$key]);
             
            // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
            showCategories($categories, $item['id'], $char.'|---');
        }
    }
}
function showCategoriesChecked($categories,$project, $parent_id = 0, $char = '')
{
    $ids = [];
    foreach ($project->cate as $it) {

      $ids[] = $it->id;
    }
    foreach ($categories as $key => $item)
    {
        // Nếu là chuyên mục con thì hiển thị
        if ($item['parent_id'] == $parent_id)
        {
            echo '<option value="'.$item['id'].'"';
                
                    if(in_array ($item['id'], $ids)== true){
                       echo 'selected'; 
                }
            echo '>';
                echo $char . $item['name'];
            echo '</option>';
             
            // Xóa chuyên mục đã lặp
            unset($categories[$key]);
             
            // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
            showCategoriesChecked($categories,$project, $item['id'], $char.'|---');
        }
    }
}

function demo($categories, $parent_id = 0, $char = '')
{
    foreach ($categories as $key => $item)
    {
        // Nếu là chuyên mục con thì hiển thị
        if ($item['parent_id'] == $parent_id)
        {
            echo '<tr>';
                echo '<td style="text-align:left; padding-left:30px;">';
                    echo $char . $item['name'];
                echo '</td>';
                echo '<td class="text-center">';
                    if($item['publish'] == 1){
                    echo '<i class="fa fa-check text-success"></i>';}
                    else{
                    echo '<i class="fa fa-times text-danger"></i>';}
                echo '</td>';
                echo '<td class="text-center">';
                echo '<a href="/admin/danh-muc/'.$item['id'].'/edit" class="btn btn-primary" title=""><i class="fa fa-edit"></i></a>';
                echo '&nbsp;&nbsp;&nbsp;&nbsp;';
                echo  '<a href="/admin/danh-muc/'.$item['id'].'/delete" class="btn btn-danger" title="" onclick="return confirm(\'Cảnh báo: Xóa dữ liệu này sẽ có thể ảnh hưởng dữ liệu tới các mục khác. Bạn chắc chắn muốn xóa tin tức này?\')"><i class="fa fa-trash"></i></a>';
                echo '</td>';
            echo '</tr>';
            // Xóa chuyên mục đã lặp
            unset($categories[$key]);
            // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
            demo($categories, $item['id'], $char.'|----');
        }
    }
}

function showCateNew($categories, $parent_id = 0, $char = '')
{
    foreach ($categories as $key => $item)
    {
        // Nếu là chuyên mục con thì hiển thị
        if ($item['parent_id'] == $parent_id)
        {
            echo '<tr>';
                echo '<td style="text-align:left; padding-left:30px;">';
                    echo $char . $item['name'];
                echo '</td>';
                echo '<td class="text-center">';
                    if($item['publish'] == 1){
                    echo '<i class="fa fa-check text-success"></i>';}
                    else{
                    echo '<i class="fa fa-times text-danger"></i>';}
                echo '</td>';
                 echo '<td class="text-center">';
                echo '<a href="/admin/chuyen-muc/'.$item['id'].'/edit" class="btn btn-primary" title=""><i class="fa fa-edit"></i></a>';
                echo '&nbsp;&nbsp;&nbsp;&nbsp;';
                echo  '<a href="/admin/chuyen-muc/'.$item['id'].'/delete" class="btn btn-danger" title="" onclick="return confirm(\'Cảnh báo: Xóa dữ liệu này sẽ có thể ảnh hưởng dữ liệu tới các mục khác. Bạn chắc chắn muốn xóa chuyên mục này?\')"><i class="fa fa-trash"></i></a>';
                echo '</td>';
            echo '</tr>';
             
            // Xóa chuyên mục đã lặp
            unset($categories[$key]);
             
            // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
            showCateNew($categories, $item['id'], $char.'|----');
        }
    }
}

function showListMenu($categories, $parent_id = 0, $char = '')
{
    foreach ($categories as $key => $item)
    {
        {
        // Nếu là chuyên mục con thì hiển thị
        if ($item['parent_id'] == $parent_id)
        {
            echo '<tr>';
                echo '<td style="text-align:left; padding-left:30px;">';
                    echo $char . $item['name'];
                echo '</td>';
                echo '<td style="text-center">';
                    echo $item['position'];
                echo '</td>';
                echo '<td style="text-align:left; padding-left:30px;">';
                    echo $item['link'];
                echo '</td>';
                echo '</td>';
                echo '<td style="text-align:left; padding-left:30px;">';
                    echo $char.$item['ord'];
                echo '</td>';
                echo '<td class="text-center">';
                    if($item['publish'] == 1){
                    echo '<i class="fa fa-check text-success"></i>';}
                    else{
                    echo '<i class="fa fa-times text-danger"></i>';}
                echo '</td>';
                echo '<td class="text-center">';
                echo  '<a href="/admin/menu/'.$item['id'].'/edit" class="btn btn-primary" title=""><i class="fa fa-edit"></i></a>&nbsp; &nbsp;&nbsp; &nbsp;';
                echo  '<a href="/admin/menu/'.$item['id'].'/delete" class="btn btn-danger" title="" onclick="return confirm(\'Cảnh báo: Xóa dữ liệu này sẽ có thể ảnh hưởng dữ liệu tới các mục khác. Bạn chắc chắn muốn xóa tin tức này?\')"><i class="fa fa-trash"></i></a>';
                echo '</td>';
            echo '</tr>';
             
            // Xóa chuyên mục đã lặp
            unset($categories[$key]);
             
            // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
            showListMenu($categories, $item['id'], $char.'|----');
        }
    }
    }
}


function showMenu($categories, $parent_id = 0)
{
    // LẤY DANH SÁCH MENU CON
    $cate_child = array();
    foreach ($categories as $key => $item)
    {
        // Nếu là chuyên mục con thì hiển thị
        if ($item['parent_id'] == $parent_id)
        {
            $cate_child[] = $item;
            unset($categories[$key]);
        }
    }
     
    // HIỂN THỊ DANH SÁCH MENU CON NẾU CÓ
    if ($cate_child)
    {
        echo '<ul>';
        foreach ($cate_child as $key => $item)
        {
            // Hiển thị tiêu đề menu
            //echo '<li><a class="#" href="'.$item['link'].'">'.$item['name'].'</a>';
            if($item->sub_cate->count()>0)
            {
                echo '<li><a class="#">'.$item['name'].'</a>';
            }else{
                echo '<li><a href="'.$item['link'].'">'.$item['name'].'</a>';
            }
             
            // Tiếp tục đệ quy để tìm menu con của menu đang lặp
            showMenu($categories, $item['id']);
            echo '</li>';
        }
        echo '</ul>';
    }
}
// function showMenu($categories, $parent_id = 0)
// {
//     // LẤY DANH SÁCH MENU CON
//     $cate_child = array();
//     foreach ($categories as $key => $item)
//     {
//         // Nếu là chuyên mục con thì hiển thị
//         if ($item['parent_id'] == $parent_id)
//         {
//             $cate_child[] = $item;
//             unset($categories[$key]);
//         }
//     }
     
//     // HIỂN THỊ DANH SÁCH MENU CON NẾU CÓ
//     if ($cate_child)
//     {
//         echo '<ul>';
//         foreach ($cate_child as $key => $item)
//         {
//             // Hiển thị tiêu đề menu
//             echo '<li><a class="#" href="'.$item['link'].'">'.$item['name'].'</a>';
             
//             // Tiếp tục đệ quy để tìm menu con của menu đang lặp
//             showMenu($categories, $item['id']);
//             echo '</li>';
//         }
//         echo '</ul>';
//     }
// }


function showMenuFt($categories, $parent_id = 0)
{
    // LẤY DANH SÁCH MENU CON
    $cate_child = array();
    foreach ($categories as $key => $item)
    {
        // Nếu là chuyên mục con thì hiển thị
        if ($item['parent_id'] == $parent_id)
        {
            $cate_child[] = $item;
            unset($categories[$key]);
        }
    }
     
    // HIỂN THỊ DANH SÁCH MENU CON NẾU CÓ
    if ($cate_child)
    {
        echo '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">';
        echo '<ul>';
        foreach ($cate_child as $key => $item)
        {
            // Hiển thị tiêu đề menu
            
            echo '<div class="list_title "><h3>'.$item['name'].'</h3></div>';
            echo '<li><a href="#">'.$item['name'].'</a>';
             
            // Tiếp tục đệ quy để tìm menu con của menu đang lặp
            showMenuFt($categories, $item['id']);
            echo '</li>';
        }
        echo '</ul>';
        echo '</div>';
    }
}

function subtext($text,$num) {
        if (strlen($text) <= $num) {
            return $text;
        }
        $text= substr($text, 0, $num);
        if ($text[$num-1] == ' ') {
            return trim($text)."...";
        }
        $x  = explode(" ", $text);
        $sz = sizeof($x);
        if ($sz <= 1)   {
            return $text."...";}
        $x[$sz-1] = '';
        return trim(implode(" ", $x))."...";
}

function rand_string( $length ) {
$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
$size = strlen( $chars );
$str = '';
for( $i = 0; $i < $length; $i++ ) {
$str .= $chars[ rand( 0, $size - 1 ) ];
 }
$str = substr( str_shuffle( $chars ), 0, $length );
return $str;
}
?>