<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    protected $table = 'drag_menu_items';

    public function __construct( array $attributes = [] ){
    	//parent::construct( $attributes );
    	$this->table = config('menu.table_prefix') . config('menu.table_name_items');
    }

    public function getsons($id) {
		return $this -> where("parent", $id) -> get();
	}
	public function getall($id) {
		return $this -> where("menu", $id) -> orderBy("sort", "asc")->get();
	}

	public static function getNextSortRoot($menu){
        return self::where('menu',$menu)->where('depth',0)->max('sort') + 1;
    }

    public function belongMenu()
    {
    	return $this->belongsTo('Harimayco\Menu\Models\Menus','menu','id');
    }
    public function sub_menuitem(){
        return $this->hasMany('Harimayco\Menu\Models\MenuItems','parent');
    }
    public function parent_menu(){
        return $this->belongsTog('Harimayco\Menu\Models\MenuItems','parent','id');
    }
}
