<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'seo_title' => 'max:255',
            'seo_keyword' => 'max:255'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên danh mục',
            'name.max' => 'Độ dài tên danh mục tối đa là 255',
            'seo_title.max' => 'Độ dài seo_title tối đa là 255',
            'seo_keyword.max' => 'Độ dài seo_keyword tối đa là 255',
        ];
    }
}
