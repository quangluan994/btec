<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HinhAnhRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'description' => 'required',
            'image' => 'required|max:255',

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên cho hình ảnh',
            'name.max' => 'Độ dài tên cho hình ảnh tối đa là 255',
            'image.required' => 'Bạn chưa nhập hình ảnh',
            'image.max' => 'Độ dài hình ảnh tối đa là 255',
            'description.required' => 'Bạn chưa nhập mô tả',
        ];
    }
}
