<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NhanXetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'content' => 'required',

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên',
            'name.max' => 'Độ dài tên tối đa là 255',
            'address.required' => 'Bạn chưa nhập địa chỉ',
            'address.max' => 'Độ dài địa chỉ tối đa là 255',
            'content.required' => 'Bạn chưa nhập nội dung',
        ];
    }
}
