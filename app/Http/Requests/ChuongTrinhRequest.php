<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChuongTrinhRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'name_2' => 'required|max:255',
            'image' => 'required|max:255',
            'description' => 'required',

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên chương trình',
            'name.max' => 'Độ dài tên chương trình tối đa là 255',
            'name_2.required' => 'Bạn chưa nhập tên thứ 2 của chương trình',
            'name_2.max' => 'Độ dài tên thứ 2 của chương trình tối đa là 255',
            'image.required' => 'Bạn chưa nhập ảnh đại diện cho chương trình đào tạo',
            'image.max' => 'Độ dài đường dẫn hình ảnh tối đa là 255',
            'description.required' => 'Bạn chưa nhập mô tả cho chương trình đào tạo',
        ];
    }
}
