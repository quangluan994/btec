<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
     public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'image' => 'required|max:255',
            'link' => 'max:255',
            'alt_thumbnail' => 'max:255'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên dịch vụ',
            'name.max' => 'Độ dài tên dịch vụ tối đa là 255',
            'link.max' => 'Độ dài link tối đa là 255',
            'alt_thumbnail.max' => 'Độ dài alt hình ảnh tối đa là 255',
        ];
    }
}
