<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required',
            'password' => 'required|min:6|max:30',
            'passwordag' => 'required|same:password',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên',
            'name.max' =>'Độ dài tên tối đa là 255 ký tự',
            'email.required'  => 'Bạn chưa nhập email',
            'password.required'  => 'Bạn chưa nhập mật khẩu',
            'password.min'  => 'Độ dài tối thiểu của mật khẩu là 6 ký tự',
            'password.max' => 'Mật khẩu tối đa 30 kí tự',
            'passwordag.required' => 'Chưa nhập lại mật khẩu',
            'passwordag.same' => 'Mật khẩu nhập lại chưa khớp'
        ];
    }
}
