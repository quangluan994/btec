<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CoHoiNgheNghiepRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'comment_name' => 'required|max:255',
            'comment_introduce' => 'required|max:255',
            'comment_avata' => 'required|max:255',
            'comment_content' => 'required',

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tiêu đề cơ hội nghề nghiệp',
            'name.max' => 'Độ dài tiêu đề cơ hội nghề nghiệp tối đa là 255',
            'comment_name.required' => 'Bạn chưa nhập tên thứ 2 của chương trình',
            'comment_name.max' => 'Độ dài tên thứ 2 của chương trình tối đa là 255',
            'comment_introduce.required' => 'Bạn chưa nhập giới thiệu(Nghề nghiệp) cho người comment',
            'comment_introduce.max' => 'Độ dài phần giới thiệu tối đa là 255',
            'comment_avata.required' => 'Bạn chưa nhập ảnh đại diện cho người comment',
            'comment_avata.max' => 'Độ dài đường dẫn hình ảnh tối đa là 255',
            'comment_content.required' => 'Bạn chưa nhập nội dung',
        ];
    }
}
