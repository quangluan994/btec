<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FeedbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'content' => 'required|max:255',
            'image' => 'required|max:255',
        ];
    }


    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên người',
            'name.max' => 'Độ dài tên tối đa là 255',
            'content.required' => 'Bạn chưa nhập nội dung',
            'content.max' => 'Độ dài nội dung tối đa là 255',
            'image.required' => 'Bạn chưa nhập ảnh đại diện',
            'image.max' => 'Độ dài đường dẫn ảnh tối đa là 255',
        ];
    }
}
