<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BannerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'link' => 'required|max:255',
            'image' => 'required|max:255',
            'alt_thumbnail' => 'max:255',

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên banner',
            'name.max' => 'Độ dài tên danh mục tối đa là 255',
            'link.max' => 'Độ dài đường dẫn tối đa là 255',
            'image.max' => 'Độ dài đường dẫn hình ảnh tối đa là 255',
            'alt_thumbnail.max' => 'Độ dài alt hình ảnh tối đa là 255',
            'link.required' => 'Bạn chưa nhập url',
            'image.required' => 'Bạn chưa nhập hình ảnh banner'
        ];
    }
}
