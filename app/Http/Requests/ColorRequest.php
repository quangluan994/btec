<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ColorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'display_name' => 'required|max:255',
            'color' => 'required|max:255',
        ];
    }

    public function messages()
    {
        return [
            'display_name.required' => 'Bạn chưa nhập tên màu',
            'display_name.max' => 'Độ dài tên màu tối đa là 255',
            'color.required' => 'Bạn chưa chọn màu',
            'color.max' => 'Độ dài mã màu tối đa là 255',
        ];
    }
}
