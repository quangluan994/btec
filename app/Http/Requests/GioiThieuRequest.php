<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GioiThieuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'image' => 'required|max:255',
            'description' => 'required',

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên giới thiệu',
            'name.max' => 'Độ dài tên giới thiệu tối đa là 255',
            'image.required' => 'Bạn chưa nhập ảnh giới thiệu',
            'image.max' => 'Độ dài ảnh tối đa là 255',
            'description.required' => 'Bạn chưa nhập mô tả',
        ];
    }
}
