<?php

namespace App\Http\Middleware;
use Illuminate\Contracts\Auth\Guard;
use Redirect;
use Closure;
use Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // public function handle($request, Closure $next)
    // {
    //     // if (Auth::user() &&  Auth::user()->email == 'edutalk@gmail.com') {
    //     if (Auth::user() &&  Auth::user()->email == 'admin@admin.com') {
    //         return $next($request);
    //     }

    //     return redirect('/')->with('message', 'Bạn không có quyền truy cập vào đường dẫn này');

    // }

    protected $auth;

    public function __construct(Guard $auth){
        $this->auth = $auth;
    }
    public function handle($request, Closure $next)
    {

        if($this->auth->user() && !$this->auth->user()->is_Admin()){
            return redirect('/');
        }
        else{
        return $next($request);
    }
    }
}
