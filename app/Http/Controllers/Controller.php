<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Option;
use App\DisplayColumn;
use App\MenuItem;
use App\Menu;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    // protected function __construct(){
    //    $skin_color = DisplayColumn::where('name','=','skin_color_admin')->value('value');
    //     view()->share('skin_color',$skin_color); 
    // }
    
    protected function gOption(){
        $option = Option::all();
        $pp = array();
        foreach ($option as $p) {
            $pp[$p->key_option] = $p->value;
        }
        return $pp;
    }
    protected function getOption(){
    	$setting = $this->gOption();
        view()->share('setting',$setting);
    }

    protected function gColorSkin(){
        $option = DisplayColumn::where('name','skin_color_admin')->get();
        $pp = array();
        foreach ($option as $p) {
            $pp[$p->name] = $p->value;
        }
        return $pp;
    }
    protected function getColorSkin(){
        $getcolorskin = $this->gColorSkin();
        view()->share('getcolorskin',$getcolorskin);
    }

    protected function getMenuTop() {
        $query_getmenutop  = MenuItem::where('parent','=',0)->orderBy('sort','asc')->whereHas('belongMenu',function ($query_getmenutop){
                    $query_getmenutop->where('drag_menus.name','=','Menu top');
                });
        $top_menu = $query_getmenutop->get();
        view()->share('top_menu',$top_menu);
    }
    protected function getMenuFooter(){
    	// $menu_footer = Menu::where('position','=','footer')->where('parent_id',0)->where('publish',1)->orderBy('ord','asc')->take(4)->get();
     //    view()->share('menu_footer',$menu_footer);

        $query_getmenufooter  = MenuItem::where('parent','=',0)->orderBy('sort','asc')->whereHas('belongMenu',function ($query_getmenufooter){
                    $query_getmenufooter->where('drag_menus.name','=','Menu footer');
                });
        $footer_menu = $query_getmenufooter->get();
        view()->share('footer_menu',$footer_menu);
    }

    protected function getMenuDelivery() {
        $query_getmenudeli  = MenuItem::where('parent','=',0)->orderBy('sort','asc')->whereHas('belongMenu',function ($query_getmenudeli){
                    $query_getmenudeli->where('drag_menus.name','=','Menu Delivery');
                });
        $delivery_menu = $query_getmenudeli->get();
        view()->share('delivery_menu',$delivery_menu);
    }
}
