<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Session;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();
        return view('front-end.pages.lienhe');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $url = $request->url;
        $contact = new Contact();
        $contact->name    = $request->name;
        $contact->email   = $request->email;
        $contact->content = $request->content;
        $contact->status   = $request->status;
        $contact->save();
        Session::flash('success', 'Cảm ơn bạn đã liên hệ với chúng tôi. Nhân viên tư vấn và chăm sóc khách hàng sẽ liên hệ lại với bạn!');
        return redirect($url);
        // echo "<script>
        //     alert('Cảm ơn bạn đã liên hệ với chúng tôi. Nhân viên tư vấn và chăm sóc khách hàng sẽ liên hệ lại với bạn!');
        //     window.location = '".url($url)."'
        // </script>";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
