<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Product;
use App\Color;
use Session;
use Auth;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();
        $product_new = Product::where('publish',1)->orderby('id','desc')->take(8)->get();
        return view('front-end.pages.cart',compact('product_new'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->product_id;
        $name = $request->product_name;
        $price = $request->product_price;
        $img = $request->product_thumbnail;
        $color = Color::where('id',$request->colorcheck)->value('display_name');
        $qty = $request->product_quantity;
        $url = $request->url;

 

        Cart::add($id, $name, $qty, $price, ['img' => $img , 'color' => $color]);
        Session::flash('thanhcong', 'Đã thêm sản phẩm vào giỏ hàng');
        return redirect($url);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function saveShoppingCart()
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->qty >= 1){
            foreach (Cart::content() as $item){
                if ($item->rowId == $id){
                    Cart::update($item->rowId , ['qty' => $request->qty]);
                    break;
                }
            }
            Session::flash('success', 'Đã cập nhật số lượng sản phẩm thành công');
        } else{
            Session::flash('danger', 'Số lượng sản phẩm tối thiểu là 1');
        }
        return redirect('cart');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        foreach (Cart::content() as $item){
            if ($item->rowId == $id){
                Cart::remove($item->rowId);
                break;
            }
        }
        Session::flash('success', 'Đã xóa sản phẩm khỏi giỏ hàng');
        return redirect('cart');
    }

    public function payMent(){

        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();
        $product_new = Product::where('publish',1)->orderby('id','desc')->take(8)->get();
        return view('front-end.pages.payment',compact('product_new'));
    }
}
