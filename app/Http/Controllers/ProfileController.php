<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\Order_Product;
use Hash;
use Session;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();
        $user = User::findOrFail($id);
        $order = Order::where('user_id',$id)->orderBy('id','desc')->paginate(20);
        // $orderdetail = Orderdetail::join('orders','order_details.order_id', '=', 'orders.id')->join('products','order_details.product_id', '=', 'products.id')->where('orders.user_id', '=', $id)->orderby('order_date','desc')->get();
        return view('front-end.pages.profile', ['user' => $user, 'order' => $order]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showOrderdetail($id)
    {
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();
        $order = Order::findOrFail($id);
        $orderdetail = Order_Product::where('order_id','=',$id)->get();
        return view('front-end.pages.orderdetailprofile', ['orderdetail' => $orderdetail,'order' => $order]);
    }


    public function changPassword(Request $request, $id){
        $this->getColorSkin();
        $this->validate($request,[
            'oldpassword' => 'required',
            'passwordnew' => 'required|min:6|max:30',
            'passwordAgain' => 'required|same:passwordnew',
        ],[
            'oldpassword.required' =>'Bạn chưa nhập mật khẩu',
            'passwordnew.required' =>'Bạn chưa nhập mật khẩu',
            'passwordnew.min' => 'Mật khẩu phải có ít nhất 6 kí tự',
            'passwordnew.max' => 'Mật khẩu tối đa 30 kí tự',
            'passwordAgain.required' => 'Chưa nhập lại mật khẩu',
            'passwordAgain.same' => 'Mật khẩu nhập lại chưa khớp'
        ]);
        $current_password = \Auth::User()->password;   
        
        $oldpass = $request->oldpassword;
        if (Hash::check($oldpass, $current_password)) {
            $users = User::findOrfail($id);
            $users->password = Hash::make($request->passwordnew);
            $users->save();
            Session::flash('success','Đổi mật khẩu thành công!!!');
        }
        else{
            Session::flash('danger', 'Cảnh báo: Mật khẩu nhập vào không đúng');
        }
        return redirect('profile/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
