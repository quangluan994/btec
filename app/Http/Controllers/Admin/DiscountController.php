<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Discount_Codes;

class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->getcolorskin();
        $discount_codes = Discount_Codes::orderBy('created_at','desc')->get();
        return view('admin.magiamgia.list',compact('discount_codes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->getColorSkin();
        return view('admin.magiamgia.create');
    }

    public function getRamdomcode(Request $request)
    {
        
        $newcode_value = rand_string(9);
        $codes = Discount_Codes::orderBy('id','desc')->pluck('code_value');
        $ids = [];
        foreach ($codes as $it) {
          $ids[] = $it;
        }
        if(in_array ($newcode_value, $ids)== true){
           $newcode_value = rand_string(9);
        }
        exit(json_encode($newcode_value));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->getColorSkin();
        $codes = Discount_Codes::orderBy('id','desc')->pluck('code_value');
        $newcode_value = $request->code_value;
        $discount_code = new Discount_Codes();
        $discount_code->name      = $request->name;
        
        $ids = [];
        foreach ($codes as $it) {
          $ids[] = $it;
        }
        if(in_array ($newcode_value, $ids)== true){
           $request->session()->flash('danger', 'Giá trị mã giảm giá đã tồn tại. Vui lòng nhập lại');
        }
        else{
            $discount_code->code_value    = $request->code_value;
        }

        $discount_code->type               = $request->type;
        $discount_code->discount           = $request->discount;
        $discount_code->number_code        = $request->number_code;
        $discount_code->from_date          = $request->from_date;
        $discount_code->to_date            = $request->to_date;
        $discount_code->save();
        return redirect('admin/ma-giam-gia')->with('success','Tạo mới mã giảm giá thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->getColorSkin();
        $discount_code = Discount_Codes::findOrfail($id);          
        return view('admin.magiamgia.edit',compact('discount_code','cate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->getColorSkin();
        $discount_code = Discount_Codes::findOrfail($id);
        $discount_code->name      = $request->name;
        $discount_code->type               = $request->type;
        $discount_code->discount           = $request->discount;
        $discount_code->number_code        = $request->number_code;
        $discount_code->from_date          = $request->from_date;
        $discount_code->to_date            = $request->to_date;
        $discount_code->save();
        return redirect('admin/ma-giam-gia')->with('success','Đã cập nhật thông tin cho mã giảm giá!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->getColorSkin();
        $discount_code = Discount_Codes::findOrfail($id);
        $discount_code->delete();
        // return redirect('admin/ma-giam-gia')->with('success','Đã xóa mã giảm giá!');
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }

    public function remove(Request $request) 
    {
        $id = $request->id;
        if (is_array($id)) {
            foreach ($id as $item) {
                $list = Discount_Codes::findOrfail($item);
                $list->delete();
            }
        } else {
            
            $list = Discount_Codes::findOrfail($id);
            $list->delete();
        }
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }
}
