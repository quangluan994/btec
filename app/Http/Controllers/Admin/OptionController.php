<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use App\Option;
use URL;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->getColorSkin();
        $option_value = Option::where('publish',1)->groupBy('position')->pluck('position');
        $showOptions = [];
        foreach ($option_value as $key => $value) {
            $option_key = Option::where('publish',1)->where('position','=',$value)->get();
            $item = [
                'option_value' => $value,
                'option_key' => $option_key
            ];
            $showOptions[] = $item;
        }

// dd($showOptions);die;


        // $option_header = Option::where('position','=','header_top')->orderBy('key_option','asc')->get();
        // $option_footer = Option::where('position','=','footer_website')->orderBy('key_option','asc')->get();
        // $option_home_page = Option::where('position','=','home_page')->orderBy('key_option','asc')->get();
        // $option_info_company = Option::where('position','=','info_company')->orderBy('key_option','asc')->get();
        // $option_seo_website = Option::where('position','=','seo_website')->orderBy('key_option','asc')->get();
        // $option_add_code = Option::where('position','=','add_code')->orderBy('key_option','asc')->get();

        return view('admin.option.list',compact('showOptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->getColorSkin();
        return view('admin.option.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->getColorSkin();
        $option = new Option();
        $option->position   = $request->position;
        $option->key_option =  $request->key_option;
        $option->value      =  $request->value;
        $option->description      =  $request->description;
        $option->type       =  $request->type;
        $option->publish    =  $request->publish ? 1 : 0;
        $option->save();
        return redirect('admin/cai-dat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->getColorSkin();
        $option = Option::findOrfail($id);
        return view('admin.option.edit',compact('option' ) );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->getColorSkin();
        $option = Option::findOrfail($id);
        $option->key_option =  $request->key_option;
        $option->value      =  $request->value;
        $option->description      =  $request->description;
        $option->type       =  $request->type;
        $option->publish    =  $request->publish ? 1 : 0;
        $option->save();
        return redirect('admin/cai-dat');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->getColorSkin();
        $option = Option::findOrfail($id);
        $option->delete();
        return redirect('admin/cai-dat');
    }
}
