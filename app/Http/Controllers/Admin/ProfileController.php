<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
use App\User;
use Session;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->getColorSkin();
        $user = User::findOrFail($id);
        return view('admin.profile.profile', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->getColorSkin();
        $users = User::findOrfail($id);
        $users->name = $request->name;
        $users->email = $request->email;        
        $users->avatar = $request->avatar;        
        // $users->phone = $request->phone;  
        // $users->gender = $request->gender;
        // $users->role_user = $request->role_user;     
        // $users->address = $request->address;  
        // $users->description = $request->description;       
        
        $users->save();

        return redirect('admin/dashboard')->with('success','Cập nhật thông tin cá nhân thành công!!!');
    }
    public function changPassword(Request $request, $id){
        $this->getColorSkin();
        $this->validate($request,[
            'oldpassword' => 'required',
            'passwordnew' => 'required|min:6|max:30',
            'passwordAgain' => 'required|same:passwordnew',
        ],[
            'oldpassword.required' =>'Bạn chưa nhập mật khẩu',
            'passwordnew.required' =>'Bạn chưa nhập mật khẩu',
            'passwordnew.min' => 'Mật khẩu phải có ít nhất 6 kí tự',
            'passwordnew.max' => 'Mật khẩu tối đa 30 kí tự',
            'passwordAgain.required' => 'Chưa nhập lại mật khẩu',
            'passwordAgain.same' => 'Mật khẩu nhập lại chưa khớp'
        ]);
        $current_password = \Auth::User()->password;   
        
        $oldpass = $request->oldpassword;
        if (Hash::check($oldpass, $current_password)) {
            $users = User::findOrfail($id);
            $users->password = Hash::make($request->passwordnew);
            $users->save();
            Session::flash('success','Đổi mật khẩu thành công!!!');
        }
        else{
            Session::flash('danger', 'Cảnh báo: Mật khẩu nhập vào không đúng');
        }
        return redirect('admin/dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
