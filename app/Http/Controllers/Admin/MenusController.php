<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\MenuuRequest as MenuuRequest;
use App\Http\Controllers\Controller;
use App\Menu;
use Alert;

class MenusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->getColorSkin();
        // $menu = Menu::orderBy('position','asc')->orderBy('ord','asc')->get();
        return view('admin.menus.index',compact('menu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->getColorSkin();
        $menu = Menu::orderBy('name','asc')->where('id','<>',0)->get();
        return view('admin.menus.create',compact('menu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuuRequest $request)
    {
        $this->getColorSkin();
        $menu = new Menu();
        $menu->name      = $request->name;
        $menu->position  = $request->position;
        $menu->link      = $request->link;
        $menu->ord       = $request->ord;
        $menu->parent_id = $request->parent_id;
        $menu->publish   = $request->publish ? 1 :0;

        $menu->save();
        return redirect('admin/menu')->with('success','tạo menu thành công!');
        //Session::flash('message', 'Tạo menu thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->getColorSkin();
        $menu = Menu::findOrfail($id);
        $mn = Menu::orderBy('name','asc')->where('id','<>',0)->get()->all();                
        return view('admin.menus.edit',compact('menu','mn'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MenuuRequest $request, $id)
    {
        $this->getColorSkin();
        $menu = Menu::findOrfail($id);
        $menu->name      = $request->name;
        $menu->position  = $request->position;
        $menu->link      = $request->link;
        $menu->ord       = $request->ord;
        $menu->parent_id = $request->parent_id;
        $menu->publish   = $request->publish ? 1 :0;
        $menu->save();
        return redirect('admin/menu')->with('success','cập nhật menu thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->getColorSkin();
        $menu = Menu::findOrfail($id);
        $sub_menu = Menu::where('parent_id', '=', "$id")->get();
        foreach ($sub_menu as $sub){
            $sub->parent_id = NULL;
            $sub->publish = 0;
            $sub->save();
        }
        $menu->delete();
        return redirect('admin/menu')->with('success','xóa menu thành công!');
    }
}
