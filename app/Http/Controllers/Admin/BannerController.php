<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\BannerRequest as BannerRequest;
use App\Http\Controllers\Controller;
use App\Banner;
use DB;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $this->getColorSkin();
        $banner = Banner::orderBy('id','desc')->get();
        return view('admin.banner.list',compact('banner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $this->getColorSkin();
        return view('admin.banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannerRequest $request)
    {
        $this->getColorSkin();

        $banner = new Banner();
        $banner->name             = $request->name;
        $banner->position         = $request->position;
        $banner->publish          = $request->publish ? 1 :0;
        $banner->link             = $request->link;
        $banner->image            = $request->image;
        $banner->save();
        return redirect('admin/banner')->with('success','tạo banner thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $this->getColorSkin();
        $banner = Banner::findOrfail($id);              
        return view('admin.banner.edit',compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\BannerRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BannerRequest $request, $id)
    {   
        $this->getColorSkin();
        $banner = Banner::findOrfail($id);
        $banner->name             = $request->name;
        $banner->position         = $request->position;
        $banner->publish          = $request->publish ? 1 :0;
        $banner->link             = $request->link;
        $banner->image            = $request->image;
        $banner->save();
        return redirect('admin/banner')->with('success','update banner thành công!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->getColorSkin();
        $banner = Banner::findOrfail($id);
        $banner->delete();
        // return redirect('admin/banner')->with('success','delete banner thành công!');
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }

    public function remove(Request $request) 
    {
        $id = $request->id;
        if (is_array($id)) {
            foreach ($id as $item) {
                $list = Banner::findOrfail($item);
                $list->delete();
            }
        } else {
            
            $list = Banner::findOrfail($id);
            $list->delete();
        }
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }

    public function active(Request $request) 
    {
        $id = $request->id;
        if (is_array($id)) {
            foreach ($id as $item) {
                $list = Banner::findOrfail($item);
                if($list->publish) {
                    $list->publish = false;
                    $list->save();
                } else {
                    $list->publish = true;
                    $list->save();
                }
            }
        } else {
            $list = Banner::findOrfail($id);
            if($list->publish) {
                $list->publish = false;
                $list->save();
            } else {
                $list->publish = true;
                $list->save();
            }
        }
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }
}
