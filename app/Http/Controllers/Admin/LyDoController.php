<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\LyDoRequest as LyDoRequest;
use App\Http\Controllers\Controller;
use App\LyDo;

class LyDoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->getColorSkin();
        $lydo = LyDo::orderBy('id','desc')->get();
        return view('admin.lydo.list',compact('lydo'));
    }

    
    public function create()
    {   
        $this->getColorSkin();
        return view('admin.lydo.create');
    }

    public function store(LyDoRequest $request)
    {
        $this->getColorSkin();

        $lydo = new LyDo();
        $lydo->name             = $request->name;
        $lydo->publish          = $request->publish ? 1 :0;
        $lydo->save();
        return redirect('admin/ly-do')->with('success','Đã tạo mới lý do!');
    }

    public function edit($id)
    {   
        $this->getColorSkin();
        $lydo = LyDo::findOrfail($id);              
        return view('admin.lydo.edit',compact('lydo'));
    }

    public function update(LyDoRequest $request, $id)
    {   
        $this->getColorSkin();
        $lydo = LyDo::findOrfail($id);
        $lydo->name             = $request->name;
        $lydo->publish          = $request->publish ? 1 :0;
        $lydo->save();
        return redirect('admin/ly-do')->with('success','Đã update lý do!');

    }

    public function destroy($id)
    {
        $this->getColorSkin();
        $lydo = LyDo::findOrfail($id);
        $lydo->delete();
        // return redirect('admin/banner')->with('success','delete banner thành công!');
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }

    public function remove(Request $request) 
    {
        $id = $request->id;
        if (is_array($id)) {
            foreach ($id as $item) {
                $list = LyDo::findOrfail($item);
                $list->delete();
            }
        } else {
            
            $list = LyDo::findOrfail($id);
            $list->delete();
        }
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }

    public function active(Request $request) 
    {
        $id = $request->id;
        if (is_array($id)) {
            foreach ($id as $item) {
                $list = LyDo::findOrfail($item);
                if($list->publish) {
                    $list->publish = false;
                    $list->save();
                } else {
                    $list->publish = true;
                    $list->save();
                }
            }
        } else {
            $list = LyDo::findOrfail($id);
            if($list->publish) {
                $list->publish = false;
                $list->save();
            } else {
                $list->publish = true;
                $list->save();
            }
        }
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }
}
