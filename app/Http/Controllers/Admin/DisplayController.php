<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DisplayColumn;

class DisplayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->getColorSkin();
        $pagi_product = DisplayColumn::where('name','=','count_item_product')->value('value');
        $pagi_new = DisplayColumn::where('name','=','count_item_new')->value('value');
        $skin_color_admin = DisplayColumn::where('name','=','skin_color_admin')->value('value');
        // var_dump($skin_color_admin);die;
        return view('admin.display.index', compact('pagi_product','pagi_new','skin_color_admin'));
    }

    public function updateItemProduct(Request $request)
    {
        $this->getColorSkin();
        $record = DisplayColumn::where('name','=','count_item_product')->first();
        $record->value = $request->count_item_product;
        $record->save();

        return redirect('admin/tuy-chon-hien-thi')->with('success','Cập nhật thành công số lượng sản phẩm trên 1 trang sản phẩm');
    }

    public function updateItemNew(Request $request)
    {
        $this->getColorSkin();
        $record = DisplayColumn::where('name','=','count_item_new')->first();
        $record->value = $request->count_item_new;
        $record->save();

        return redirect('admin/tuy-chon-hien-thi')->with('success','Cập nhật thành công số lượng tin tức trên 1 trang tin tức');
    }

    public function updateColorAdmin(Request $request)
    {
        $this->getColorSkin();
        $record = DisplayColumn::where('name','=','skin_color_admin')->first();
        $record->value = $request->skin_color_admin;
        $record->save();

        return redirect('admin/tuy-chon-hien-thi')->with('success','Cập nhật thành công skin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
