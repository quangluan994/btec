<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\PostRequest as PostRequest;
use App\Http\Controllers\Controller;
use App\CatePost;
use App\Post;
use DB;
use Session;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->getColorSkin();
        // $catenew = CateNews::orderBy('name','ASC')->get();
        $query = Post::orderBy('id', 'DESC');

        $name = isset($_GET['name']) ? $_GET['name'] : '';
        $publish = isset($_GET['publish']) ? $_GET['publish'] : 'all';
        // $catenew_id = isset($_GET['catenew_id']) ? $_GET['catenew_id'] : 'all';
        if($name) {
            $query->where('name', 'like', '%'.$name.'%');
        }
        if( $publish != 'all') {
            $query->where('publish', '=', $publish);
        }
        // if($catenew_id !='all'){
        //     $query->whereHas('cate', function ($query) use ($catenew_id) {
        //         $query->where('catenews.id', '=', $catenew_id);
        //     });
        // }
        $posts = $query->paginate(15);
        return view('admin.news.list',compact('posts', 'name', 'publish','catenew_id','catenew'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->getColorSkin();
        $catepost = CatePost::orderBy('name','asc')->get()->all();
        return view('admin.news.create',compact('catepost'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $this->getColorSkin();
        $slug = Post::orderBy('created_at','desc')->pluck('slug');
        $sg = $request->slug;
        $post = new Post();
        $post->name              = $request->name;
        
        $ids = [];
        foreach ($slug as $it) {
          $ids[] = $it;
        }
        if(in_array ($sg, $ids)== true){
           $post->slug = ''; 
        }
        else{
            $post->slug    = $request->slug;
        }
        //var_dump($request->gallery);die;
        $post->category_id        = $request->category_id;
        $post->status             = $request->status;
        $post->publish            = $request->publish ? 1 :0;
        $post->image              = $request->image;
        $post->description        = $request->description;
        $post->content            = $request->content;    
        $post->seo_title          = $request->seo_title;
        $post->seo_description    = $request->seo_description;
        $post->seo_keyword        = $request->seo_keyword;
        $post->save();
        return redirect('admin/bai-viet')->with('success','Tạo mới bài viết thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->getColorSkin();
        $post = Post::findOrfail($id); 
        $cate = CatePost::orderBy('name','asc')->get()->all();            
        return view('admin.news.edit',compact('post','cate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        $this->getColorSkin();
        $slug = Post::where('id','<>',$id)->pluck('slug');
        $sg = $request->slug;
        $post = Post::findOrfail($id);
        $post->name              = $request->name;
        
        $ids = [];
        foreach ($slug as $it) {
          $ids[] = $it;
        }
        if(in_array ($sg, $ids)== true){
           $post->slug = ''; 
        }
        else{
            $post->slug    = $request->slug;
        }
        $post->category_id        = $request->category_id;
        $post->status             = $request->status;
        $post->publish            = $request->publish ? 1 :0;
        $post->image              = $request->image;
        $post->description        = $request->description;
        $post->content            = $request->content;    
        $post->seo_title          = $request->seo_title;
        $post->seo_description    = $request->seo_description;
        $post->seo_keyword        = $request->seo_keyword;
        $post->save();
        return redirect('admin/bai-viet')->with('success','Update bài viết thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->getColorSkin();
        $post = Post::findOrfail($id);
        $post->delete();
        // return redirect('admin/bai-viet')->with('success','xóa bài viết thành công!');
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }

    public function checkSlugPost(Request $request)
    {
        
        $slug  = $request->name;
        $slug = changTitle($slug);
        $checkSlug = Post::Where('slug', '=', $slug)->first();
        if($checkSlug) {
            $slug = $slug.'-1';
        }
        exit(json_encode($slug));
    }

    public function remove(Request $request) 
    {
        $id = $request->id;
        if (is_array($id)) {
            foreach ($id as $item) {
                $list = Post::findOrfail($item);
                $list->delete();
            }
        } else {
            
            $list = Post::findOrfail($id);
            $list->delete();
        }
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }

    public function active(Request $request) 
    {
        $id = $request->id;
        if (is_array($id)) {
            foreach ($id as $item) {
                $list = Post::findOrfail($item);
                if($list->publish) {
                    $list->publish = false;
                    $list->save();
                } else {
                    $list->publish = true;
                    $list->save();
                }
            }
        } else {
            $list = Post::findOrfail($id);
            if($list->publish) {
                $list->publish = false;
                $list->save();
            } else {
                $list->publish = true;
                $list->save();
            }
        }
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }
}
