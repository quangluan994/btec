<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest as CategoryRequest;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {   
        $this->getColorSkin();
        $cate = Category::all();
        return view('admin.danhmuc.list',compact('cate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->getColorSkin();
        $cate = Category::orderBy('name','asc')->where('id','<>',0)->get()->all();
        return view('admin.danhmuc.create',compact('cate'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $this->getColorSkin();
        $slug = Category::orderBy('created_at','desc')->pluck('slug');
        $sg = $request->slug;
        $category = new Category();
        $category->name             = $request->name;
        $ids = [];
        foreach ($slug as $it) {
          $ids[] = $it;
        }
        if(in_array ($sg, $ids)== true){
           $category->slug = ''; 
        }
        else{
            $category->slug    = $request->slug;
        }
        $category->publish          = $request->publish ? 1 :0;
        $category->parent_id        = $request->parent_id;
        $category->image         = $request->image;
        $category->seo_title        = $request->seo_title;
        $category->seo_description  = $request->seo_description;
        $category->seo_keyword      = $request->seo_keyword;
        $category->save();
        return redirect('admin/danh-muc')->with('success','tạo danh mục thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->getColorSkin();
        $cate = Category::findOrfail($id);
        return view('admin.danhmuc.show',compact('cate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->getColorSkin();
        $category = Category::findOrfail($id);
        $cate = Category::orderBy('name','asc')->where('id','<>',0)->get()->all();                
        return view('admin.danhmuc.edit',compact('category','cate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\CategoryRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        $this->getColorSkin();
        $slug = Category::where('id','<>',$id)->pluck('slug');
        $sg = $request->slug;
        $category = Category::findOrfail($id);
        $category->name             = $request->name;
        $ids = [];
        foreach ($slug as $it) {
          $ids[] = $it;
        }
        if(in_array ($sg, $ids)== true){
           $category->slug = ''; 
        }
        else{
            $category->slug    = $request->slug;
        }  
        $category->publish           = $request->publish ? 1 :0;
        $category->parent_id        = $request->parent_id;
        $category->image         = $request->image;
        $category->seo_title        = $request->seo_title;
        $category->seo_description  = $request->seo_description;
        $category->seo_keyword      = $request->seo_keyword;

        $category->save();
        return redirect('admin/danh-muc')->with('success','Cập nhật danh mục thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->getColorSkin();
        $category = Category::findOrfail($id);
        $sub_cate = Category::where('parent_id', '=', "$id")->get();
        $product = Product::where('category_id', '=', "$id")->get();
        foreach ($sub_cate as $sub){
            $sub->parent_id = NULL;
            $sub->publish = 0;
            $sub->save();
        }
        foreach ($product as $pro){
            $pro->category_id = NULL;
            $pro->publish = 0;
            $pro->save();
        }
        $category->delete();
        // return redirect('admin/danh-muc')->with('success','xóa danh mục thành công!');
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }

    public function checkSlugCategory(CategoryRequest $request)
    {
        $this->getColorSkin();
        $slug  = $request->name;
        $slug = changTitle($slug);
        $checkSlug = Category::Where('slug', 'like', $slug)->first();
        if($checkSlug) {
            $slug = $slug.'-1';
        }
        exit(json_encode($slug));
    }

    public function remove(Request $request) 
    {
        $id = $request->id;
        if (is_array($id)) {
            foreach ($id as $item) {
                $list = Category::findOrfail($item);
                $list->delete();
            }
        } else {
            
            $list = Category::findOrfail($id);
            $list->delete();
        }
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }

    public function active(Request $request) 
    {
        $id = $request->id;
        if (is_array($id)) {
            foreach ($id as $item) {
                $list = Category::findOrfail($item);
                if($list->publish) {
                    $list->publish = false;
                    $list->save();
                } else {
                    $list->publish = true;
                    $list->save();
                }
            }
        } else {
            $list = Category::findOrfail($id);
            if($list->publish) {
                $list->publish = false;
                $list->save();
            } else {
                $list->publish = true;
                $list->save();
            }
        }
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }
}
