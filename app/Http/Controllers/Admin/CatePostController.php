<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\CatepostRequest as CatepostRequest;
use App\Http\Controllers\Controller;
use App\CatePost;
use App\Post;

class CatePostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->getColorSkin();
        $cate = CatePost::all();
        return view('admin.chuyenmuc.list',compact('cate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->getColorSkin();
        $cate = CatePost::orderBy('name','asc')->where('id','<>',0)->get()->all();
        return view('admin.chuyenmuc.create',compact('cate'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CatepostRequest $request)
    {
        $this->getColorSkin();
        $slug = CatePost::orderBy('created_at','desc')->pluck('slug');
        $sg = $request->slug;
        $catepost = new CatePost();
        $catepost->name             = $request->name;
        $ids = [];
        foreach ($slug as $it) {
          $ids[] = $it;
        }
        if(in_array ($sg, $ids)== true){
           $catepost->slug = ''; 
        }
        else{
            $catepost->slug    = $request->slug;
        }
        $catepost->publish          = $request->publish ? 1 :0;
        $catepost->parent_id        = $request->parent_id;
        $catepost->seo_title        = $request->seo_title;
        $catepost->seo_description  = $request->seo_description;
        $catepost->seo_keyword      = $request->seo_keyword;
        $catepost->save();
        return redirect('admin/chuyen-muc')->with('success','tạo chuyên mục thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->getColorSkin();
        $catepost = CatePost::findOrfail($id);
        $cate = CatePost::orderBy('name','asc')->where('id','<>',0)->get()->all();                
        return view('admin.chuyenmuc.edit',compact('catepost','cate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\CatepostRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CatepostRequest $request, $id)
    {
        $this->getColorSkin();
        $slug = CatePost::where('id','<>',$id)->pluck('slug');
        $sg = $request->slug;
        $catepost = CatePost::findOrfail($id);
        $catepost->name             = $request->name;
        $ids = [];
        foreach ($slug as $it) {
          $ids[] = $it;
        }
        if(in_array ($sg, $ids)== true){
           $catepost->slug = ''; 
        }
        else{
            $catepost->slug    = $request->slug;
        }
        $catepost->publish          = $request->publish ? 1 :0;
        $catepost->parent_id        = $request->parent_id;
        $catepost->seo_title        = $request->seo_title;
        $catepost->seo_description  = $request->seo_description;
        $catepost->seo_keyword      = $request->seo_keyword;
        $catepost->save();
        return redirect('admin/chuyen-muc')->with('success','Update chuyên mục thành công!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->getColorSkin();
        $catepost = CatePost::findOrfail($id);
        $sub_cate = CatePost::where('parent_id', '=', "$id")->get();
        $post = Post::where('category_id', '=', "$id")->get();
        foreach ($sub_cate as $sub){
            $sub->parent_id = NULL;
            $sub->publish = 0;
            $sub->save();
        }
        foreach ($post as $pro){
            $pro->category_id = NULL;
            $pro->publish = 0;
            $pro->save();
        }
        $catepost->delete();
        // return redirect('admin/chuyen-muc')->with('success','xóa chuyên mục thành công!');
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }

    public function checkSlugCatePost(CatepostRequest $request)
    {
        $slug  = $request->name;
        $slug = changTitle($slug);
        $checkSlug = CatePost::Where('slug', 'like', $slug)->first();
        if($checkSlug) {
            $slug = $slug.'-1';
        }
        exit(json_encode($slug));
    }

    public function remove(Request $request) 
    {
        $id = $request->id;
        if (is_array($id)) {
            foreach ($id as $item) {
                $list = CatePost::findOrfail($item);
                $list->delete();
            }
        } else {
            
            $list = CatePost::findOrfail($id);
            $list->delete();
        }
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }

    public function active(Request $request) 
    {
        $id = $request->id;
        if (is_array($id)) {
            foreach ($id as $item) {
                $list = CatePost::findOrfail($item);
                if($list->publish) {
                    $list->publish = false;
                    $list->save();
                } else {
                    $list->publish = true;
                    $list->save();
                }
            }
        } else {
            $list = CatePost::findOrfail($id);
            if($list->publish) {
                $list->publish = false;
                $list->save();
            } else {
                $list->publish = true;
                $list->save();
            }
        }
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }
}
