<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\Order_Product;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->getColorSkin();
        $order = Order::orderBy('id','desc')->paginate(15);
        return view('admin.donhang.list',compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->getColorSkin();
        $order = Order::findOrFail($id);
        $orderdetail = Order_Product::where('order_id','=',$id)->get();
        return view('admin.donhang.orderdetail', ['orderdetail' => $orderdetail,'order' => $order]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->getColorSkin();
        $order = Order::findOrFail($id);
        $order->status = $request->status;
        $order->save();

        return redirect('admin/order');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->getColorSkin();
        $order = Order::findOrFail($id);
        $order_detail = Order_Product::where('order_id','=',$id)->get();
        foreach ($order_detail as $td){
            $td->delete();
        }
        $order->delete();
        // return redirect('admin/order')->with('success','Đã xóa đơn hàng!!!');
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }

    public function remove(Request $request) 
    {
        $id = $request->id;
        if (is_array($id)) {
            foreach ($id as $item) {
                $list = Order::findOrfail($item);
                $order_detail = Order_Product::where('order_id','=',$item)->get();
                foreach ($order_detail as $td){
                    $td->delete();
                }
                $list->delete();
            }
        } else {
            
            $list = Order::findOrfail($id);
            $order_detail = Order_Product::where('order_id','=',$id)->get();
                foreach ($order_detail as $td){
                    $td->delete();
                }
            $list->delete();
        }
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }
}
