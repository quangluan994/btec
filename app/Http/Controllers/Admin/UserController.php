<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\UsersRequest as UsersRequest;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->getColorSkin();
        $users = User::orderBy('created_at','desc')->paginate(20);
        return view('admin.users.list',compact('users','administrator'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->getColorSkin();
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersRequest $request)
    {
        $this->getColorSkin();
        $user = new User();
        $email = User::orderBy('created_at','desc')->pluck('email');
        $em = $request->email;
        $ids = [];
        foreach ($email as $it) {
          $ids[] = $it;
        }
        if(in_array ($em, $ids)== true){
           Session::flash('danger', 'Lỗi:email đã tồn tại trong hệ thống, vui lòng nhập lại');
           return redirect('admin/user/create');
        }
        else{
            $user->name = $request->name;
            $user->email = $request->email;
            $user->avatar = $request->avatar;
            $user->role_id = $request->role_id;
            $user->password = bcrypt($request->password);
            $user->save();
            return redirect('admin/user')->with('success','Đã tạo mới tài khoản hệ thống');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->getColorSkin();
        $user = User::findOrFail($id);
        return view('admin.users.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->getColorSkin();
        $user = User::findOrfail($id);
        $user->avatar = $request->avatar;
        $user->save();
        return redirect('admin/user')->with('success','Cập nhật ảnh đại diện thành công!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->getColorSkin();
        if (Auth::user()->is_Admin()){
                $user = User::findOrFail($id);
                if ($user->role_id == 1){
                    Session::flash('danger', 'Cảnh báo: Không thể xóa admin!');
                } else{
                    $user->delete();
                    Session::flash('success', 'Xóa tài khoản người dùng thành công!');
                }
            }else{
            Session::flash('danger', 'Cảnh báo: Bạn không phải là Admin!');
            }
        return redirect('admin/user');
    }
    public function remove(Request $request) 
    {
        $id = $request->id;
        if (is_array($id)) {
            foreach ($id as $item) {
                $list = User::findOrfail($item);
                if ($list->role_id != 1){
                $list->delete();
                }
            }
        } else {
            
            $list = Product::findOrfail($id);
            if ($list->role_id != 1){
                $list->delete();
            }
        }
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }
}
