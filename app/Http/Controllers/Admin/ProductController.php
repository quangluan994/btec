<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest as ProductRequest;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use App\Color;
use App\Product_Color;
use DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->getColorSkin();
        $query = Product::orderBy('id','desc');
        $name = isset($_GET['name']) ? $_GET['name'] : '';
        $publish = isset($_GET['publish']) ? $_GET['publish'] : 'all';
        if($name) {
            $query->where('name', 'like', '%'.$name.'%');
        }
        if( $publish != 'all') {
            $query->where('publish', '=', $publish);
        }
        $products = $query->paginate(15);
        return view('admin.sanpham.list',compact('products','name','publish'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->getColorSkin();
        $colors = Color::orderBy('display_name','asc')->get();
        $cate = Category::orderBy('name','asc')->where('id','<>',0)->get()->all();
        return view('admin.sanpham.create',compact('cate','colors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $this->getColorSkin();
        $slug = Product::orderBy('created_at','desc')->pluck('slug');
        $sg = $request->slug;
        $product = new Product();
        $product->name             = $request->name;
        $ids = [];
        foreach ($slug as $it) {
          $ids[] = $it;
        }
        if(in_array ($sg, $ids)== true){
           $product->slug = ''; 
        }
        else{
            $product->slug    = $request->slug;
        }
        $product->price            = $request->price;
        if($request->price_sale){
            $product->price_sale       = $request->price_sale;
        }else{
            $product->price_sale       = $request->price;
        }

        $product->publish          = $request->publish ? 1 :0;
        $product->thumbnail        = $request->thumbnail;
        $product->gallery          = json_encode($request->gallery);
        $product->qty_in_stock     = $request->qty_in_stock;
        $product->description      = $request->description;
        $product->category_id      = $request->category_id;
        $product->seo_title        = $request->seo_title;
        $product->seo_description  = $request->seo_description;
        $product->seo_keyword      = $request->seo_keyword;
        $product->save();

        if(!empty($request->input('color_id'))){
            foreach ($request->input('color_id') as $key => $value) {
                $pro_id = Product_Color::orderBy('id','desc')->where('product_id',$product->id)->where('color_id',$value)->first();
                if(!$pro_id){
                $product_color = new Product_Color();
                $product_color->product_id = $product->id;
                $product_color->color_id = $value;
                $product_color->quantity=$request->input('number_color')[$key];
                $product_color->save();
                }else{
                $pro_id->quantity += $request->input('number_color')[$key];
                $pro_id->save();
                }       
            }
            $quantity_color = Product_Color::where('product_id',$product->id)->sum('quantity');
            $proselect = Product::findOrfail($product->id);
            $proselect->qty_in_stock =$quantity_color;
            $proselect->save();
        }
        return redirect('admin/san-pham')->with('success','thêm mới sản phẩm thành công!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->getColorSkin();
        $product = Product::findOrfail($id);
        $colors = Color::orderBy('display_name','asc')->get();
        $cate = Category::orderBy('name','asc')->where('id','<>',0)->get()->all();                
        return view('admin.sanpham.edit',compact('product','cate','colors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\ProductRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $this->getColorSkin();
        $slug = Product::where('id','<>',$id)->pluck('slug');
        $sg = $request->slug;
        $product = Product::findOrfail($id);
        $product->name             = $request->name;
        $ids = [];
        foreach ($slug as $it) {
          $ids[] = $it;
        }
        if(in_array ($sg, $ids)== true){
           $product->slug = ''; 
        }
        else{
            $product->slug    = $request->slug;
        }
        $product->price            = $request->price;
        if($request->price_sale){
            $product->price_sale       = $request->price_sale;
        }else{
            $product->price_sale       = $request->price;
        }
        $product->publish          = $request->publish ? 1 :0;
        $product->thumbnail        = $request->thumbnail;
        $product->gallery          = json_encode($request->gallery);
        $product->qty_in_stock     = $request->qty_in_stock;
        $product->description      = $request->description;
        $product->category_id      = $request->category_id;
        $product->seo_title        = $request->seo_title;
        $product->seo_description  = $request->seo_description;
        $product->seo_keyword      = $request->seo_keyword;
        $product->save();

        DB::table("product_colors")->where("product_colors.product_id",$id)->delete();
        if(!empty($request->input('color_id'))){
            foreach ($request->input('color_id') as $key => $value) {
                $pro_id = Product_Color::orderBy('id','desc')->where('product_id',$id)->where('color_id',$value)->first();
                if(!$pro_id){
                $product_color = new Product_Color();
                $product_color->product_id = $product->id;
                $product_color->color_id = $value;
                $product_color->quantity=$request->input('number_color')[$key];
                $product_color->save();
                }else{
                $pro_id->quantity += $request->input('number_color')[$key];
                $pro_id->save();
                }
            }
            $quantity_color = Product_Color::where('product_id',$id)->sum('quantity');
            $proselect = Product::findOrfail($id);
            $proselect->qty_in_stock =$quantity_color;
            $proselect->save();
        }

        return redirect('admin/san-pham')->with('success','Update sản phẩm thành công!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->getColorSkin();
        $product = Product::findOrfail($id);
        $product_color = Product_Color::where('product_id','=',"$id")->get();
        foreach($product_color as $tc){
            $tc->delete();
        }
        $product->delete();
        // return redirect('admin/san-pham')->with('success','Đã xóa sản phẩm!');
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }
    public function checkSlugProduct(Request $request)
    {
        $this->getColorSkin();
        $slug  = $request->name;
        $slug = changTitle($slug);
        $checkSlug = Product::Where('slug', 'like', $slug)->first();
        if($checkSlug) {
            $slug = $slug.'-1';
        }
        exit(json_encode($slug));
    }

    public function remove(Request $request) 
    {
        $id = $request->id;
        if (is_array($id)) {
            foreach ($id as $item) {
                $list = Product::findOrfail($item);
                $list->delete();
            }
        } else {
            
            $list = Product::findOrfail($id);
            $list->delete();
        }
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }

    public function active(Request $request) 
    {
        $id = $request->id;
        if (is_array($id)) {
            foreach ($id as $item) {
                $list = Product::findOrfail($item);
                if($list->publish) {
                    $list->publish = false;
                    $list->save();
                } else {
                    $list->publish = true;
                    $list->save();
                }
            }
        } else {
            $list = Product::findOrfail($id);
            if($list->publish) {
                $list->publish = false;
                $list->save();
            } else {
                $list->publish = true;
                $list->save();
            }
        }
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }

    public function getColor(Request $request) 
    {
        $this->getColorSkin();
        $colors = Color::orderBy('display_name','asc')->get();
        $str =  '<div class="col-lg-12" style="margin-bottom:5px">';
        $str = $str.'<div class="col-lg-5">';
        $str = $str.    '<select name="color_id[]" class=" form-control select2">';
                            foreach($colors as $key => $item){
        $str = $str.        '<option value="'.$item['id'].'">'.$item['display_name'].'</option>';
                            }
        $str = $str.    '</select>';
        $str = $str.'</div>';
        $str = $str.'<div class="col-lg-5">';
        $str = $str.'<input type="number" name="number_color[]" class="form-control" min="0" placeholder="Số lượng" value="1">';
        $str = $str.'</div>';
        $str = $str.'<div class="col-lg-2">';
        $str = $str.'    <span class="iframe-btn-remove" onclick="removea(this)">';
        $str = $str.'    <a class="btn btn-danger red iframe-btn"><i class="fa fa-minus" aria-hidden="true"></i></a>';
        $str = $str.'    </span>';
        $str = $str.'</div>';
        $str = $str.'</div>';

        exit(json_encode($str));
    }
}
