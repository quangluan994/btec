<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ColorRequest as ColorRequest;
use App\Http\Controllers\Controller;
use App\Color;
use App\Product_Color;
use Session;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->getColorSkin();
        $colors = Color::orderBy('display_name','asc')->get();
        return view('admin.color.list',compact('colors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->getColorSkin();
        return view('admin.color.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ColorRequest $request)
    {
        $this->getColorSkin();
        $cl = Color::orderBy('created_at','desc')->pluck('color');
        $sg = $request->color;
        $color = new Color();
        $ids = [];
        foreach ($cl as $it) {
          $ids[] = $it;
        }
        if(in_array ($sg, $ids)== true){
            Session::flash('danger', 'Cảnh báo: Mã màu đã tồn tại, yêu cầu nhập lại!');
            return redirect('admin/mau-sac/create');
        }
        else{
            $color->display_name    = $request->display_name;
            $color->color           = $request->color;
            $color->save();
            return redirect('admin/mau-sac')->with('success', 'Tạo màu sản phẩm thành công!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->getColorSkin();
        $color = Color::findOrfail($id);            
        return view('admin.color.edit',compact('color'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ColorRequest $request, $id)
    {
        $this->getColorSkin();
        $cl = Color::where('id','<>',$id)->pluck('color');
        $sg = $request->color;
        $color = Color::findOrfail($id);
        $ids = [];
        foreach ($cl as $it) {
          $ids[] = $it;
        }
        if(in_array ($sg, $ids)== true){
            Session::flash('danger', 'Cảnh báo: Mã màu đã tồn tại, yêu cầu nhập lại!');
            return redirect('admin/mau-sac/'.$id.'/edit');
        }
        else{
             $color->display_name   = $request->display_name;
            $color->color           = $request->color;
            $color->save();
            return redirect('admin/mau-sac')->with('success', 'Tạo màu sản phẩm thành công!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->getColorSkin();
        $color = Color::findOrfail($id);
        $product_color = Product_Color::where('color_id','=',"$id")->get();
        foreach($product_color as $pc){
            $pc->delete();
        }
        $color->delete();
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }

    public function remove(Request $request) 
    {
        $id = $request->id;
        if (is_array($id)) {
            foreach ($id as $item) {
                $list = Color::findOrfail($item);
                $list->delete();
            }
        } else {
            
            $list = Color::findOrfail($id);
            $list->delete();
        }
        exit(json_encode(array('status' => true, 'message' => 'Thành công',)));
    }
}
