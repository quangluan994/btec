<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Order;
use App\Order_Product;
use App\User;
use App\Discount_Codes;
use Session;
use DB;
use Cart;
use Auth;
use Mail;
use Carbon\Carbon;
use App\Product;
use App\Product_Color;
use App\Jobs\SendReminderEmail;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function useDiscount(Request $request){
        $pass = $request->discount_code;
        $codes = DB::table('discount_codes')->orderBy('created_at','desc')->pluck('code_value');
        $ids = [];
        foreach ($codes as $it) {
          $ids[] = $it;
        }
        if(in_array ($pass, $ids)== true){
            // $code_date = DB::table('discount_codes')->select('from_date','to_date','discount')->where('code_value','=',$pass)->get();
            $from_date = DB::table('discount_codes')->where('code_value','=',$pass)->value('from_date');
            $to_date = DB::table('discount_codes')->where('code_value','=',$pass)->value('to_date');
            $discount = DB::table('discount_codes')->where('code_value','=',$pass)->value('discount');
            $id_discount = DB::table('discount_codes')->where('code_value','=',$pass)->value('id');
            
            $ldate = Carbon::now()->format('Y-m-d');

            $type = DB::table('discount_codes')->where('code_value','=',$pass)->value('type');
            $number_code = DB::table('discount_codes')->where('code_value','=',$pass)->value('number_code');
            if($type == 'So_luong'){
                if($number_code > 0){
                    $request->session()->flash('discount_value', $discount);
                    $request->session()->flash('usediscount_code', $pass);
                    $request->session()->flash('usediscount_id', $id_discount);
                }
                else{
                    Session::flash('danger', 'Số lần sử dụng mã giảm giá đã hết');
                }
            }
            else{
                if($ldate>=$from_date && $ldate<=$to_date){
                    $request->session()->flash('discount_value', $discount);
                    $request->session()->flash('usediscount_code', $pass);
                    $request->session()->flash('usediscount_id', $id_discount);
                }
                else{
                    Session::flash('danger', 'Mã giảm giá đã hết hạn hoặc chưa được phép giảm giá');
                }
            }
        }
        else{
            Session::flash('danger', 'Mã giảm giá bạn sử dụng không tồn tại!!!');
        }
        return redirect('check-out');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'receiver_name' => 'required|max:255',
            'receiver_phone_number' => 'required|numeric',
            'receiver_address'   => 'required|max:255',
            'receiver_email' => 'required',
        ],[
            'receiver_name.required' =>'Tên không được để trống',
            'receiver_name.max' =>'Tên dài quá 255 ký tự',
            'receiver_phone_number.required' =>'Số điện thoại không được để trống',
            'receiver_phone_number.numeric' =>'Số điện thoại nhập vào không đúng định dạng',
            'receiver_address.required' =>'Địa chỉ nhận hàng không được để trống',
            'receiver_address.max' =>'Địa chỉ dài quá 255 ký tự',
            'receiver_email.required' =>'Chưa nhập địa chỉ email'
        ]);

        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();

        if(Cart::countRows() > 0){
            $order = new Order();
            if (Auth::user()) {
            $order->user_id = Auth::user()->id;
            }else{
                $order->user_id = 0;
            }
            $order->receiver_name = $request->receiver_name;
            $order->receiver_phone_number = $request->receiver_phone_number;
            $order->receiver_address = $request->receiver_address;
            $order->receiver_email = $request->receiver_email;
            $order->receiver_note = $request->receiver_note;
            $order->discount = ($request->discount) ? $request->discount : 0;
            $order->total_amount = $request->total_amount;
            $order->pay_method = $request->pay_method;
            $order->status = 'dang_cho';
            $order->save();

            if($request->usediscount_code){
                $idcode = $request->usediscount_id;
                $discount_code = Discount_Codes::findOrfail($idcode);
                if($discount_code->type=='So_luong'){
                    $discount_code->number_code = $discount_code->number_code - 1;
                }
                $discount_code->save();
            }

            foreach (Cart::content() as $item){
                $detail = new Order_Product();
                $detail->product_id = $item->id;
                $detail->order_id = $order->id;
                $detail->price_sale = $item->price;
                $detail->color = $item->options->color;
                $detail->quantity = $item->qty;
                $detail->save();

                if($detail->color !=null){
                    $pro_color = Product_Color::where('product_id',$detail->product_id)->whereHas('color',function($query) use ($item){
                        $query->where('colors.display_name', '=', $item->options->color);
                    })->first();
                    $pro_color->quantity -= ($detail->quantity);
                    if($pro_color->quantity<0){
                        $pro_color->quantity=0;
                    }
                    $pro_color->save();
                }
                $product = Product::findOrfail($detail->product_id);
                $product->qty_in_stock -= ($detail->quantity);
                if($product->qty_in_stock<0){
                    $product->qty_in_stock=0;
                }
                $product->save();
            }

            if($request->pay_method == 'banking') {

                // $data = ['order' => $order];
                $url = $this->vtcPay($order->total_amount,$order->id);

                $request->session()->flash('orderID', $order->id);
                $request->session()->flash('orderCreatedat', $order->created_at);
                $request->session()->flash('orderDiscount', $order->discount);
                $request->session()->flash('orderPaymethod', $order->pay_method);
                $this->dispatch(new SendReminderEmail($order));

                Cart::destroy();
                return redirect($url);
                // exit(json_encode(['status' => 1, 'url' => $url]));
            }
            else{
                // $data = ['order' => $order];

                $request->session()->flash('orderID', $order->id);
                $request->session()->flash('orderCreatedat', $order->created_at);
                $request->session()->flash('orderDiscount', $order->discount);
                $request->session()->flash('orderPaymethod', $order->pay_method);
                $this->dispatch(new SendReminderEmail($order));

                Cart::destroy();
                Session::flash('success', 'Đã đặt hàng hàng thành công');
                return view('front-end.pages.order',['order' => $order]);
                // exit(json_encode(['status' => 2]));
            }


            // $data = ['order' => $order];
            // $request->session()->flash('orderID', $order->id);
            // $request->session()->flash('orderCreatedat', $order->created_at);
            // $request->session()->flash('orderDiscount', $order->discount);
            // $request->session()->flash('orderPaymethod', $order->pay_method);

            // Mail::send('mail.register', $data, function($message){
            //     $message->from(env('MAIL_USERNAME'),'Admin Shop');
            //     $message->to(env('MAIL_ADMIN_WEBSITE'))->subject('Đơn hàng mới');
            // });
            

            // $this->dispatch(new SendReminderEmail($order));

            // Cart::destroy();
            // Session::flash('success', 'Đã đặt hàng hàng thành công');
            // return view('front-end.pages.order',['order' => $order]);
        } 
        else{
            Session::flash('danger', 'Giỏ hàng của bạn trống!!!');
            return redirect('check-out');
            // return json_encode(['status' => 0, 'mess' => 'Giỏ hàng của bạn trống']);
        }
    }


    private function vtcPay($amount, $orderId ) {
        $destinationUrl = env('VTC_DESTINATION_URL');

        $plaintext = $amount."|VND|".env('VTC_RECEIVER_ACCOUT')."|".$orderId."|".env('VTC_RETURN_URL')."|".env('VTC_WEB_ID')."|".env('VTC_SECRET_KEY');
        $sign = (hash('sha256', $plaintext));
        $data = "?website_id=" .env('VTC_WEB_ID'). "&currency=VND&reference_number=" .$orderId. "&amount=" . $amount. "&receiver_account=" .env('VTC_RECEIVER_ACCOUT'). "&url_return=" .  urlencode(env('VTC_RETURN_URL')). "&signature=" . $sign;
        $destinationUrl = $destinationUrl . $data;
        return $destinationUrl;
    }


    public function vtcCallbackPost(){
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();

        $data = $_POST["data"];
        $sign = $_POST["signature"];
        $string = explode ('|' , $data);
        $amount = $string[0];
        $reference_number = $string[3];
        $status = $string[4];
        $secret_key = env('VTC_SECRET_KEY');
        $plaintext = $data . "|" . $secret_key;
        $mysign = strtoupper(hash('sha256', $plaintext));
        if($mysign === $sign) {
            $order = Order::whereId($reference_number)->first();
            if($order) {

                if($status == 1){
                    $order->status = 'da_thanh_toan';
                    $order->save();
                }
                elseif(($status == -1) || ($status == 7) || ($status == -9) || ($status == -3) || ($status == -4) || ($status == -5) || ($status == -6) || ($status == -7) || ($status == -8) || ($status == -22)){
                    $order->status = 'chua_thanh_toan';
                    $order->save();
                }
                else{
                    $order->status = 'chua_thanh_toan';
                    $order->save();
                }

                Mail::send('mail.orderuser', array('order' => $order), function($message){
                    $message->from(env('MAIL_USERNAME'), 'Admin Shop');
                    $message->to($order->receiver_email)->subject('Thông tin đơn hàng');
                });
            }
            file_put_contents('post.txt', json_encode($_POST) );
            die('ok');
        }else {
            file_put_contents('post.txt', 'error' );
            die('ok');
        }
       

    }

    public function vtcCallback(){
        
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();

        $secret_key = env('VTC_SECRET_KEY');
        $websiteid = $_GET['website_id'];
        $paymentType = $_GET['payment_type'];
        $amount = $_GET["amount"];
        $message = $_GET["message"];
        $reference_number=$_GET["reference_number"];
        $status = $_GET["status"];
        $trans_ref_no = $_GET["trans_ref_no"];
        $sign = $_GET["signature"];
        $data = $amount . "-" . $message . "-" . $reference_number. "-" . $status. "-" . $trans_ref_no. "-" . $websiteid;

        $plaintext = $amount . "|" . $message ."|".$paymentType."|" . $reference_number. "|" . $status. "|" . $trans_ref_no. "|" . $websiteid. "|" . $secret_key;
        // dd($plaintext);die;
        $mysign = strtoupper(hash('sha256', $plaintext));
        if($mysign != $sign)
        {
            return redirect('/thanh-toan-loi.html');
        }
        else
        {
            // return redirect('/thanh-toan-thanh-cong.html');
            if($status == 1){
                $order = Order::findOrfail($reference_number);
                return view('front-end.pages.order',compact('order'));
            }
            elseif($status == -1){
                return redirect('/giao-dich-that-bai.html');
            }
            elseif($status == -9){
                return redirect('/khach-hang-huy-giao-dich.html');
            }
            elseif($status == 7){
                return redirect('/tai-khoan-bi-tru-tien.html');
            }
            elseif($status == -7){
                return redirect('/sai-thong-tin-otp.html');
            }
            else{
                return redirect('/thanh-toan-loi.html');
            }
        }
    }

    public function payment_error() {
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();
        return view('front-end.pages.payment_error');
    }
    public function payment_success() {
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();

        return view('front-end.pages.payment_success');
    }
    public function pay_error() {
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();
        Session::flash('danger', 'Giao dịch thất bại!!!');
        return view('front-end.pages.pay_err');
    }

    public function pay_otp() {
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();
        Session::flash('danger', 'Lỗi: Khách hàng nhập sai thông tin thanh toán ( Sai thông tin tài khoản hoặc sai OTP)!!!');
        return view('front-end.pages.pay_err');
    }

    public function pay_cancel() {
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();
        Session::flash('danger', 'Lỗi: Khách hàng tự hủy giao dịch, giao dịch thất bại!!!');
        return view('front-end.pages.pay_err');
    }
    public function pay_minus() {
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();
        Session::flash('danger', 'Tài khoản thanh toán của khách hàng đã bị trừ tiền nhưng tài khoản của cửa hàng chưa được cộng tiền. Xin vui lòng chờ Bộ phận quản trị thanh toán của VTC duyệt để quyết định giao dịch thành công hay thất bại');
        return view('front-end.pages.pay_err');
    }



    // public function get_Register()
    // {
    //     return view('/');
    // }
    // public function post_Register(Request $request)
    // {   
    //     $email = $request->newsletter_email;

    //     $data = ['email' =>$email];
    //     Mail::send('mail.register', $data, function($message){
    //         $message->from('luanpro9x94@gmail.com','Quang Luận');
    //         $message->to('quangluan4494@gmail.com','Admin Shop')->subject('Đăng ký Email với Shop');
    //     });
    // }
}
