<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\DisplayColumn;
use App\Product;
use App\Product_Color;
use App\Project;
use App\Banner;
use App\Contact;
use App\CatePost;
use App\Post;
use DB;
class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //Trang chủ
    public function index()
    {
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();
        $bnfirst = Banner::where('position','=','slide')->where('publish',1)->orderBy('id','desc')->first();
        $take_item = Banner::where('position','=','slide')->count();
        if($bnfirst){
        $banners = Banner::where('position','=','slide')->where('publish',1)->orderBy('id','desc')->skip(1)->take($take_item-1)->get();
        }
        $banner_right = Banner::where('position','=','banner_right')->where('publish',1)->orderBy('id','desc')->take(3)->get();
        $vecongty = Post::where('slug','=','gioi-thieu-cosmetic')->first();
        $post_new = Post::where('publish','=','1')->orderBy('id','desc')->take(3)->get();


        $category = Category::where('parent_id',0)->where('publish',1)->get();

        $showProducts = [];
        foreach ($category as $cate) {
            $subCategory = Category::where('parent_id',$cate->id)->where('publish',1)->get();
            $cateIds = [$cate->id];
            if($subCategory) {
                foreach ($subCategory as $key => $value) {
                    $cateIds[] = $value->id;
                }
            }

            $products = Product::where('publish',1)->orderBy('id','desc')->whereHas('cate', function($product) use($cateIds) {
                $product->whereIn('categories.id', $cateIds);
            })->take(8)->get();
            
            $item = [
                'category' => $cate,
                'product' => $products
            ];
            $showProducts[] = $item;
        }
        return view('front-end.index',compact('bnfirst','banners','banner_right','category','vecongty','post_new', 'showProducts'));

    }


    //Tất cả sản phẩm
    public function tatcadanhmuc(){
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();
        $count_item_product = DisplayColumn::where('name','=','count_item_product')->value('value');
        
        $cate = Category::where('parent_id',0)->where('publish',1)->get();
        $category = Category::whereHas('product')->where('publish',1)->orderBy('id','desc')->get();
        $product = Product::where('publish',1)->orderBy('id','desc')->paginate($count_item_product);
        return view('front-end.pages.allproduct',['product'=>$product,'category'=>$category, 'cate'=>$cate]);
    }

    // public function danhmuc($slug){
    //     $this->getOption();
    //     $this->getMenuTop();
    //     $this->getMenuFooter();
    //     $count_item_product = DisplayColumn::where('name','=','count_item_product')->value('value');

    //     $setting = $this->gOption();

    //     $category = Category::whereSlug($slug)->first();

    //     if(!isset($category) ) {
    //         return view('front-end.layouts.404');
    //     }

    //     $showProducts = [];
    //         $subCategory = Category::where('parent_id',$category->id)->where('publish',1)->get();
    //         $cateIds = [$category->id];
    //         if($subCategory) {
    //             foreach ($subCategory as $key => $value) {
    //                 $cateIds[] = $value->id;
    //             }
    //         }

    //         $products = Product::where('publish',1)->orderBy('id','desc')->whereHas('cate', function($product) use($cateIds) {
    //             $product->whereIn('categories.id', $cateIds);
    //         })->paginate($count_item_product);
            
    //         $item = [
    //             'category' => $category,
    //             'product' => $products
    //         ];
    //         $showProducts[] = $item;
        
        
    //     $setting['seo_keyword'] = $category->seo_keyword ? $category->seo_keyword : $category->name;
    //     $setting['seo_description'] = $category->seo_description ? $category->seo_description : $setting['seo_description'];
    //     $setting['seo_title'] = $category->seo_title ? $category->seo_title : $category->name;
        
    //     $cate = Category::where('parent_id',0)->where('publish',1)->get();
    //     // $query=Product::whereHas('cate', function ($query) use ($slug) {
    //     //     $query->where('categories.slug','=',$slug);
    //     // });
    //     // $products = $query->paginate($count_item_product);
        
    //     return view('front-end.pages.category',compact('products','category','setting','cate', 'showProducts'));
    // }


    //Danh mục sản phẩms
    public function danhmuc($slug){
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();
        $count_item_product = DisplayColumn::where('name','=','count_item_product')->value('value');

        $setting = $this->gOption();

        $category = Category::whereSlug($slug)->first();

        if(!isset($category) ) {
            return view('front-end.layouts.404');
        }
        
        $setting['seo_keyword'] = $category->seo_keyword ? $category->seo_keyword : $category->name;
        $setting['seo_description'] = $category->seo_description ? $category->seo_description : $setting['seo_description'];
        $setting['seo_title'] = $category->seo_title ? $category->seo_title : $category->name;
        
        $cate = Category::where('parent_id',0)->where('publish',1)->get();

        if($category->parent_id != 0){
            $query=Product::whereHas('cate', function ($query) use ($slug) {
                $query->where('categories.slug','=',$slug);
            });
            $products = $query->paginate($count_item_product);
        }
        else{
            $subCategory = Category::where('parent_id',$category->id)->where('publish',1)->get();
            $cateIds = [$category->id];
            if($subCategory) {
                foreach ($subCategory as $key => $value) {
                    $cateIds[] = $value->id;
                }
            }
            $query = Product::where('publish',1)->orderBy('id','desc')->whereHas('cate', function($product) use($cateIds) {
                $product->whereIn('categories.id', $cateIds);
            });
            $products = $query->paginate($count_item_product);
        }
        
        return view('front-end.pages.category',compact('products','category','setting','cate'));
    }

    //Chi tiết sản phẩm
    public function sanpham($slug){
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();
        $this->getMenuDelivery();
        
        $setting = $this->gOption();
        $product = Product::whereSlug($slug)->first();
        if(!isset($product)){
            return view('front-end.layouts.404');
        }
        $setting['seo_keyword'] = $product->seo_keyword ? $product->seo_keyword : $product->name;
        $setting['seo_description'] = $product->seo_description ? $product->seo_description : $setting['seo_description'];
        $setting['seo_title'] = $product->seo_title ? $product->seo_title : $product->name;
        $cate_id = Product::whereSlug($slug)->value('category_id');
        $care = Product::where('category_id','=',$cate_id)->where('slug','<>',$slug)->where('publish',1)->take(8)->get();

        $cate = Category::where('parent_id',0)->where('publish',1)->get();
        $parent_cate = Category::where('id',$cate_id)->value('parent_id');
        if($parent_cate == 0){
            $best_cate = Category::where('id',$cate_id)->first();
        }else{
            $best_cate = Category::where('id',$parent_cate)->first();
        }

        return view('front-end.pages.detailproduct',compact('product','setting','care','best_cate','cate'));
    }

    //Trang tin tức
    public function tatcatintuc(){
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();
        $count_item_new = DisplayColumn::where('name','=','count_item_new')->value('value');
        $posts = Post::where('publish',1)->orderBy('created_at','desc')->paginate($count_item_new);
        $post_highlight = Post::where('publish',1)->where('status','=','Noi_bat')->orderBy('id','desc')->take(3)->get();
        $productnew = Product::where('publish',1)->orderBy('id','desc')->take(8)->get();
        return view('front-end.pages.allpost',['posts' => $posts,'post_highlight'=>$post_highlight,'productnew'=> $productnew]);
    }

    //Chuyên mục tin tức
    public function tintuc($slug){
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();
            $category = CatePost::whereSlug($slug)->first();
            if(!isset($category) ) {
                return view('front-end.layouts.404');
            }
            $setting = $this->gOption();
            $setting['seo_keyword'] = $category->seo_keyword ? $category->seo_keyword : $category->name;
            $setting['seo_description'] = $category->seo_description ? $category->seo_description : $setting['seo_description'];
            $setting['seo_title'] = $category->seo_title ? $category->seo_title : $category->name;

            $count_item_new = DisplayColumn::where('name','=','count_item_new')->value('value');
            $query=Post::where('publish',1)->whereHas('cate', function ($query) use ($slug) {
                    $query->where('cateposts.slug','=',$slug);
                });
                $posts = $query->paginate($count_item_new);

            $post_highlight = Post::where('publish',1)->where('status','=','Noi_bat')->orderBy('id','desc')->take(3)->get();
            $productnew = Product::where('publish',1)->orderBy('id','desc')->take(8)->get();
            return view('front-end.pages.new',['posts' => $posts,'post_highlight'=>$post_highlight,'productnew'=> $productnew,'category'=>$category,'setting'=>$setting]);
    }

    // Chi tiết của tin tức
    public function chitiettintuc($slug){
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();
        $setting = $this->gOption();
            $post = Post::whereSlug($slug)->first();
            if(!isset($post)){
                return view('front-end.layouts.404');
            }
            $setting['seo_keyword'] = $post->seo_keyword ? $post->seo_keyword : $post->name;
            $setting['seo_description'] = $post->seo_description ? $post->seo_description : $setting['seo_description'];
            $setting['seo_title'] = $post->seo_title ? $post->seo_title : $post->name;
            $cate_id = Post::whereSlug($slug)->value('category_id');
            
            $post_highlight = Post::where('publish',1)->where('status','=','Noi_bat')->orderBy('id','desc')->take(3)->get();
            $productnew = Product::where('publish',1)->orderBy('id','desc')->take(8)->get();
            return view('front-end.pages.new_detail',compact('post','projectnew','post_highlight','productnew'));
    }

    // Tìm kiếm sản phẩm
    public function timkiemSanpham(Request $request){
        $this->getOption();
        $this->getMenuTop();
        $this->getMenuFooter();

        $count_item_product = DisplayColumn::where('name','=','count_item_product')->value('value');
        $query = Product::where('publish',1);
        $keyword = isset($_GET['keyword']) ? $_GET['keyword'] : '';
        $filter_price = isset($_GET['filter_price']) ? $_GET['filter_price'] : '';
        if($keyword){
            $query->where('name','like','%'.$keyword.'%');
        }
        if($filter_price != 'all'){
            switch ($filter_price) {
                case 'duoi-500000':
                   $query->whereBetween('price_sale', [0, 499999]);
                   break;
                case 'tu-500000-den-1000000':
                    $query->whereBetween('price_sale', [500000,999999]);
                    break;
                case 'tu-1000000-den-1500000':
                    $query->whereBetween('price_sale', [1000000,1499999]);
                    break;
                case 'tu-1500000-den-2000000':
                    $query->whereBetween('price_sale', [1500000,1999999]);
                    break;
                case 'tu-2000000-den-2500000':
                    $query->whereBetween('price_sale', [2000000,2499999]);
                    break;
                case 'tu-2500000-den-3000000':
                    $query->whereBetween('price_sale', [2500000,2999999]);
                    break;
                case 'tren-3000000':
                    $query->where('price_sale','>',2999999);
                    break;
                default:
                    // chuỗi câu lệnh
                    break;
            }
        }
        $products = $query->paginate($count_item_product);
        $cate = Category::where('parent_id',0)->where('publish',1)->get();

        //$products = Product::where('name','like','%'.$keyword.'%')->paginate(16);
        return view('front-end.pages.searchresult',compact('products','keyword','cate'));
    }
    
    // Kiểm tra màu của sản phẩm
    public function kiemtraMausp(Request $request){
        $product_id = $request->product_id;
        $color_id = $request->color_id;
        $qty = Product_Color::where('product_id',$product_id)->where('color_id',$color_id)->value('quantity');
        if($qty>0){
            $str ='<div style="color:white"><button type="submit" class="btn-buy"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Mua hàng</button></div>';
        }else{
            $str ='<div class="btn-hotline btn btn-warning" style="border:none"> Sản phẩm đã hết hàng</div>';
        }
        exit(json_encode($str));
    }

}
