<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model
{
   	protected $table = 'products';
    use Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    public function cate(){
    	return $this->belongsTo('App\Category','category_id','id');	
    }
    public function color(){
        return $this->belongsToMany('App\Color','product_colors','product_id','color_id');
    }
    public function colorProduct(){
        return $this->hasMany('App\Product_Color','product_id','id');
    }
}
