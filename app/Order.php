<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    public function orderdetail(){
    	return $this->hasMany('\App\Order_Product','order_id');
    }

    public function product(){
    	return $this->belongsToMany('App\Product','order_details','order_id','product_id');
    }

    public function useroder(){
    	return $this->belongsTo('App\User','user_id','id');
    }
}
