<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role(){
        return $this->belongsTo('App\RoleUser','role_id','id');
    }

    public function is_Admin()
    {
        if($this->role_id == 1){
            return true;
        }
        return false;
    }

    public function is_User()
    {
        if($this->role_id == 3){
            return true;
        }
        return false;
    }
}
