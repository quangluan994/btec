<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DisplayColumn extends Model
{
    protected $table = 'display_columns';
}
