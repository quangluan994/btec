<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
class CatePost extends Model
{
    protected $table = 'cateposts';
    use Sluggable;
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
   
   public function new()
	{
		return $this->hasMany('App\Post','category_id','id');
	}
	public function parent_cate()
	{
		return $this->belongsTo('App\CatePost','parent_id','id');
	}
	public function sub_cate()
	{
		return $this->hasMany('App\CatePost','parent_id','id');
	}
}
