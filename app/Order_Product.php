<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_Product extends Model
{
    protected $table = 'order_details';

    public function product()
    {
    	return $this->belongsTo('App\Product','product_id','id');
    }

    public function order()
    {
    	return $this->belongsTo('App\Order','order_id','id');
    }

}
