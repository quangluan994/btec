<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Order;
use Mail;
use Carbon\Carbon;
use Session;

class SendReminderEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Order $order)
    {   
        // $to_email = env('MAIL_ADMIN_WEBSITE');
        // var_dump($to_email);die;
        $data = ['order' => $order];
        Mail::queue('mail.register', $data, function($message){
            $message->from(env('MAIL_USERNAME'),'Thông báo đơn hàng mới');
            $message->to(env('MAIL_ADMIN_WEBSITE'),'Admin Shop')->subject('Đơn hàng mới');
        });
    }
}
