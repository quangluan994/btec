<!DOCTYPE html>
<html>
<head>
    <title>Register</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/components.css')); ?>"/>
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/login.css')); ?>"/>
</head>
<body>
    <div class="container wow fadeInDown" data-wow-duration="0.1s" data-wow-delay="0.1s">
        <div class="row login_top_bottom">
            <div class="col-lg-10 push-lg-1 col-sm-10 push-sm-1">
                <div class="row">
                    <div class="col-lg-6 push-lg-3 col-sm-10 push-sm-1">
                        <div class="login_logo login_border_radius1">
                            <h3 class="text-center">
                                <img src="<?php echo e(asset('assets/img/logow.png')); ?>" alt="josh logo" class="admire_logo"><span class="text-white"> Gemi<br/>
                                Register</span>
                            </h3>
                        </div>
                        <div class="bg-white login_content login_border_radius">
                            <form class="form-horizontal login_validator m-b-20" id="register_valid"
                            action="<?php echo e(route('register')); ?>" method="post">

                            <?php echo e(csrf_field()); ?>


                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="username" class="col-form-label">Họ tên *</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"> <i class="fa fa-user text-primary"></i>
                                        </span>
                                        <input type="text" class="form-control" name="name" id="username" placeholder="Họ tên" value="<?php echo e(old('name')); ?>">
                                        
                                    </div>
                                    <?php if($errors->has('name')): ?>
                                    <span class="help-block text-danger">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row <?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                                <div class="col-sm-12">
                                    <label for="email" class="col-form-label">Email *</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope text-primary"></i>
                                        </span>
                                        <input type="text" placeholder="Email Address" value="<?php echo e(old('email')); ?>"  name="email" id="email" class="form-control"/>

                                    </div>
                                    <?php if($errors->has('email')): ?>
                                    <span class="help-block text-danger">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="password" class="col-form-label text-sm-right">Mật khẩu *</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-key text-primary"></i>
                                        </span>
                                        <input type="password" placeholder="Password"  id="password" name="password" class="form-control"/>

                                    </div>
                                    <?php if($errors->has('password')): ?>
                                    <span class="help-block text-danger">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label for="confirmpassword" class="col-form-label">Nhập lại mật khẩu *</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-key text-primary"></i>
                                        </span>
                                        <input type="password" placeholder="Confirm Password" name="password_confirmation" id="confirmpassword" class="form-control" />
                                    </div>
                                    <?php if($errors->has('password')): ?>
                                    <span class="help-block text-danger">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-9">
                                    <input type="submit" value="Đăng ký" class="btn btn-primary"/>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-9">
                                    <label class="col-form-label">Bạn đã có tài khoản?</label> <a href="<?php echo url('login'); ?>" class="text-primary login_hover"><b>Đăng nhập</b></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- global js -->
<script type="text/javascript" src="<?php echo e(asset('assets/js/jquery.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('assets/js/tether.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('assets/js/bootstrap.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('assets/js/pages/register.js')); ?>"></script>
<!-- end of page level js -->
</body>

</html>
