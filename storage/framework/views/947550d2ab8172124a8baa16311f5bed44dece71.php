<?php $__env->startSection('title'); ?>
Bảng điều khiển Admin
##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header_styles'); ?>
<!--Plugin styles-->
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/c3/css/c3.min.css')); ?>"/>
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/toastr/css/toastr.min.css')); ?>"/>
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/switchery/css/switchery.min.css')); ?>" />
<!--page level styles-->
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/new_dashboard.css')); ?>"/>
<!-- end of page level styles -->
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>

    <section class="content-header">
        <h1><i class="fa fa-th"></i> Bảng điều khiển</h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        </ol>
    </section>

    <section class="content">
        <?php if(Session::has('success')): ?>
            <div class="callout alert alert-success col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto" role="alert" >
                <button type="button" class="close">×</button> 
                <?php echo e(Session::get('success')); ?>

            </div>
        <?php endif; ?>
        <?php if(Session::has('danger')): ?>
            <div class="alert alert-danger"><?php echo e(Session::get('danger')); ?>

            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        
                    </div>
                    <div class="box-body">
                        <div class="card-block p-t-25">
                            <div class ="col-lg-12"> 
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box">
                                        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

                                        <div class="info-box-content">
                                          <span class="info-box-text">Option</span>
                                          <span class="info-box-number"><?php echo count($option); ?></span>
                                        </div>
                                        <!-- /.info-box-content -->
                                      </div>
                                      <!-- /.info-box -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box">
                                        <span class="info-box-icon bg-red"><i class="fa fa-book"></i></span>

                                        <div class="info-box-content">
                                          <span class="info-box-text">Danh mục</span>
                                          <span class="info-box-number"><?php echo count($cate); ?></span>
                                        </div>
                                        <!-- /.info-box-content -->
                                      </div>
                                      <!-- /.info-box -->
                                    </div>
                                    <!-- /.col -->

                                    <!-- fix for small devices only -->
                                    <div class="clearfix visible-sm-block"></div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box">
                                        <span class="info-box-icon bg-green"><i class="fa fa-product-hunt"></i></span>

                                        <div class="info-box-content">
                                          <span class="info-box-text">Sản phẩm</span>
                                          <span class="info-box-number"><?php echo count($product); ?></span>
                                        </div>
                                        <!-- /.info-box-content -->
                                      </div>
                                      <!-- /.info-box -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box">
                                        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                                        <div class="info-box-content">
                                          <span class="info-box-text">Người dùng</span>
                                          <span class="info-box-number"><?php echo count($user); ?></span>
                                        </div>
                                        <!-- /.info-box-content -->
                                      </div>
                                      <!-- /.info-box -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box">
                                        <span class="info-box-icon bg-aqua"><i class="fa fa-bars"></i></span>

                                        <div class="info-box-content">
                                          <span class="info-box-text">Chuyên mục</span>
                                          <span class="info-box-number"><?php echo count($catepost); ?></span>
                                        </div>
                                        <!-- /.info-box-content -->
                                      </div>
                                      <!-- /.info-box -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box">
                                        <span class="info-box-icon bg-red"><i class="fa fa-newspaper-o" aria-hidden="true"></i></span>

                                        <div class="info-box-content">
                                          <span class="info-box-text">Tin tức</span>
                                          <span class="info-box-number"><?php echo count($post); ?></span>
                                        </div>
                                        <!-- /.info-box-content -->
                                      </div>
                                      <!-- /.info-box -->
                                    </div>
                                    <!-- /.col -->

                                    <!-- fix for small devices only -->
                                    <div class="clearfix visible-sm-block"></div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box">
                                        <span class="info-box-icon bg-green"><i class="fa fa-truck" aria-hidden="true"></i></span>

                                        <div class="info-box-content">
                                          <span class="info-box-text">Đơn hàng</span>
                                          <span class="info-box-number"><?php echo count($order); ?></span>
                                        </div>
                                        <!-- /.info-box-content -->
                                      </div>
                                      <!-- /.info-box -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box">
                                        <span class="info-box-icon bg-yellow"><i class="fa fa-comments" aria-hidden="true"></i></span>

                                        <div class="info-box-content">
                                          <span class="info-box-text">Phản hồi</span>
                                          <span class="info-box-number"><?php echo count($contact); ?></span>
                                        </div>
                                        <!-- /.info-box-content -->
                                      </div>
                                      <!-- /.info-box -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_scripts'); ?>
<!--  plugin scripts -->

<!-- end page level scripts -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>