<?php $__env->startSection('title'); ?>
Sửa màu sản phẩm
##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header_styles'); ?>
<!-- plugin styles-->
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')); ?>"/>
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')); ?>"/>
<!--end of page level css-->
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>


<section class="content-header">
        <h1>Sửa màu</h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="<?php echo e(url('admin/mau-sac')); ?>">Màu sắc</a></li>
          <li class="active">Chỉnh sửa</li>
        </ol>
    </section>
    <section class="content">
        <?php if($errors->any()): ?>
        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
            <button type="button" class="close">×</button>
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
        <?php endif; ?>

        <?php if(Session::has('danger')): ?>
            <div class="alert alert-danger"><?php echo e(Session::get('danger')); ?>

            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      
                        <div class="col-xs-6"><h4 style="font-weight: bolder">Sửa màu săc:<?php echo e($color->display_name); ?></h4></div>
                        <div class="col-xs-6" ><a href="<?php echo e(url("admin/mau-sac")); ?>"  class="btn btn-info" title="" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i> Quay lại</a></div>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal login_validator" id="tryitForm" action="<?php echo url("admin/mau-sac"); ?>/<?php echo $color->id; ?>/update" method="post">
                            <div class="col-12">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label for="display_name" class="col-lg-3 control-label">Tên màu *</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="display_name" id="display_name" class="form-control" value="<?php echo $color->display_name; ?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="color" class="col-lg-3 control-label">Mã màu*</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                                            <div class="input-group-btn" style="width:50px">
                                            <input type="color" name="value_color" id="value_color" class="form-control" value="<?php echo $color->color; ?>" style="padding:0">
                                            </div>
                                            <input type="text" name="color" id="color" class="form-control" value="<?php echo $color->color; ?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-lg-9 push-lg-3">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Cập nhật</button>
                                        <button class="btn btn-warning" type="reset" id="clear"><i class="fa fa-refresh"></i> Nhập lại</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script src="<?php echo e(asset('assets/fancybox/source/jquery.fancybox.js')); ?>"></script>
<script>
    $("#value_color").change(function() {
        var color = $("#value_color").val();
        $("#color").val(color);
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>