<?php $__env->startSection('title'); ?>
    Danh sách menu
    ##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header_styles'); ?>
    <!--Plugin styles-->
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/select2/css/select2.min.css')); ?>"/>
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/dataTables.bootstrap.css')); ?>"/>
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/tables.css')); ?>"/>
    <link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/tablesid.css')); ?>"/>

    <!-- end of page level styles -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    


    <section class="content-header">
        <h1><i class="fa fa-th"></i> Menu</h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="<?php echo e(url('admin/menu')); ?>">Menu</a></li>
          <li class="active">Danh sách</li>
        </ol>
    </section>

    <section class="content">
        <?php echo Menu::render(); ?>

    </section>

    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_scripts'); ?>
    <?php echo Menu::scripts(); ?>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>