<?php $__env->startSection('head'); ?>

    <meta name="keywords" content="<?php echo e($setting['seo_keyword']); ?>" />

    <meta name="description" content="<?php echo e($setting['seo_description']); ?>" />

    <link rel="SHORTCUT ICON" href="<?php echo $setting['icon_website']; ?>">

    <title>Mỹ phẩm</title>



    <meta property="og:title" content="Mỹ phẩm">

    <meta property="og:description" content="<?php echo e($setting['seo_description']); ?>">

    <meta property="og:type" content="website"/>

    <meta property="og:url" content="<?php echo URL::current(); ?>">

    <meta property="og:site_name" content="<?php echo e($setting['company_name']); ?>">

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

    <div class="body-page">

            <h1 class="hidden"><?php echo e($setting['seo_title']); ?></h1>

            <div class="container">

                <div class="home-item">

                    <div class="row">

                        <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 padding-none-right">

                            <div class="owl-carousel owl-carousel-1 owl-theme">

                                <div class="item active">
                                    <?php if($bnfirst): ?>
                                    <a href="<?php echo $bnfirst->link; ?>"><img src="<?php echo $bnfirst->image; ?>" alt="<?php echo $bnfirst->name; ?>"></a>
                                    <?php endif; ?>

                                </div>
                                <?php if($banners): ?>
                                <?php $__currentLoopData = $banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <div class="item">

                                    <a href="<?php echo $item->link; ?>"><img src="<?php echo $item->image; ?>" alt="<?php echo $item->name; ?>"></a>

                                </div>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                            </div>

                        </div>

                        <div class="hidden-xs hidden-sm col-md-2 col-lg-2">

                            <?php $__currentLoopData = $banner_right; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <div class="item-row">

                                <a href="<?php echo $item->link; ?>"><img src="<?php echo $item->image; ?>" alt="<?php echo $item->name; ?>"></a>

                            </div>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>

                    </div>

                </div>

                <!-- GROUP 1 -->

                <?php if($showProducts): ?>

                <?php $__currentLoopData = $showProducts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <div class="home-item">

                    <div class="item-wrap">

                        <div class="category-box">

                            <div class="category-title">

                                <h2><a href="/danh-muc/<?php echo e($item['category']->slug); ?>.html"><?php echo e($item['category']->name); ?></a></h2>

                                <div class="view-more pull-right">

                                    

                                </div>

                            </div>

                            <div class="category-content">

                                <div class="row">

                                    <div class="hidden-xs hidden-sm col-md-3 col-lg-3 left-content">

                                        <div class="banner-top">

                                            <a href="/danh-muc/<?php echo e($item['category']->slug); ?>.html"><img src="<?php echo e($item['category']->image); ?>" alt="<?php echo e($item['category']->name); ?>"></a>

                                        </div>

                                        

                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 right-content">

                                        <div class="row">

                                            <?php $__currentLoopData = $item['product']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $it): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 product-item">

                                                <div class="product-box">

                                                    <div class="product-thumb">

                                                        <a href="/san-pham/<?php echo e($it->slug); ?>.html"><img src="<?php echo e($it->thumbnail); ?>" alt="<?php echo e($it->name); ?>"></a>

                                                        <?php if($it->price != $it->price_sale): ?>

                                                        <div class="sale">-<?php echo e(round(((1-(($it->price_sale)/($it->price)))*100),0)); ?> %</div>

                                                        <?php endif; ?>

                                                    </div>

                                                    <div class="product-name"><a href="/san-pham/<?php echo e($it->slug); ?>.html">

                                                        <?php echo e(subtext(($it->name),100)); ?></a>

                                                    </div>

                                                    <div class="product-price">

                                                        <?php if($it->qty_in_stock > 0): ?>

                                                            <?php if($it->price != $it->price_sale): ?>

                                                            <div class="price-old"><?php echo e(number_format($it->price)); ?> đ</div>

                                                            <?php endif; ?>

                                                            <div class="price-new"><?php echo e(number_format($it->price_sale)); ?> đ</div>

                                                        <?php else: ?>

                                                            <div class="price-new">Hết hàng</div>

                                                        <?php endif; ?>

                                                    </div>

                                                </div>

                                            </div>

                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                <?php endif; ?>

                <!-- GROUP 1 end -->

                

                <!-- new  -->

                <div class="news-wrap">

                    <div class="row">

                        <div class="hidden-xs hidden-sm col-md-7 col-lg-7">

                            <div class="about-wrap">

                                <div class="about-title">

                                    <h1><?php echo e($vecongty->name); ?></h1>

                                </div>

                                <div class="content">

                                    <div class="thumb">

                                        <img src="<?php echo e($vecongty->image); ?>" alt="<?php echo e($vecongty->name); ?>">

                                    </div>

                                    <div class="content-text">

                                        <?php echo $vecongty->content; ?>


                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 blog-wrap">

                            <div class="blog-title">

                                <h4>TIN TỨC</h4>

                            </div>

                            <?php $__currentLoopData = $post_new; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <div class="blog-item">

                                <div class="row">

                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 blog-thumb">

                                        <a href="/tin-tuc/<?php echo e($item->slug); ?>.html"><img src="<?php echo e($item->image); ?>" alt="<?php echo e($item->name); ?>"></a>

                                    </div>

                                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 blog-content">

                                        <h5 class="blog-name">

                                            <a href="/tin-tuc/<?php echo e($item->slug); ?>.html"><?php echo e($item->name); ?></a>

                                        </h5>

                                        <p class="blog-desc">

                                            <?php echo subtext(strip_tags($item->description),120); ?>


                                        </p>

                                        <a class="more-view" href="/tin-tuc/<?php echo e($item->slug); ?>.html">Đọc thêm <i class="fa fa-angle-right" aria-hidden="true"></i></a>

                                    </div>

                                </div>

                            </div>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                        </div>

                    </div>

                </div>

                <!-- new end -->

            </div>

        </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('front-end.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>