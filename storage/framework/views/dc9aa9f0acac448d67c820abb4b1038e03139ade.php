<?php $__env->startSection('title'); ?>
404
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>

    <!-- project -->
    
        <main class="error-page">
            <div class="page-404">
                <img src="images/404.png" alt="404">
                <h2>Lỗi!, Nội dung không tìm thấy</h2>
                <a href="#">Trở về trang chủ</a>
            </div>
        </main>
    <!-- /.project -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front-end.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>