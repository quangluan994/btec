<?php $__env->startSection('title'); ?>
Mã giảm giá
##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header_styles'); ?>
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/tablesid.css')); ?>"/>
<!--End of page level styles-->
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <h1><i class="fa fa-th"></i> Mã giảm giá</h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="<?php echo e(url('admin/ma-giam-gia')); ?>">Mã giảm giá</a></li>
          <li class="active">Danh sách</li>
        </ol>
    </section>

    <section class="content">
        <?php if(Session::has('success')): ?>
            <div class="callout alert alert-success col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto" role="alert" >
                <button type="button" class="close">×</button> 
                <?php echo e(Session::get('success')); ?>

            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border text-right">
                        <a href="<?php echo e(url("admin/ma-giam-gia/create")); ?>"  class="btn btn-primary" title=""><i class="fa fa-plus"></i> Thêm</a>

                        <div class=" form-group fix-float">
                            <select class="styled hasCustomSelect form-control" style="width:150px; float:left">
                                <option>Chọn tác vụ</option>
                                <option value="remove">Xóa</option>
                            </select>
                            <a href="javascript:action();" class="btn btn-info">Apply</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="card-block p-t-25">
                            <div class ="row col-lg-12"> 
                                <table class="respontables">
                                    <tr>
                                        <th style="width: 1%"> <input type="checkbox" class="check-all"></th>
                                        <th class="text-center">Tên</th>
                                        <th class="text-center">Mã</th>
                                        <th class="text-center">Loại mã</th>
                                        <th class="text-center">Giá trị(%)</th>
                                        <th class="text-center">Số lượng</th>
                                        <th class="text-center">Ngày bắt đầu</th>
                                        <th class="text-center">Ngày kết thúc</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                    <?php if($discount_codes->count()>0): ?>
                                        <?php $__currentLoopData = $discount_codes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><input type="checkbox" class="checkbox" name="checkbox_id" value="<?php echo e($val->id); ?>" id="select-<?php echo e($val->id); ?>"></td>
                                            <td class="text-center"><?php echo $val->name; ?></td>
                                            <td class="text-center"><?php echo $val->code_value; ?></td>
                                            <td class="text-center">
                                                <?php if($val->type == 'So_luong'): ?>
                                                Giới hạn số lượng
                                                <?php else: ?>
                                                Giảm giá theo ngày
                                                <?php endif; ?>
                                            </td>
                                            <td class="text-center"><?php echo $val->discount; ?></td>
                                            <td class="text-center">
                                                <?php if($val->type == 'So_luong'): ?>
                                                <?php echo $val->number_code; ?>

                                                <?php else: ?>
                                                Không giới hạn
                                                <?php endif; ?>
                                            </td>
                                            <td class="text-center"><?php echo $val->from_date; ?></td>
                                            <td class="text-center"><?php echo $val->to_date; ?></td>
                                            <td class="text-center">
                                                <a href="/admin/ma-giam-gia/<?php echo $val->id; ?>/edit" class="btn btn-primary" title=""><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-danger delitem" id="del_<?php echo e($val->id); ?>"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                    <tr><td colspan="9"><h4>Không có dữ liệu để hiển thị</h4></td></tr>  
                                    <?php endif; ?>      
                                </table>       
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo e(csrf_field()); ?>

    </section>
    <script>
        $(".delitem").click(function(){
            var id = $(this).attr("id");
            var splitid = id.split("_");
            var deleteid = splitid[1];
            console.log(deleteid);
            swal({
              title: "Bạn chắc chắn muốn xóa?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Ok! Xóa ngay",
              closeOnConfirm: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    

                    var token = $("input[name='_token']").val();
                    $.ajax({
                        type: 'get',
                        url: '/admin/ma-giam-gia/'+deleteid+'/delete',
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token', token);
                        },
                        success: function(response) {
                            var res = $.parseJSON(response);
                            console.log(res);
                            if(!res.status) {
                                return;
                            }
                            location.reload(true);
                            swal({
                                title: "Thành công!",
                                timer: 2000,
                                type: "success"
                            });
                        }
                    });
                }
            });
        });
    </script>
    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>