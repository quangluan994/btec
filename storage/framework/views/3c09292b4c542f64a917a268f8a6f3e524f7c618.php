<?php $__env->startSection('title'); ?>
Chỉnh sửa mã giảm giá
##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header_styles'); ?>
<!-- plugin styles-->
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')); ?>"/>
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')); ?>"/>
<!--end of page level css-->
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>


<section class="content-header">
        <h1>Chỉnh sửa bài viết</h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="<?php echo e(url('admin/ma-giam-gia')); ?>">Mã giảm giá</a></li>
          <li class="active">Chỉnh sửa</li>
        </ol>
    </section>
    <section class="content">
        <?php if($errors->any()): ?>
        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
            <button type="button" class="close">×</button>
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      
                        <div class="col-xs-6"><h4 style="font-weight: bolder">Chỉnh sửa mã giảm giá</h4></div>
                        <div class="col-xs-6" ><a href="<?php echo e(url("admin/ma-giam-gia")); ?>"  class="btn btn-info" title="" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i> Quay lại</a></div>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal login_validator" id="tryitForm" action="<?php echo url("admin/ma-giam-gia"); ?>/<?php echo $discount_code->id; ?>/update" method="post">
                            <div class="col-12">
                                <?php echo e(csrf_field()); ?>


                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Tên(mô tả)</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="name" id="name" value="<?php echo e($discount_code->name); ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="code_value" class="col-lg-3 control-label">Mã*</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="btn btn-primary input-group-addon" onclick="getRandomcode()"> <i class="fa fa-random" aria-hidden="true"></i> Random</span>
                                            <input type="text" name="code_value" id="code_value" class="form-control" value="<?php echo e($discount_code->code_value); ?>" readonly>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="type" class="col-lg-3 control-label">Loại mã</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <div class="radio">
                                                <label>
                                                  <input type="radio" name="type" id="optionsRadios1" onclick="changTypeNb()" value="So_luong" <?php echo ($discount_code->type == 'So_luong') ? 'checked' : '' ?>> Giới hạn số lượng
                                                </label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <label>
                                                  <input type="radio" name="type" id="optionsRadios2" onclick="changTypeDate()" value="Theo_ngay" <?php echo ($discount_code->type == 'Theo_ngay') ? 'checked' : '' ?>> Giảm giá theo ngày
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="discount" class="col-lg-3 control-label">Giá trị(%)</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="number" name="discount" id="discount" class="form-control" min="0" max="100" value="<?php echo e($discount_code->discount); ?>" required>
                                        </div>
                                    </div>
                                </div>
                                
                                <?php if($discount_code->type == 'So_luong'): ?>
                                <div class="form-group" id="number_type">
                                    <label for="number_code" class="col-lg-3 control-label">Số lượng</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="number" name="number_code" id="number_code" class="form-control" min="0" value="<?php echo e($discount_code->number_code); ?>" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="form_date_type" style="display:none">
                                    <label for="from_date" class="col-lg-3 control-label">Từ ngày</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <input type="date" name="from_date" id="from_date" class="form-control" value="<?php echo e($discount_code->from_date); ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="to_date_type" style="display:none">
                                    <label for="to_date" class="col-lg-3 control-label">Đến ngày</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <input type="date" name="to_date" id="to_date" class="form-control" value="<?php echo e($discount_code->to_date); ?>">
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>

                                <?php if($discount_code->type == 'Theo_ngay'): ?>
                                <div class="form-group" id="number_type" style="display:none">
                                    <label for="number_code" class="col-lg-3 control-label">Số lượng</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="number" name="number_code" id="number_code" class="form-control" min="0" value="<?php echo e($discount_code->number_code); ?>" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="form_date_type">
                                    <label for="from_date" class="col-lg-3 control-label">Từ ngày</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <input type="date" name="from_date" id="from_date" class="form-control" value="<?php echo e($discount_code->from_date); ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="to_date_type">
                                    <label for="to_date" class="col-lg-3 control-label">Đến ngày</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-calendar" aria-hidden="true"></i></span>
                                            <input type="date" name="to_date" id="to_date" class="form-control" value="<?php echo e($discount_code->to_date); ?>">
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-lg-9 push-lg-3">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Cập nhật</button>
                                        <button class="btn btn-warning" type="reset" id="clear"><i class="fa fa-refresh"></i> Nhập lại</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script src="<?php echo e(asset('assets/fancybox/source/jquery.fancybox.js')); ?>"></script>
<script>
    function changTypeNb(){
        $('#number_type').show();
        $('#form_date_type').hide();
        $('#to_date_type').hide();
    }
    function changTypeDate(){
        $('#number_type').hide();
        $('#form_date_type').show();
        $('#to_date_type').show();
    }    
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>