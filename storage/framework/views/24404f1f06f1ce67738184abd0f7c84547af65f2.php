<?php $__env->startSection('title'); ?>
Danh sách tin tức
##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header_styles'); ?>
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/tablesid.css')); ?>"/>
<!--End of page level styles-->
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <h1><i class="fa fa-th"></i> Danh sách Tin tức</h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="<?php echo e(url('admin/bai-viet')); ?>">Tin tức</a></li>
          <li class="active">Danh sách</li>
        </ol>
    </section>

    <section class="content">
        <?php if(Session::has('success')): ?>
            <div class="callout alert alert-success col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto" role="alert" >
                <button type="button" class="close">×</button> 
                <?php echo e(Session::get('success')); ?>

            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border text-right">
                        <div class="col-lg-2">
                            <a href="<?php echo e(url("admin/bai-viet/create")); ?>"  class="btn btn-primary" title=""><i class="fa fa-plus"></i> Thêm</a>
                        </div>
                        <div class="col-lg-7">
                            <form action="bai-viet" method="get" accept-charset="utf-8">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <input type="text" name="name" value="<?php echo e($name); ?>" placeholder="Bài viết" class="form-group form-control">
                                    </div>&nbsp; &nbsp;
                                    <div class="col-lg-3">
                                       <select name="publish" class="form-group form-control col-lg-2">
                                           <option value="all" <?php if($publish == 'all') echo "selected"; ?>>Trạng thái</option>
                                           <option value="1" <?php if($publish == '1') echo "selected"; ?>>Enable</option>
                                           <option value="0" <?php if($publish == '0') echo "selected"; ?>>Disable</option>
                                       </select>
                                    </div>
                                   &nbsp; &nbsp;
                                    <div class="col-lg-3">
                                        <input type="submit" name="" value="Tìm kiếm" class=" form-group form-control btn btn-success">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class=" form-group fix-float">
                            <select class="styled hasCustomSelect form-control" style="width:150px; float:left">
                                <option>Chọn tác vụ</option>
                                <option value="active">Kích hoạt/Vô hiệu hóa</option>
                                <option value="remove">Xóa</option>
                            </select>
                            <a href="javascript:action();" class="btn btn-info">Apply</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="card-block p-t-25">
                            <div class ="row col-lg-12"> 
                                <table class="respontables">
                                    <tr>
                                        <th style="width: 1%"> <input type="checkbox" class="check-all"></th>
                                        <th class="text-center">Tên bài viết</th>
                                        <th class="text-center" style="width:180px;">Hình ảnh</th>
                                        <th data-th="Driver details" class="text-center"><span>Chuyên mục</span></th>
                                        <th class="text-center">Trạng thái</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                    <?php if($posts->count()>0): ?>
                                        <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><input type="checkbox" class="checkbox" name="checkbox_id" value="<?php echo e($val->id); ?>" id="select-<?php echo e($val->id); ?>"></td>
                                            <td class="text-center"><?php echo $val->name; ?></td>
                                            <td class="text-center" style="width:180px"><img src="<?php echo $val->image; ?>" alt="" width="150" height="80"></td>
                                            <td class="text-center">
                                                <?php if(($val->category_id != 0) && ($val->category_id !=NULL)): ?>
                                                <?php echo $val->cate->name; ?>

                                                <?php else: ?>
                                                <?php endif; ?>
                                            </td>
                                            
                                            <td class="text-center">
                                                <?php if($val->publish == 1): ?>
                                                <i class="fa fa-check text-success"></i>
                                                <?php else: ?>
                                                <i class="fa fa-times text-danger"></i>
                                                <?php endif; ?>
                                            </td>
                                            <td class="text-center">
                                                <a href="/admin/bai-viet/<?php echo $val->id; ?>/edit" class="btn btn-primary" title=""><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-danger delitem" id="del_<?php echo e($val->id); ?>"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                    <tr><td colspan="6"><h4>Không có dữ liệu để hiển thị</h4></td></tr>  
                                    <?php endif; ?>      
                                </table>       
                            </div>
                            <div style="clear:both"></div>
                            <div class="row col-lg-4 col-md-6 col-sm-6" style="float:right">
                               <?php echo e($posts->links()); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo e(csrf_field()); ?>

    </section>
    
    <script>
        $(".delitem").click(function(){
            var id = $(this).attr("id");
            var splitid = id.split("_");
            var deleteid = splitid[1];
            console.log(deleteid);
            swal({
              title: "Bạn chắc chắn muốn xóa?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Ok! Xóa ngay",
              closeOnConfirm: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    

                    var token = $("input[name='_token']").val();
                    $.ajax({
                        type: 'get',
                        url: '/admin/bai-viet/'+deleteid+'/delete',
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token', token);
                        },
                        success: function(response) {
                            var res = $.parseJSON(response);
                            console.log(res);
                            if(!res.status) {
                                return;
                            }
                            location.reload(true);
                            swal({
                                title: "Thành công!",
                                timer: 2000,
                                type: "success"
                            });
                        }
                    });
                }
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>