<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php echo $__env->yieldContent('head'); ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset('front-end/node_modules/font-awesome/css/font-awesome.min.css')); ?>">
    <!-- Link css -->
    <link rel="stylesheet" href="<?php echo e(asset('front-end/node_modules/bootstrap/dist/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('front-end/node_modules/owl.carousel/dist/assets/owl.carousel.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('front-end/node_modules/owl.carousel/dist/assets/owl.theme.default.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('front-end/css/style.css')); ?>">
    <link rel='stylesheet' href='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.css'>
    <script src='https://cdn.rawgit.com/t4t5/sweetalert/v0.2.0/lib/sweet-alert.min.js'></script>
    <?php echo $setting['add_code_header']; ?>


    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "WebSite",
      "name" : "<?php echo e($setting['company_name']); ?>",
      "url": "<?php echo e(url('/')); ?>",
      "potentialAction": [{
        "@type": "SearchAction",
        "target": "<?php echo e(url('/tim-kiem-san-pham?keyword={search_term_string}')); ?>",
        "query-input": "required name=search_term_string"
      }]
    }
    </script>

    <!-- Organization -->
    <script type="application/ld+json">
    { "@context" : "http://schema.org",
      "@type" : "Organization",
      "legalName" : "<?php echo e($setting['company_name']); ?>",
      "url" : "<?php echo e(url('/')); ?>",
      "contactPoint" : [{
        "@type" : "ContactPoint",
        "telephone" : "<?php echo e($setting['hot_line']); ?>",
        "contactType" : "customer service"
      }],
      "logo" : "<?php echo e($setting['logo']); ?>"
    }
    </script>
</head>

<body>
    <div id="main-page">
        <!-- header -->
        <header>
            <!-- header top -->
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6 hidden-xs">
                            <div class="contact-top">
                                <p><?php echo e($setting['header_service']); ?></p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-right fix-center">
                            <ul class="user-acction">
                                <?php if(Auth::guest()): ?>
                                    <li><a href="<?php echo e(route('register')); ?>">Đăng ký</a></li>
                                    <li><a href="<?php echo e(route('login')); ?>">Đăng nhập</a></li>
                                <?php else: ?>
                                    <li class="dropdown">
                                        <a href="<?php echo e(url('profile/'.Auth::user()->id)); ?>">
                                          <i class="fa fa-user-circle" aria-hidden="true"></i> <?php echo e(Auth::user()->name); ?>

                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo e(route('logout')); ?>"
                                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();"> Đăng xuất
                                        </a>

                                        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                            <?php echo e(csrf_field()); ?>

                                        </form>
                                    </li>

                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <!-- header top end-->
            <!-- header body -->
            <div class="header-body">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-5 col-md-3 col-lg-3">
                            <div class="logo"><a href="/"><img src="<?php echo e($setting['logo']); ?>" alt="Logo"></a></div>
                        </div>
                        <div class="col-sm-7 col-md-5 col-lg-5 hidden-xs">
                            <div class="contact-wrap">
                                <div class="contact-phone pull-left">
                                    <span class="contact-icon">
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                    </span>
                                    <div class="contact-content">
                                        <span class="name">Hotline</span>
                                        <a href="#" class="desc"><?php echo e($setting['hot_line']); ?></a>
                                    </div>
                                </div>
                                <div class="contact-adress pull-left">
                                    <span class="contact-icon">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    </span>
                                    <div class="contact-content">
                                        <span class="name"><?php echo e($setting['company_province']); ?></span>
                                        <a href="#" class="desc"><?php echo e($setting['company_address']); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                            <div class="input-group form-header">
                                <input type="text" name="keyword" class="form-control" placeholder="Từ khóa cần tìm" >
                                <span class="input-group-btn">
                                    <button class="btn btn-default" onclick="searchrs()"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- header body end -->
            <!-- nav -->
            <div id="nav" data-spy="affix" data-offset-top="197">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-9 col-sm-9 col-md-11 col-lg-11">
                            <!-- nav desktop -->
                            <nav class="nav-desktop navbar navbar-default hidden-xs" role="navigation">
                                <ul class="nav navbar-nav">
                                    <?php $__currentLoopData = $top_menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li>
                                        <a href="<?php echo e($item->link); ?>">
                                            <?php echo e($item->label); ?>

                                            <?php if($item->getsons($item->id)->count()>0): ?>
                                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            <?php endif; ?>
                                        </a>
                                        <?php if($item->getsons($item->id)->count()>0): ?>
                                        <ul class="sub-menu">
                                            <?php $__currentLoopData = $item->getsons($item->id); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li><a href="<?php echo e($sub->link); ?>"><?php echo e($sub->label); ?></a></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                        <?php endif; ?>
                                    </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </nav>
                            <!-- nav desktop end-->
                            <!-- nav mobile -->
                            <nav class=" nav-mobile navbar navbar-default hidden-sm hidden-md hidden-lg" role="navigation">
                                <div class="navbar-header">
                                    <a href="#" class="navbar-btn pull-left" data-toggle="collapse" data-target=".mobile-collapse">
                                        <i class="fa fa-bars" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <div class="collapse mobile-collapse">
                                    <ul class="nav navbar-nav side-nav">
                                        <?php $__currentLoopData = $top_menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li>
                                            <a href="<?php echo e($item->link); ?>">
                                                <?php echo e($item->label); ?>

                                            </a>
                                            
                                            <?php if($item->getsons($item->id)->count()>0): ?>
                                            <button class="accordion"></button>
                                            <ul class="sub-menu">
                                                <?php $__currentLoopData = $item->getsons($item->id); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <li><a href="<?php echo e($sub->link); ?>"><?php echo e($sub->label); ?></a></li>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </ul>
                                            <?php endif; ?>
                                        </li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            </nav>
                            <!-- nav mobile -->
                        </div>
                        <div class="col-xs-3 col-sm-2 col-md-1 col-lg-1">
                            <div class="cart-wrap">
                                <a class="cart-icon" href="#">
                                    <span class="cart-count"><?php echo e(Cart::countRows()); ?></span>
                                    <img src="<?php echo e(asset('front-end/images/cart.png')); ?>" alt="">
                                </a>
                                <div class="" id="cart-box">
                                    <div class="box-head">
                                        <?php if(Cart::countRows() == 0): ?>
                                            <span class="cart-counter">Giỏ hàng của bạn trống</span>
                                        <?php else: ?>
                                        <i class="fa fa-check" aria-hidden="true"></i>
                                        <span class="cart-counter"><?php echo e(Cart::count()); ?></span>
                                        Sản phẩm trong giỏ hàng
                                        <?php endif; ?>
                                    </div>
                                    <?php if(Cart::countRows() > 0): ?>
                                    <div class="box-body">
                                        <ul class="list-cart">
                                            <?php $__currentLoopData = Cart::content(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li class="item-cart clearfix">
                                                <div class="product-thumb">
                                                    <img src="<?php echo e($item->options->img); ?>" alt="<?php echo e($item->name); ?>">
                                                </div>
                                                <div class="product-desc">
                                                    <a href="#" class="product-name"><?php echo e($item->name); ?></a>
                                                    <a href="<?php echo url("cart/$item->rowId/delete"); ?>" class="btn-delete">
                                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                    </a>
                                                    
                                                </div>
                                                <div class="product-action">
                                                    <div class="product-price">
                                                        <span><?php echo e(number_format($item->price)); ?> đ</span> x <span class="cart-count"><?php echo e($item->qty); ?></span> = <span><?php echo e(number_format($item->subtotal)); ?> đ</span>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </ul>
                                    </div>
                                    <div class="box-footer">
                                        <div class="total-price">
                                            Tổng cộng:
                                            <span> <?php echo e(Cart::total(0)); ?> đ</span>
                                        </div>
                                        <div class="total-btn">
                                            <a href="<?php echo e(url('cart')); ?>"><button>Tiến hành đặt hàng</button></a>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- nav end -->
            <?php if(Session::has('success')): ?>
                <div class="callout alert alert-success col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto; background-color:#00b300 ; color:white" role="alert" >
                    <button type="button" class="close">×</button> 
                    <?php echo e(Session::get('success')); ?>

                </div>
            <?php endif; ?>

            <?php if($errors->any()): ?>
            <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto; background-color:red ; color:white" role="alert">
                <button type="button" class="close">×</button>
                <ul>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
            </div>
            <?php endif; ?>
        </header>
        <!-- end header -->