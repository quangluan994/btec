<ul class="sidebar-menu" data-widget="tree">
	<li>
        <a href="<?php echo e(url('/')); ?> " target="blank">
            <i class="fa fa-globe"></i>
            <span class="link-title">&nbsp;Website</span>
        </a>
    </li>
	<li class="<?php echo e(Request::is('admin/dashboard') ? 'active' : ''); ?>">
		<a href="<?php echo e(url('admin/dashboard')); ?>">
			<i class="fa fa-dashboard"></i> <span>Dashboard</span>
		</a>
	</li>
	<li>
		<a href="<?php echo url('admin/media'); ?> ">
            <i class="fa fa-picture-o"></i>
            <span class="link-title">&nbsp;Thư viện ảnh</span>
        </a>
	</li>

	<li>
		<a href="<?php echo url('admin/order'); ?> ">
			<i class="fa fa-truck"></i>
			<span class="link-title">&nbsp;Đơn hàng</span>
		</a>
	</li>

	<li class="treeview <?php echo e((Request::is('admin/danh-muc/create') || Request::is('admin/danh-muc/edit') || Request::is('admin/danh-muc') ? 'active' : '')); ?>">
		<a href="#">
			<i class="fa fa-book"></i>
			<span>Danh mục</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo url("admin/danh-muc/create"); ?>"><i class="fa fa-plus"></i>Thêm</a></li>
			<li><a href="<?php echo url("admin/danh-muc"); ?>"><i class="fa fa-list"></i>Danh sách</a></li>
		</ul>
	</li>
	<li class="treeview <?php echo e((Request::is('admin/san-pham/create') || Request::is('admin/san-pham/edit') || Request::is('admin/san-pham') ? 'active' : '')); ?>">
		<a href="#">
			<i class="fa fa-product-hunt"></i>
			<span>Sản phẩm</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo url("admin/san-pham/create"); ?>"><i class="fa fa-plus"></i>Thêm</a></li>
			<li><a href="<?php echo url("admin/san-pham"); ?>"><i class="fa fa-list"></i>Danh sách</a></li>
		</ul>
	</li>

	<li class="treeview <?php echo e((Request::is('admin/mau-sac/create') || Request::is('admin/mau-sac/edit') || Request::is('admin/mau-sac') ? 'active' : '')); ?>">
		<a href="#">
			<i class="fa fa-pencil" aria-hidden="true"></i>
			<span>Màu sản phẩm</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo url("admin/mau-sac/create"); ?>"><i class="fa fa-plus"></i>Thêm</a></li>
			<li><a href="<?php echo url("admin/mau-sac"); ?>"><i class="fa fa-list"></i>Danh sách</a></li>
		</ul>
	</li>

	<li class="treeview <?php echo e((Request::is('admin/ma-giam-gia/create') || Request::is('admin/ma-giam-gia/edit') || Request::is('admin/ma-giam-gia') ? 'active' : '')); ?>">
		<a href="#">
			<i class="fa fa-credit-card-alt"></i>
			<span>Mã giảm giá</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo url("admin/ma-giam-gia/create"); ?>"><i class="fa fa-plus"></i>Thêm</a></li>
			<li><a href="<?php echo url("admin/ma-giam-gia"); ?>"><i class="fa fa-list"></i>Danh sách</a></li>
		</ul>
	</li>

	<li class="treeview <?php echo e((Request::is('admin/banner/create') || Request::is('admin/banner/edit') || Request::is('admin/banner') ? 'active' : '')); ?>">
		<a href="#">
			<i class="fa fa-picture-o"></i>
			<span>Banner</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo url("admin/banner/create"); ?>"><i class="fa fa-plus"></i>Thêm</a></li>
			<li><a href="<?php echo url("admin/banner"); ?>"><i class="fa fa-list"></i>Danh sách</a></li>
		</ul>
	</li>
	
	<li class="treeview <?php echo e((Request::is('admin/chuyen-muc/create') || Request::is('admin/chuyen-muc/edit') || Request::is('admin/chuyen-muc') ? 'active' : '')); ?>">
		<a href="#">
			<i class="fa fa-bars"></i>
			<span>Chuyên mục tin tức</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo url("admin/chuyen-muc/create"); ?>"><i class="fa fa-plus"></i>Thêm</a></li>
			<li><a href="<?php echo url("admin/chuyen-muc"); ?>"><i class="fa fa-list"></i>Danh sách</a></li>
		</ul>
	</li>
	<li class="treeview <?php echo e((Request::is('admin/bai-viet/create') || Request::is('admin/bai-viet/edit') || Request::is('admin/bai-viet') ? 'active' : '')); ?>">
		<a href="#">
			<i class="fa fa-file-o"></i>
			<span>Tin tức</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo url("admin/bai-viet/create"); ?>"><i class="fa fa-plus"></i>Thêm</a></li>
			<li><a href="<?php echo url("admin/bai-viet"); ?>"><i class="fa fa-list"></i>Danh sách</a></li>
		</ul>
	</li>
	

	<li class="treeview <?php echo e((Request::is('admin/user/create') || Request::is('admin/user/edit') || Request::is('admin/user') ? 'active' : '')); ?>">
		<a href="#">
			<i class="fa fa-user"></i>
			<span>Người dùng</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo url("admin/user/create"); ?>"><i class="fa fa-plus"></i>Thêm</a></li>
			<li><a href="<?php echo url("admin/user"); ?>"><i class="fa fa-list"></i>Danh sách</a></li>
		</ul>
	</li>

	<li class=" <?php echo e((Request::is('admin/lien-he/create') || Request::is('admin/lien-he/edit') || Request::is('admin/lien-he') ? 'active' : '')); ?>">
		<a href="<?php echo url("admin/lien-he"); ?>">
			<i class="fa fa-comment"></i>
			<span>Liên hệ</span>
		</a>
	</li>
	<li class="treeview <?php echo e((Request::is('admin/menu') || Request::is('admin/cai-dat') ? 'active' : '')); ?>">
		<a href="#">
			<i class="fa fa-cogs"></i>
			<span>Cài đặt</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="<?php echo url("admin/menu"); ?>"><i class="fa fa-bars"></i>Menu</a></li>
			<li><a href="<?php echo url("admin/tuy-chon-hien-thi"); ?>"><i class="fa fa-television"></i>Hiển thị</a></li>
			<li><a href="<?php echo url("admin/cai-dat"); ?>"><i class="fa fa-angle-right"></i>Tùy chỉnh</a></li>
		</ul>
	</li>

	
</ul>