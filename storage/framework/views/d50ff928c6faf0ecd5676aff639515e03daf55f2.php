<?php $__env->startSection('head'); ?>
    <meta name="keywords" content="<?php echo e($setting['seo_keyword']); ?>" />
    <meta name="description" content="<?php echo e($setting['seo_description']); ?>" />
    <link rel="SHORTCUT ICON" href="<?php echo $setting['icon_website']; ?>">
    <title><?php echo e($setting['seo_title']); ?></title>

    <meta property="og:title" content="<?php echo e($setting['seo_title']); ?>">
    <meta property="og:description" content="<?php echo e($setting['seo_description']); ?>">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo URL::current(); ?>">
    <meta property="og:site_name" content="<?php echo e($setting['company_name']); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
        
    <h1 class="hidden"><?php echo e($setting['seo_title']); ?></h1>
    <div class="body-page">
        <div class="container">
            <div class="row">
                <div class="hidden-xs hidden-sm col-md-3 col-lg-3 left-content">
                    
                    <div class="side-category">
                        <div class="side-title">
                            <h4><i class="fa fa-bars" aria-hidden="true"></i>Tất cả sản phẩm</h4>
                        </div>
                        <ul class="side-content">
                            <?php $__currentLoopData = $cate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li>
                                <a href="/danh-muc/<?php echo e($item->slug); ?>.html">
                                    <?php echo e($item->name); ?>

                                </a>
                                <?php if($item->sub_cate->count()>0): ?>
                                <button class="accordion"></button>
                                <ul class="sub-menu">
                                    <?php $__currentLoopData = $item->sub_cate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $it): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><a href="/danh-muc/<?php echo e($it->slug); ?>.html"><?php echo e($it->name); ?></a></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                                <?php endif; ?>
                            </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                    </div>

                    <div class="search-price">
                        <div class="title">
                            <span>Khoảng giá</span>
                        </div>
                        <div class="content">
                            <form action="/tim-kiem-san-pham" method="get">
                                <div class="form-group clearfix">
                                    <select class="form-control" name="filter_price" id="">
                                        <option value="all">--Chọn khoảng giá--</option>
                                        <option value="duoi-500000">Dưới 500.000đ</option>
                                        <option value="tu-500000-den-1000000">500.000đ- 1.000.000đ</option>
                                        <option value="tu-1000000-den-1500000">1.000.000đ- 1.500.000đ</option>
                                        <option value="tu-1500000-den-2000000">1.500.000đ- 2.000.000đ</option>
                                        <option value="tu-2000000-den-2500000">2.000.000đ- 2.500.000đ</option>
                                        <option value="tu-2500000-den-3000000">2.500.000đ- 3.000.000đ</option>
                                        <option value="tren-3000000">Trên 3.000.000đ</option>
                                    </select>
                                    <button type="submit" class="btn btn-submit"><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 right-content">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 breadcrumb-wrap">
                            <ul class="breadcrumb">
                                <li><a href="/">Trang chủ</a></li>
                                <li><strong><?php echo e($category->name); ?></strong></li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 list-product">
                            <div class="row">
                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 product-item">
                                    <div class="product-box">
                                        <div class="product-thumb">
                                            <a href="/san-pham/<?php echo e($item->slug); ?>.html"><img src="<?php echo e($item->thumbnail); ?>" alt="<?php echo e($item->name); ?>"></a>
                                            <?php if($item->price_sale != $item->price): ?>
                                            <div class="sale">-<?php echo e(round(((1-(($item->price_sale)/($item->price)))*100),0)); ?> %</div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="product-name"><a href="/san-pham/<?php echo e($item->slug); ?>.html">
                                           <?php echo e(subtext(($item->name),100)); ?>

                                        </a></div>
                                        <div class="product-price">
                                            <?php if($item->qty_in_stock > 0): ?>
                                                <?php if($item->price_sale != $item->price): ?>
                                                <div class="price-old"><?php echo e(number_format($item->price)); ?> đ</div>
                                                <?php endif; ?>
                                                <div class="price-new"><?php echo e(number_format($item->price_sale)); ?> đ</div>
                                            <?php else: ?>
                                                <div class="price-new">Hết hàng</div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                        <div class="pagination-wrap">
                            <?php echo e($products->links()); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front-end.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>