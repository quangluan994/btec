<?php $__env->startSection('title'); ?>
Chỉnh sửa chuyên mục
##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header_styles'); ?>
<!-- plugin styles-->
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')); ?>"/>
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')); ?>"/>
<!--end of page level css-->
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <h1>Chỉnh sửa chuyên mục</h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="<?php echo e(url('admin/danh-muc')); ?>">Danh mục</a></li>
          <li class="active">Chỉnh sửa</li>
        </ol>
    </section>
    <section class="content">
        <?php if($errors->any()): ?>
        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
            <button type="button" class="close">×</button>
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      
                        <div class="col-xs-6"><h4 style="font-weight: bolder">Chỉnh sửa chuyên mục: <?php echo $catepost->name; ?></h4></div>
                      
                        <div class="col-xs-6" ><a href="<?php echo e(url("admin/chuyen-muc")); ?>"  class="btn btn-info" title="" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i> Quay lại</a></div>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal login_validator" id="tryitForm" action="<?php echo url("admin/chuyen-muc"); ?>/<?php echo $catepost->id; ?>/update" method="post">
                            <div class="col-12">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Tên chuyên mục *</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="name" id="name" class="form-control" value="<?php echo $catepost->name; ?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Slug *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="slug" id="slug" class="form-control" value="<?php echo $catepost->slug; ?>" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">URL: </label>
                                    <div class="col-xl-7 col-lg-8">
                                        /chuyen-muc-tin/<span id="url_slug"><?php echo $catepost->slug; ?></span>.html
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="parent_id" class="col-lg-3 control-label">Chọn chuyên mục cha *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <select required name="parent_id" id="parent_id" class="validate[required] form-control select2">
                                            <?php if($catepost->parent_id !=0 && $catepost->parent_id != NULL): ?>
                                            <option value="<?php echo $catepost->parent_cate->id; ?>" style="text-align: center" selected ><?php echo $catepost->parent_cate->name; ?></option>
                                            <option value="0">-- Chọn chuyên mục cha --</option>
                                            <?php showCateselected($cate,$catepost); ?>
                                            <?php else: ?>
                                                <option value="0">-- Chọn chuyên mục cha --</option>
                                                <?php showCategories($cate); ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-xl-7 col-lg-8 add_user_checkbox_error push-lg-3">
                                        <div>
                                            <label class="custom-control custom-checkbox">
                                                <input id="publish" type="checkbox" name="publish"
                                                       class="custom-control-input" <?php echo $catepost->publish ? 'checked' : ''; ?> >
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description"> Publish </span>
                                            </label>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <h3>SEO</h3>
                                        <hr style="margin: 0">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_title" class="col-lg-3 control-label">Seo title</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <input type="text" name="seo_title" id="seo_title" class="form-control" value="<?php echo $catepost->seo_title; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_keyword" class="col-lg-3 control-label">Seo keyword</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <input type="text" name="seo_keyword" id="seo_keyword" class="form-control" value="<?php echo $catepost->seo_keyword; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_description" class="col-lg-3 control-label">Seo descripton</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <textarea name="seo_description" class="form-control my-editor" rows="7" id="seo_description"><?php echo e($catepost->seo_description); ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-lg-9 push-lg-3">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Cập nhật</button>
                                        <button class="btn btn-warning" type="reset" id="clear"><i class="fa fa-refresh"></i>Nhập lại</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $("#slug").keyup(function(){
            var slug = $("#slug").val();
            $('#url_slug').html(slug);
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>