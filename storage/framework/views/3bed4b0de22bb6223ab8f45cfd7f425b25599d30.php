<?php $__env->startSection('title'); ?>
Chỉnh sửa danh mục
##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>
<?php $__env->startSection('header_styles'); ?>
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')); ?>"/>
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')); ?>"/>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <h1>Chỉnh sửa danh mục</h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="<?php echo e(url('admin/danh-muc')); ?>">Danh mục</a></li>
          <li class="active">Chỉnh sửa</li>
        </ol>
    </section>
    <section class="content">
        <?php if($errors->any()): ?>
        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
            <button type="button" class="close">×</button>
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="col-xs-6"><h4 style="font-weight: bolder">Chỉnh sửa danh mục: <?php echo $category->name; ?></h4></div>
                      
                        <div class="col-xs-6" ><a href="<?php echo e(url("admin/danh-muc")); ?>"  class="btn btn-info" title="" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i> Quay lại</a></div>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal login_validator" id="tryitForm" action="<?php echo url("admin/danh-muc"); ?>/<?php echo $category->id; ?>/update" method="post">
                            <div class="col-12">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Tên danh mục *</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="name" id="name" class="form-control" value="<?php echo $category->name; ?>" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Slug *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="slug" id="slug" class="form-control" value="<?php echo $category->slug; ?>" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">URL: </label>
                                    <div class="col-xl-7 col-lg-8">
                                        /danh-muc/<span id="url_slug"><?php echo $category->slug; ?></span>.html
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Hình ảnh</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                                <span class="input-group-btn">
                                                <a href="/filemanager/dialog.php?type=0&field_id=thumb_0" class="btn btn-primary red iframe-btn" id="iframe-btn-0"><i class="fa fa-picture-o"></i>Chọn ảnh</a>
                                                </span>
                                                <input id="thumb_0" class="form-control" type="text" name="image" value="<?php echo e($category->image); ?>">
                                        </div>
                                        <div id="preview">
                                            <img id="holder" src="<?php echo e($category->image); ?>" style="margin-top:15px;max-height:100px;">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="parent_id" class="col-lg-3 control-label">Chọn danh mục cha *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <select required name="parent_id" id="parent_id" class="validate[required] form-control select2">
                                            <?php if($category->parent_id !=0 && $category->parent_id != NULL): ?>
                                            <option value="<?php echo $category->parent_cate->id; ?>" style="text-align: center" selected ><?php echo $category->parent_cate->name; ?></option>
                                            <option value="0">-- Chọn danh mục cha --</option>
                                            <?php showCateselected($cate,$category); ?>
                                            <?php else: ?>
                                                <option value="0">-- Chọn danh mục cha --</option>
                                                <?php showCategories($cate); ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-xl-7 col-lg-8 add_user_checkbox_error push-lg-3">
                                        <div>
                                            <label class="custom-control custom-checkbox">
                                                <input id="publish" type="checkbox" name="publish"
                                                       class="custom-control-input" <?php echo $category->publish ? 'checked' : ''; ?> >
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description"> Publish </span>
                                            </label>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <h3>SEO</h3>
                                        <hr style="margin: 0">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_title" class="col-lg-3 control-label">Seo title (<span id="count_seo_title"><?php echo e(mb_strlen($category->seo_title, 'UTF-8')); ?></span>)</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <input type="text" name="seo_title" id="seo_title" class="form-control" value="<?php echo $category->seo_title; ?>" onKeyDown="countNumseo_title(this.form.seo_title);" onKeyUp="countNumseo_title(this.form.seo_title);">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_keyword" class="col-lg-3 control-label">Seo keyword (<span id="count_seo_keyword"><?php echo e(mb_strlen($category->seo_keyword, 'UTF-8')); ?></span>)</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <input type="text" name="seo_keyword" id="seo_keyword" class="form-control" value="<?php echo $category->seo_keyword; ?>" onKeyDown="countNumseo_keyword(this.form.seo_keyword);" onKeyUp="countNumseo_keyword(this.form.seo_keyword);">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_description" class="col-lg-3 control-label">Seo descripton (<span id="count_seo_description"><?php echo e(mb_strlen($category->seo_description, 'UTF-8')); ?></span>)</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <textarea name="seo_description" class="form-control my-editor" rows="7" id="seo_description" onKeyDown="countNumseo_description(this.form.seo_description);" onKeyUp="countNumseo_description(this.form.seo_description);"><?php echo e($category->seo_description); ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-lg-9 push-lg-3">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i>Cập nhật</button>
                                        <button class="btn btn-warning" type="reset" id="clear"><i class="fa fa-refresh"></i>Nhập lại</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="<?php echo e(asset('assets/fancybox/source/jquery.fancybox.js')); ?>"></script>
    <script>
   
        $('#iframe-btn-0').fancybox({
            'width': 900,
            'height': 900,
            'type': 'iframe',
            'autoScale': false,
            'autoSize': false,
            afterClose: function() {
                var thumb = $('#thumb_0').val();
                var html = '<div class="img_preview"><img src="'+thumb+'"/>';
                html +='<input type="hidden" name="image" value="'+thumb+'" /> </div>';
                $('#preview').html(html);
            }
        });
    </script>
    <script>
        $("#slug").keyup(function(){
            var slug = $("#slug").val();
            $('#url_slug').html(slug);
        });
    </script>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('admin.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>