<?php $__env->startSection('title'); ?>
Danh sách đơn hàng
##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header_styles'); ?>
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/tablesid.css')); ?>"/>

<!--End of page level styles-->
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <h1><i class="fa fa-th"></i> Danh sách đơn hàng</h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="<?php echo e(url('admin/order')); ?>">Đơn hàng</a></li>
          <li class="active">Danh sách</li>
        </ol>
    </section>

    <section class="content">
        <?php if(Session::has('success')): ?>
            <div class="callout alert alert-success col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto" role="alert" >
                <button type="button" class="close">×</button> 
                <?php echo e(Session::get('success')); ?>

            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border text-right">
                        <div class=" form-group fix-float">
                            <select class="styled hasCustomSelect form-control" style="width:150px; float:left">
                                <option>Chọn tác vụ</option>
                                <option value="remove">Xóa</option>
                            </select>
                            <a href="javascript:action();" class="btn btn-info">Apply</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="card-block p-t-25">
                            <div class ="row col-lg-12"> 
                                <table class="respontables">
                                    <tr>
                                        <th style="width: 1%"> <input type="checkbox" class="check-all"></th>
                                        <th class="text-center">Mã đơn hàng</th>
                                        <th class="text-center">Người đặt</th>
                                        <th class="text-center">Giảm giá(%)</th>
                                        <th class="text-center">Giá trị</th>
                                        <th class="text-center">Ngày đặt</th>
                                        <th class="text-center">Thanh toán</th>
                                        <th class="text-center">Trạng thái</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                    <?php $__empty_1 = true; $__currentLoopData = $order; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                        <tr>
                                            <td>
                                            <input type="checkbox" class="checkbox" name="checkbox_id" value="<?php echo e($item->id); ?>" id="select-<?php echo e($item->id); ?>">
                                            </td>
                                            <td class="text-center">PAY.ORDER.<?php echo e(addchart($item->id)); ?></td>
                                            <td class="text-center"><?php echo e($item->receiver_name); ?></td>
                                            <td class="text-center"><?php echo e($item->discount); ?></td>
                                            <td class="text-center"><?php echo e(number_format($item->total_amount)); ?> đ</td>
                                            <td class="text-center">
                                                <?php echo $item->created_at->format('d-m-Y'); ?>

                                            </td>
                                            <td class="text-center">
                                                <?php if($item->pay_method == 'store'): ?>
                                                    Tại cửa hàng
                                                <?php elseif($item->pay_method == 'cod'): ?>
                                                    COD
                                                <?php else: ?>
                                                    Qua cổng thanh toán
                                                <?php endif; ?>
                                            </td>
                                            <td class="text-center">
                                                <form action="<?php echo url("admin/order"); ?>/<?php echo $item->id; ?>/update" method="post">
                                                    <?php echo e(csrf_field()); ?>

                                                    <select name="status" class="form-control">
                                                        <option value="dang_cho" <?php echo e($item->status == 'dang_cho' ? 'selected' : ''); ?>>Đang chờ xử lý</option>
                                                        <option value="da_giao_hang" <?php echo e($item->status == 'da_giao_hang' ? 'selected' : ''); ?>>Đã giao hàng</option>
                                                        <option value="chua_thanh_toan" <?php echo e($item->status == 'chua_thanh_toan' ? 'selected' : ''); ?>>Thanh toán thất bại</option>
                                                        <option value="da_thanh_toan" <?php echo e($item->status == 'da_thanh_toan' ? 'selected' : ''); ?>>Thanh toán thành công</option>
                                                    </select><br/>
                                                    <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Cập nhật</button>
                                                </form>
                                            </td>
                                            <td class="text-center">
                                                <a href="<?php echo e(url('admin/order/'.$item->id)); ?>" class="btn btn-success" title=""><i class="fa fa-eye"></i></a>
                                                <a class="btn btn-danger delitem" id="del_<?php echo e($item->id); ?>"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                    <tr><td colspan="9"><h4>Không có dữ liệu để hiển thị</h4></td></tr>  
                                    <?php endif; ?>     
                                </table>    
                            </div>
                            <div style="clear:both"></div>
                            <div class="row col-lg-4 col-md-6 col-sm-6" style="float:right">
                                <?php echo e($order->links()); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo e(csrf_field()); ?>

    </section>
    <script>
        $(".delitem").click(function(){
            var id = $(this).attr("id");
            var splitid = id.split("_");
            var deleteid = splitid[1];
            console.log(deleteid);
            swal({
              title: "Bạn chắc chắn muốn xóa?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Ok! Xóa ngay",
              closeOnConfirm: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    

                    var token = $("input[name='_token']").val();
                    $.ajax({
                        type: 'get',
                        url: '/admin/order/'+deleteid+'/delete',
                        beforeSend: function(xhr){
                            xhr.setRequestHeader('X-CSRF-Token', token);
                        },
                        success: function(response) {
                            var res = $.parseJSON(response);
                            console.log(res);
                            if(!res.status) {
                                return;
                            }
                            location.reload(true);
                            swal({
                                title: "Thành công!",
                                timer: 2000,
                                type: "success"
                            });
                        }
                    });
                }
            });
        });
    </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>