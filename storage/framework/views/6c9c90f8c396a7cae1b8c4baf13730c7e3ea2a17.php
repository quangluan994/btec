<?php $__env->startSection('head'); ?>
    <meta name="keywords" content="<?php echo e($setting['seo_keyword']); ?>" />
    <meta name="description" content="<?php echo e($setting['seo_description']); ?>" />
    <link rel="SHORTCUT ICON" href="<?php echo $setting['icon_website']; ?>">
    <title><?php echo e($setting['seo_title']); ?></title>

    <meta property="og:title" content="<?php echo e($setting['seo_title']); ?>">
    <meta property="og:description" content="<?php echo e($setting['seo_description']); ?>">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo URL::current(); ?>">
    <meta property="og:site_name" content="<?php echo e($setting['company_name']); ?>">

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
        
    <div class="cart-page body-page">
        <div class="container">
            <div class="head">
                <h1>GIỎ HÀNG</h1>
            </div>
            <?php if(Session::has('danger')): ?>
                <div class="alert alert-danger"><?php echo e(Session::get('danger')); ?>

                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-9">
                    <table class="table table-left">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Sản phẩm</th>
                                <th style="white-space:nowrap;">Ghi chú</th>
                                <th>Giá</th>
                                <th>Số lượng</th>
                                <th>Tổng cộng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(Cart::countRows() > 0): ?>
                            <?php $__currentLoopData = Cart::content(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td scope="row"><a href="<?php echo url("cart/$item->rowId/delete"); ?>" class="btn-delete"><button><i class="fa fa-times"></i></button></a></td>
                                <td><img src="<?php echo e($item->options->img); ?>" alt="<?php echo e($item->name); ?>"></td>
                                <td><a href="#"><?php echo e(subtext(($item->name),100)); ?></a></td>
                                <td><?php echo e($item->options->color); ?></td>
                                <td class="nowrap"><?php echo e(number_format($item->price)); ?> đ</td>
                                <td>
                                    <form action="cart/<?php echo e($item->rowId); ?>/update" method="post">
                                        <?php echo e(csrf_field()); ?>

                                        <div class="form-group">
                                            <input type="number" name="qty" id="" value="<?php echo e($item->qty); ?>">
                                            <button type="submit"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                                        </div>
                                    </form>
                                </td>
                                <td class="nowrap"><?php echo e(number_format($item->subtotal)); ?> đ</td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="6">Không có sản phẩm nào trong giỏ hàng</td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-3">
                    <table class="table table-right">
                        <thead>
                            <tr>
                                <th colspan="2" class="text-center">Tổng số lượng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row">Số lượng:</td>
                                <td><?php echo e(Cart::count()); ?></td>
                            </tr>
                            <tr>
                                <td scope="row">Tổng cộng:</td>
                                <td class="nowrap"><?php echo e(Cart::total(0)); ?> đ</td>
                            </tr>
                            <?php if(Cart::countRows() > 0): ?>
                            <tr>
                                <td scope="row" colspan="2" class="text-center">
                                    <a href="/check-out"><button class="submit">Thanh toán</button></a>
                                </td>
                            </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class=" more-product">
                    <div class="home-item">
                        <div class="item-wrap">
                            <div class="category-box">
                                <div class="category-title">
                                    <h2><a href="#">SẢN PHẨM MỚI</a></h2>
                                </div>
                                <div class="category-content">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 right-content">
                                            <div class="row">
                                                <?php if($product_new): ?>
                                                <?php $__currentLoopData = $product_new; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 product-item">
                                                    <div class="product-box">
                                                        <div class="product-thumb">
                                                            <a href="/san-pham/<?php echo e($item->slug); ?>.html"><img src="<?php echo e($item->thumbnail); ?>" alt="<?php echo e($item->name); ?>"></a>
                                                        </div>
                                                        <div class="product-name"><a href="/san-pham/<?php echo e($item->slug); ?>.html">
                                                                <?php echo e(subtext(($item->name),100)); ?>

                                                            </a></div>
                                                        <div class="product-price">
                                                            <?php if($item->qty_in_stock > 0): ?>
                                                            <?php if($item->price != $item->price_sale): ?>
                                                                <div class="price-old"><?php echo e(number_format($item->price)); ?> đ</div>
                                                            <?php endif; ?>
                                                                <div class="price-new"><?php echo e(number_format($item->price_sale)); ?> đ</div>
                                                            <?php else: ?>
                                                                <div class="price-new">Hết hàng</div>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front-end.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>