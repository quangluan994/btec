<?php $__env->startSection('head'); ?>
    <meta name="keywords" content="<?php echo e($setting['seo_keyword']); ?>" />
    <meta name="description" content="<?php echo e($setting['seo_description']); ?>" />
    <link rel="SHORTCUT ICON" href="<?php echo $setting['icon_website']; ?>">
    <title><?php echo e($setting['seo_title']); ?></title>

    <meta property="og:title" content="<?php echo e($setting['seo_title']); ?>">
    <meta property="og:description" content="<?php echo e($setting['seo_description']); ?>">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo URL::current(); ?>">
    <meta property="og:site_name" content="<?php echo e($setting['company_name']); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <?php if($order): ?>   
    <div class="orders-page">
        <div class="container">
            <div class="head">
                <h1>ĐƠN HÀNG ĐÃ NHẬN</h1>
                <p>Cảm ơn bạn. Đơn hàng của bạn đã được nhận thành công.</p>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <table class="table table-head">
                        <tbody>
                            <tr>
                                <td scope="row">MÃ ĐƠN HÀNG:</td>
                                <td>NGÀY:</td>
                                <?php if($order->discount != 0): ?>
                                <td>GIẢM GIÁ(%)</td>
                                <?php endif; ?>
                                <td>TỔNG CỘNG:</td>
                                <td>PHƯƠNG THỨC THANH TOÁN:</td>
                            </tr>
                            <tr>
                                <td scope="row">PAY.ORDER.<?php echo e(addchart($order->id)); ?></td>
                                <td><?php echo e($order->created_at->format('d-m-Y')); ?></td>
                                <?php if($order->discount != 0): ?>
                                <td><?php echo e($order->discount); ?>%</td>
                                <?php endif; ?>
                                <td><?php echo e(number_format($order->total_amount)); ?> đ</td>
                                <td>
                                    <?php
                                    $i = $order->pay_method;
                                    if($i == 'store')
                                        echo 'Thanh toán trực tiếp tại cửa hàng';
                                    elseif($i == 'cod')
                                        echo 'Trả tiền mặt khi giao hàng(COD)';
                                    else
                                        echo 'Thanh toán qua cổng thanh toán vtcPay';
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-8">
                    <h2>Chi tiết đơn hàng</h2>
                    <table class="table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Sản phẩm</th>
                                <th>Số lượng</th>
                                <th>Tổng cộng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $order->orderdetail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td style="width:150px"><img src="<?php echo e($item->product->thumbnail); ?>" width="120px"></td>
                                <td scope="row"><?php echo e($item->product->name); ?></td>
                                <td><?php echo e($item->quantity); ?></td>
                                <td> <?php echo e(number_format(($item->price_sale)*($item->quantity))); ?> đ</td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <tr style="font-weight: bolder; color:red">
                                <td scope="row" colspan="3">Phương thức thanh toán</td>
                                <td>Kiểm tra thanh toán</td>
                            </tr>
                            <?php if($order->discount != 0): ?>
                            <tr style="font-weight: bolder; color:red">
                                <td scope="row" colspan="3">Giảm giá</td>
                                <td><?php echo e($order->discount); ?>%</td>
                            </tr>
                            <?php endif; ?>
                            <tr style="font-weight: bolder; color:red">
                                <td scope="row" colspan="3">Tổng</td>
                                <td ><?php echo e(number_format($order->total_amount)); ?> đ</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php else: ?>

        <main class="error-page">
            <div class="page-404">
                <img src="images/404.png" alt="404">
                <h2>Lỗi!, Nội dung không tìm thấy</h2>
                <a href="#">Trở về trang chủ</a>
            </div>
        </main>
    <?php endif; ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front-end.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>