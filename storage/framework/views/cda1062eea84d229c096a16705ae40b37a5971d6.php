<?php $__env->startSection('title'); ?>
Thêm chuyên mục
##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header_styles'); ?>
<!-- plugin styles-->
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')); ?>"/>
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')); ?>"/>
<!--end of page level css-->
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <h1>Thêm chuyên mục</h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="<?php echo e(url('admin/chuyen-muc')); ?>">Chuyên mục</a></li>
          <li class="active">Thêm</li>
        </ol>
    </section>
    <section class="content">
        <?php if($errors->any()): ?>
        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
            <button type="button" class="close">×</button>
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      
                        <div class="col-xs-6"><h4>Thêm chuyên mục</h4></div>
                      
                        <div class="col-xs-6" ><a href="<?php echo e(url("admin/chuyen-muc")); ?>"  class="btn btn-info" title="" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i> Quay lại</a></div>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal login_validator" id="tryitForm" action="<?php echo url("admin/chuyen-muc"); ?>" method="post">
                            <div class="col-12">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Tên chuyên mục *</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="name" id="name" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Slug *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="slug" id="slug" class="form-control" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">URL: </label>
                                    <div class="col-xl-7 col-lg-8">
                                        /chuyen-muc-tin/<span id="url_slug"></span>.html
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Chọn chuyên mục cha *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-plus text-primary"></i></span>
                                            <select name = 'parent_id' class = 'form-control'>
                                                <option value="0">-- Chọn chuyên mục cha --</option>
                                                <?php showCategories($cate); ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-xl-7 col-lg-8 add_user_checkbox_error push-lg-3">
                                        <div>
                                            <label class="custom-control custom-checkbox">
                                                <input id="publish" type="checkbox" name="publish" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description"> Publish </span>
                                            </label>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <h3>SEO</h3>
                                        <hr style="margin: 0">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_title" class="col-lg-3 control-label">Seo title</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <input type="text" name="seo_title" id="seo_title" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_keyword" class="col-lg-3 control-label">Seo keyword</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <input type="text" name="seo_keyword" id="seo_keyword" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_description" class="col-lg-3 control-label">Seo descripton</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <textarea name="seo_description" class="form-control my-editor" rows="7" id="seo_description"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-lg-9 push-lg-3">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-plus"></i>Thêm</button>
                                        <button class="btn btn-warning" type="reset" id="clear"><i class="fa fa-refresh"></i>Nhập lại</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_scripts'); ?>
<script type="text/javascript" src="<?php echo e(asset('assets/js/pages/validation.js')); ?>"></script>
<!-- end of page level js -->

<script>
    $("#name").keyup(function(){
        var name = $("#name").val();
        var token = $('input[name=_token]').val();
        if(name) {
            $.ajax({
            type: 'post',
            url: '/slugCatePost',
            headers: { 'X-XSRF-TOKEN' : token },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', token);
                },
                data: {type: 'ajax', _csrfToken : token},
            data: {name: name},
            success: function(data) {
                data = $.parseJSON(data);
                $('#slug').val(data);
                $('#url_slug').html(data);
            },
            error: function(error) {
                console.log('error');
            }
        });        
        }
    });
</script>
<script>
        $("#slug").keyup(function(){
            var slug = $("#slug").val();
            $('#url_slug').html(slug);
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>