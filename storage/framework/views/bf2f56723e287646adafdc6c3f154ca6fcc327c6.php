
        <footer>
            <div class="container footer-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="logo-bottom">
                            <a href="/"><img src="<?php echo e($setting['logo_footer']); ?>" alt=""></a>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <?php $__currentLoopData = $footer_menu; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <h4 class="title"><?php echo e($item->label); ?></h4>
                        <?php if($item->getsons($item->id)->count()>0): ?>
                        <div class="content">
                            <ul class="nav-bot">
                                <?php $__currentLoopData = $item->getsons($item->id); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <li>
                                    <i class="fa fa-star"></i>
                                    <a href="<?php echo e($sub->link); ?>"> <?php echo e($sub->label); ?></a>
                                </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <h4 class="title"><?php echo e($setting['company_name']); ?></h4>
                        <div class="content">
                            <p>
                                <i class="fa fa-map-marker" aria-hidden="true"></i><?php echo e($setting['company_address']); ?>

                            </p>
                            <p>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <a href="#"><?php echo e($setting['hot_line']); ?></a>
                            </p>
                            <p>
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <a href="#"><?php echo e($setting['company_email']); ?></a>
                            </p>
                            <a class="btn-hostline" href="#">Hotline: <?php echo e($setting['hot_line']); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bot">
                <div class="container">
                    <span>
                        © Bản quyền thuộc về Creative Team | Cung cấp bởi <a href="https://gemisoft.com.vn">Gemisoft</a>
                    </span>
                </div>
            </div>
        </footer>

        <?php if(Session::has('thanhcong')): ?>
        <div class="callout alert alert-success col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto; background-color: red; color:white" role="alert">
            <button type="button" class="close">×</button>
            <?php echo e(Session::get('thanhcong')); ?>

        </div>
        <?php endif; ?>
    </div>
    
    <script type="text/javascript" src="<?php echo e(asset('front-end/node_modules/jquery/dist/jquery.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('front-end/node_modules/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('front-end/node_modules/owl.carousel/dist/owl.carousel.min.js')); ?>"></script>
    <script src="<?php echo e(asset('front-end/js/js.js')); ?>"></script>

    <?php echo $__env->yieldContent('script'); ?>

    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.12&appId=429389374186066&autoLogAppEvents=1';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <script>
    $(".callout").css("transform", 'translateY(-100px)');
        $('.alert-success').delay(8000).fadeOut('slow', function() {
            $('.alert-success').hide();
        });
        $('.alert-danger').delay(8000).fadeOut('slow', function() {
            $('.alert-danger').hide();
        });
        $(document).on('click', '.close', function() {
            $(this).parent().hide();
        });
        $(document).on('click', '.close-thik', function() {
            $(this).parent().remove();
        });
    </script>
    
    
</body>
<?php echo $setting['add_code_footer']; ?>


</html>