<?php $__env->startSection('title'); ?>
Chỉnh sửa sản phẩm
##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header_styles'); ?>
<!-- plugin styles-->
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')); ?>"/>
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')); ?>"/>
<!--end of page level css-->
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>



    <section class="content-header">
        <h1>Chỉnh sửa sản phẩm</h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="<?php echo e(url('admin/san-pham')); ?>">Sản phẩm</a></li>
          <li class="active">Chỉnh sửa</li>
        </ol>
    </section>
    <section class="content">
        <?php if($errors->any()): ?>
        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
            <button type="button" class="close">×</button>
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      
                        <div class="col-xs-6"><h4 style="font-weight: bolder">Chỉnh sửa Sản phẩm: <?php echo e($product->name); ?></h4></div>
                        <div class="col-xs-6" ><a href="<?php echo e(url("admin/san-pham")); ?>"  class="btn btn-info" title="" style="float:right"><i class="fa fa-chevron-left" aria-hidden="true"></i> Quay lại</a></div>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal login_validator" id="tryitForm" action="<?php echo url("admin/san-pham"); ?>/<?php echo $product->id; ?>/update" method="post">
                            <div class="col-12">
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Tên Sản phẩm *</label>
                                    <div class="col-xl-7 col-lg-8 ">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="name" id="name" value="<?php echo e($product->name); ?>" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Slug *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-text-width text-primary"></i></span>
                                            <input type="text" name="slug" id="slug" value="<?php echo e($product->slug); ?>" class="form-control" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">URL: </label>
                                    <div class="col-xl-7 col-lg-8">
                                        /san-pham/<span id="url_slug"><?php echo e($product->slug); ?></span>.html
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="price" class="col-lg-3 control-label">Giá sản phẩm *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-credit-card-alt text-primary"></i></span>
                                            <input type="text" name="price" id="price" class="form-control" value="<?php echo $product->price; ?>" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="price_sale" class="col-lg-3 control-label">Giảm giá</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-caret-down text-primary"></i></span>
                                            <input type="text" name="price_sale" id="price_sale" class="form-control" value="<?php echo $product->price_sale; ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Hình ảnh *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                                <span class="input-group-btn">
                                                <a href="/filemanager/dialog.php?type=0&field_id=thumb_0" class="btn btn-primary red iframe-btn" id="iframe-btn-0"><i class="fa fa-picture-o"></i>Chọn ảnh</a>
                                                </span>
                                                <input id="thumb_0" class="form-control" type="text" name="thumbnail" required value="<?php echo e($product->thumbnail); ?>">
                                        </div>
                                        <div id="preview">
                                            <img id="holder" src="<?php echo e($product->thumbnail); ?>" style="margin-top:15px;max-height:100px;">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="gallery" class="col-lg-3 control-label">Gallery *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                            <a href="/filemanager/dialog.php?type=0&field_id=thumb_1" class="btn btn-primary red iframe-btn" id="iframe-btn-1"><i class="fa fa-picture-o"></i>Chọn ảnh</a>
                                            </span>
                                            <input id="thumb_1" type="hidden">
                                        </div>
                                        <div id="preview1">
                                            <?php 
                                                $a = $product->gallery;
                                                $b = json_decode($a,true);
                                                for($i=0;$i<count($b);$i++)
                                                {
                                                    echo '<div class="img_preview"><a class="close-thik"></a><img src="'.$b[$i].'"/><input type="hidden" name="gallery[]" value="'.$b[$i].'" /></div>';
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="qty_in_stock" class="col-lg-3 control-label">Số lượng *</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-credit-card-alt text-primary"></i></span>
                                            <input type="number" name="qty_in_stock" min="0" id="qty_in_stock" class="form-control" value="<?php echo e($product->qty_in_stock); ?>" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="color_product" class="col-lg-3 control-label" >Màu sản phẩm 
                                        <span>
                                        <a class="btn btn-primary iframe-btn" id="iframe-btn-plus"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                        </span>
                                    </label>

                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <div class="input-group">
                                                
                                            </div>

                                        </div>
                                        <div id="preview2">
                                            <?php $__currentLoopData = $product->colorProduct; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="col-lg-12" style="margin-bottom:5px">
                                                <div class="col-lg-5">
                                                    <select name="color_id[]" class=" form-control select2">
                                                        <?php $__currentLoopData = $colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cl): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option value="<?php echo e($cl->id); ?>" <?php if($cl->id == $item->color_id){ echo 'selected';} ?>><?php echo e($cl->display_name); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-5">
                                                <input type="number" name="number_color[]" class="form-control" min="0" value="<?php echo e($item->quantity); ?>">
                                                </div>
                                                <div class="col-lg-2">
                                                    <span class="iframe-btn-remove" onclick="removea(this)">
                                                    <a class="btn btn-danger red iframe-btn"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                                    </span>
                                                </div>
                                            </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Mô tả</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <textarea name="description" id="editor" class="form-control my-editor" rows="20"><?php echo $product->description; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-lg-3 control-label">Danh mục*</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-plus text-primary" required></i></span>
                                            <select name = 'category_id' class = 'form-control'>
                                                <?php if($product->category_id !=0 && $product->category_id != NULL): ?>
                                                <option value="<?php echo $product->cate->id; ?>" style="text-align: center" selected ><?php echo $product->cate->name; ?></option>
                                                <option value="">---Chọn danh mục---</option>
                                                <?php showCategoryChecked($cate,$product); ?>
                                                <?php else: ?>
                                                    <option value="">---Chọn danh mục---</option>
                                                    <?php showCategories($cate); ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-xl-7 col-lg-8 add_user_checkbox_error push-lg-3">
                                        <div>
                                            <label class="custom-control custom-checkbox">
                                                <input id="publish" type="checkbox" name="publish"
                                                   class="custom-control-input" <?php echo $product->publish ? 'checked' : ''; ?>>
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description"> Publish </span>
                                            </label>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <h3>SEO</h3>
                                        <hr style="margin: 0">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_title" class="col-lg-3 control-label">Seo title (<span id="count_seo_title"><?php echo e(mb_strlen($product->seo_title, 'UTF-8')); ?></span>)</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <input type="text" name="seo_title" id="seo_title" class="form-control" value="<?php echo $product->seo_title; ?>" onKeyDown="countNumseo_title(this.form.seo_title);" onKeyUp="countNumseo_title(this.form.seo_title);">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_keyword" class="col-lg-3 control-label">Seo keyword (<span id="count_seo_keyword"><?php echo e(mb_strlen($product->seo_keyword, 'UTF-8')); ?></span>)</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <input type="text" name="seo_keyword" id="seo_keyword" class="form-control" value="<?php echo $product->seo_keyword; ?>" onKeyDown="countNumseo_keyword(this.form.seo_keyword);" onKeyUp="countNumseo_keyword(this.form.seo_keyword);">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="seo_description" class="col-lg-3 control-label">Seo descripton (<span id="count_seo_description"><?php echo e(mb_strlen($product->seo_description, 'UTF-8')); ?></span>)</label>
                                    <div class="col-xl-7 col-lg-8">
                                        <textarea name="seo_description" class="form-control my-editor" rows="7" id="seo_description" onKeyDown="countNumseo_description(this.form.seo_description);" onKeyUp="countNumseo_description(this.form.seo_description);"><?php echo e($product->seo_description); ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-3 col-lg-8">
                                    <div class="col-lg-9 push-lg-3">
                                        <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i> Cập nhật</button>
                                        <button class="btn btn-warning" type="reset" id="clear"><i class="fa fa-refresh"></i>Nhập lại</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

<script src="<?php echo e(asset('assets/fancybox/source/jquery.fancybox.js')); ?>"></script>
<!-- end of page level js -->
<script>
   
    $('#iframe-btn-0').fancybox({
        'width': 900,
        'height': 900,
        'type': 'iframe',
        'autoScale': false,
        'autoSize': false,
        afterClose: function() {
            var thumb = $('#thumb_0').val();
            if(thumb){
                var html = '<div class="img_preview"><img src="'+thumb+'"/>';
                html +='<input type="hidden" name="image" value="'+thumb+'" /> </div>';
                $('#preview').html(html);
            }
        }
    });

    $('#iframe-btn-1').fancybox({
        'width': 900,
        'height': 900,
        'type': 'iframe',
        'autoScale': false,
        'autoSize': false,
        afterClose: function() {
            var thumb1 = $('#thumb_1').val();
            if(thumb){
                var html1 = '<div class="img_preview"><a class="close-thik"></a><img src="'+thumb1+'"/>';
                html1 +='<input type="hidden" name="gallery[]" value="'+thumb1+'" /> </div>';
                $('#preview1').append(html1);
            }
        }
    });
</script>
<script>
    $("#slug").keyup(function(){
        var slug = $("#slug").val();
        $('#url_slug').html(slug);
    });
</script>
<script>
    $('#iframe-btn-plus').click(function(){

        var token = $('input[name=_token]').val();
        if(token) {
            $.ajax({
            type: 'post',
            url: '/admin/san-pham/get-color',
            headers: { 'X-XSRF-TOKEN' : token },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('X-CSRF-Token', token);
                },
                data: {type: 'ajax', _csrfToken : token},
            success: function(data) {
                data = $.parseJSON(data);
                $('#preview2').append(data);
            },
            error: function(error) {
                console.log('error');
            }
        });
        }
    });
    
    function removea(element) {
        $(element).parent().parent().remove();
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>