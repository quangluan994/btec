<?php $__env->startSection('head'); ?>
    <meta name="keywords" content="<?php echo e($setting['seo_keyword']); ?>" />
    <meta name="description" content="<?php echo e($setting['seo_description']); ?>" />
    <link rel="SHORTCUT ICON" href="<?php echo $setting['icon_website']; ?>">
    <title><?php echo e($setting['seo_title']); ?></title>

    <meta property="og:title" content="<?php echo e($setting['seo_title']); ?>">
    <meta property="og:description" content="<?php echo e($setting['seo_description']); ?>">
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="<?php echo URL::current(); ?>">
    <meta property="og:site_name" content="<?php echo e($setting['company_name']); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
       
    <div class="payment-page body-page">
        <div class="container">
            <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="col-12">
                            <h5>Mã đơn hàng: <b>PAY.ORDER.<?php echo e(addchart($order->id)); ?></b></h5>
                            <h5>Họ tên người nhận: <b><?php echo e($order->receiver_name); ?></b></h5>
                            <h5>Địa chỉ giao hàng: <b><?php echo e($order->receiver_address); ?></b></h5>
                            <h5>Số điện thoại người nhận: <b><?php echo e($order->receiver_phone_number); ?></b></h5>
                            <h5>Hình thức thanh toán: <b>
                                <?php if($order->pay_method == 'store'): ?>
                                    Thanh toán trực tiếp tại cửa hàng
                                <?php elseif($order->pay_method == 'cod'): ?>
                                    Trả tiền mặt khi giao hàng
                                <?php else: ?>
                                    Thanh toán qua cổng thanh toán vtcPay
                                <?php endif; ?>
                            </b></h5>
                            <table class="table table-striped" style="margin-top: 50px;">
                                <tr>
                                    <th>STT</th>
                                    <th style="width:180px;">Hình ảnh sản phẩm</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Số lượng</th>
                                    <th>Đơn giá</th>
                                    <th>Ghi chú</th>
                                    <th>Thành tiền</th>
                                </tr>
                                <?php if($orderdetail): ?>
                                    <?php
                                    $stt = 1;
                                    ?>
                                    <?php $__currentLoopData = $orderdetail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($stt); ?></td>
                                            <td><img src="<?php echo $item->product->thumbnail; ?>" alt="<?php echo $item->product->name; ?>" width="150"></td>
                                            <td><?php echo e($item->product->name); ?></td>
                                            <td><?php echo e($item->quantity); ?></td>
                                            <td><?php echo e(number_format($item->price_sale)); ?> đ</td>
                                            <td><?php echo e($item->color); ?></td>
                                            <td><?php echo e(number_format(($item->price_sale)*($item->quantity))); ?> đ</td>
                                        </tr>
                                    <?php
                                    $stt++;
                                    ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($order->discount !=0): ?>
                                    <tr>
                                        <td colspan="6" class="text-center"><strong>Giảm giá(%):</strong></td>
                                        <td style="color:red"><strong><?php echo e($order->discount); ?>%</strong></td>
                                    </tr>
                                    <?php endif; ?>
                                    <tr>
                                        <td colspan="6" class="text-center"><strong>Tổng cộng:</strong></td>
                                        <td style="color:red"><strong><?php echo e(number_format($item->order->total_amount)); ?> đ</strong></td>
                                    </tr>
                                <?php endif; ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front-end.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>