<?php $__env->startSection('title'); ?>
Tùy chỉnh website
##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>
<?php $__env->startSection('header_styles'); ?>
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/tablesid.css')); ?>"/>
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/optionnavtab.css')); ?>"/>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>

    <section class="content-header">
        <h1><i class="fa fa-th"></i> Option</h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="<?php echo e(url('admin/option')); ?>">Option</a></li>
          <li class="active">Danh sách</li>
        </ol>
    </section>

    <section class="content">
        <?php if(Session::has('success')): ?>
            <div class="callout alert alert-success col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto" role="alert" >
                <button type="button" class="close">×</button> 
                <?php echo e(Session::get('success')); ?>

            </div>
        <?php endif; ?>
        
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        
                    </div>
                    <?php if($showOptions): ?>
                    <div class="box-body">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                          <div class="list-group" style="margin-top:30px">
                            <?php $__currentLoopData = $showOptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <a href="#" class="list-group-item text-center"><?php echo e($item['option_value']); ?></a>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                            <?php $__currentLoopData = $showOptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $it): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="bhoechie-tab-content">
                                <center>
                                    <table class="respontables">
                                            <?php $__currentLoopData = $it['option_key']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td class="text-center"><?php echo $val->description; ?></td>
                                                <td class="text-center">
                                                    <?php if($val->type == 'media'): ?>
                                                    <img src="<?php echo $val->value; ?>" width="230"/>
                                                    <?php else: ?>
                                                    <?php echo $val->value; ?>

                                                    <?php endif; ?>

                                                </td>
                                                <td class="text-center">
                                                    <a href="/admin/cai-dat/<?php echo $val->id; ?>/edit" class="btn btn-primary" title=""><i class="fa fa-edit"></i> Cập nhật</a>
                                                    
                                                </td>
                                            </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>    
                                    </table>  
                                </center>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </section>

<script>
    $(document).ready(function() {
        $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
            e.preventDefault();
            $(this).siblings('a.active').removeClass("active");
            $(this).addClass("active");
            var index = $(this).index();
            $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
            $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
        });
        $(".list-group a:first-child").addClass("active");
        $(".bhoechie-tab .bhoechie-tab-content:first-child").addClass("active");
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>