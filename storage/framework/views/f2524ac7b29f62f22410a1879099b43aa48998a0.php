<?php $__env->startSection('title'); ?>
Chi tiết đơn hàng
##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header_styles'); ?>
<!-- plugin styles-->
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/tablesid.css')); ?>"/>
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <h1>Chi tiết đơn hàng</h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="<?php echo e(url('admin/order')); ?>">Đơn hàng</a></li>
          <li class="active">Thêm</li>
        </ol>
    </section>
    <section class="content">
        <?php if($errors->any()): ?>
        <div class="callout alert alert-danger col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto">
            <button type="button" class="close">×</button>
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                      <a href="<?php echo e(url('admin/order')); ?>" class="btn btn-primary">Quay lại</a><br/>
                    </div>
                    <div class="box-body">
                        <div class="col-12">
                            <h5>Mã đơn hàng: <b>PAY.ORDER.<?php echo e(addchart($order->id)); ?></b></h5>
                            <h5>Họ tên người nhận: <b><?php echo e($order->receiver_name); ?></b></h5>
                            <h5>Địa chỉ giao hàng: <b><?php echo e($order->receiver_address); ?></b></h5>
                            <h5>Số điện thoại người nhận: <b><?php echo e($order->receiver_phone_number); ?></b></h5>
                            <h5>Hình thức thanh toán: <b>
                                <?php if($order->pay_method == 'store'): ?>
                                    Thanh toán trực tiếp tại cửa hàng
                                <?php elseif($order->pay_method == 'cod'): ?>
                                    Trả tiền mặt khi giao hàng
                                <?php else: ?>
                                    Thanh toán qua cổng thanh toán vtcPay
                                <?php endif; ?>
                            </b></h5>
                            <table class="table table-striped" style="margin-top: 50px;">
                                <tr>
                                    <th>STT</th>
                                    <th style="width:180px;">Hình ảnh sản phẩm</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Số lượng</th>
                                    <th>Đơn giá</th>
                                    <th>Ghi chú</th>
                                    <th>Thành tiền</th>
                                </tr>
                                <?php if($orderdetail): ?>
                                    <?php
                                    $stt = 1;
                                    ?>
                                    <?php $__currentLoopData = $orderdetail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($stt); ?></td>
                                            <td><img src="<?php echo $item->product->thumbnail; ?>" alt="<?php echo $item->product->name; ?>" width="150"></td>
                                            <td><?php echo e($item->product->name); ?></td>
                                            <td><?php echo e($item->quantity); ?></td>
                                            <td><?php echo e(number_format($item->price_sale)); ?> đ</td>
                                            <td><?php echo e($item->color); ?></td>
                                            <td><?php echo e(number_format(($item->price_sale)*($item->quantity))); ?> đ</td>
                                        </tr>
                                    <?php
                                    $stt++;
                                    ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($order->discount !=0): ?>
                                    <tr>
                                        <td colspan="5" class="text-center"><strong>Giảm giá(%):</strong></td>
                                        <td style="color:red"><strong><?php echo e($order->discount); ?>%</strong></td>
                                    </tr>
                                    <?php endif; ?>
                                    <tr>
                                        <td colspan="5" class="text-center"><strong>Tổng cộng:</strong></td>
                                        <td style="color:red"><strong><?php echo e(number_format($item->order->total_amount)); ?> đ</strong></td>
                                    </tr>
                                <?php endif; ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>