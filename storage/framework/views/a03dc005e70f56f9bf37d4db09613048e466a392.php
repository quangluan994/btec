<?php $__env->startSection('title'); ?>
Danh sách người dùng
##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header_styles'); ?>
<link type="text/css" rel="stylesheet" href="<?php echo e(asset('assets/css/pages/tablesid.css')); ?>"/>
<!--End of page level styles-->
<?php $__env->stopSection(); ?>



<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <h1><i class="fa fa-th"></i> Danh sách người dùng</h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo e(url('admin/dashboard')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="<?php echo e(url('admin/user')); ?>">Người dùng</a></li>
          <li class="active">Danh sách</li>
        </ol>
    </section>

    <section class="content">
        <?php if(Session::has('success')): ?>
            <div class="callout alert alert-success col-xl-12 col-lg-12" style="font-size: 16px;margin:10px auto" role="alert" >
                <button type="button" class="close">×</button> 
                <?php echo e(Session::get('success')); ?>

            </div>
        <?php endif; ?>

        <?php if(Session::has('danger')): ?>
            <div class="alert alert-danger"><?php echo e(Session::get('danger')); ?>

            </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border  text-right">
                        <div class=" form-group fix-float">
                            <select class="styled hasCustomSelect form-control" style="width:150px; float:left">
                                <option>Chọn tác vụ</option>
                                <option value="remove">Xóa</option>
                            </select>
                            <a href="javascript:action();" class="btn btn-info">Apply</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="card-block p-t-25">
                            <div class ="row col-lg-12"> 
                                <table class="respontables">
                                    <tr>
                                        <th style="width: 1%"> <input type="checkbox" class="check-all"></th>
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Tên hiển thị </th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center">Ảnh đại diện</th>
                                        <th class="text-center">Quyền người dùng</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                    <?php if($users->count()>0): ?>
                                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><input type="checkbox" class="checkbox" name="checkbox_id" value="<?php echo e($val->id); ?>" id="select-<?php echo e($val->id); ?>"></td>
                                            <td class="text-center"><?php echo $val->id; ?></td>
                                            <td class="text-center"><?php echo $val->name; ?></td>
                                            <td class="text-center"><?php echo $val->email; ?></td>
                                            <td class="text-center"><img src="<?php echo $val->avatar; ?>" alt="" width="70" height="70"></td>
                                            <td class="text-center"><?php echo $val->role->display_name; ?></td>
                                            <td class="text-center">
                                                <a href="/admin/user/<?php echo $val->id; ?>/edit" class="btn btn-primary" title=""><i class="fa fa-edit"></i></a>
                                                <a href="/admin/user/<?php echo $val->id; ?>/delete" class="btn btn-danger" ><i class="fa fa-trash"></i></a>
                                                
                                                
                                            </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                    <tr><td colspan="7"><h4>Không có dữ liệu để hiển thị</h4></td></tr>  
                                    <?php endif; ?>      
                                </table>   
                            </div>
                            <div style="clear:both"></div>
                            <div class="row col-lg-4 col-md-6 col-sm-6" style="float:right">
                                <?php echo e($users->links()); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo e(csrf_field()); ?>

    </section>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>